#pragma once
#include "Framework\Vec3.h"
#include "Framework\HudElement.h"
#include "Framework\FrameCommander.h"
#include "RandomUnit.h"
#include "Framework\PlaneTexNorm.h"
struct Effect2DData
{
	Effect2DData(float pX, float pY, float scale = 1.0f, float opacity = 1.0f,float rotation = 0.0f) : posX(pX), posY(pY), scale(scale), opacity(opacity),rotation(rotation) {}
	Effect2DData operator*(float f)
	{
		Effect2DData e2d(posX * f, posY * f, scale * f, opacity * f, rotation*f);
		return e2d;
	}
	Effect2DData operator+(Effect2DData in)
	{
		Effect2DData e2d(posX + in.posX, posY + in.posY, scale + in.scale, opacity + in.opacity,rotation+in.rotation);
		return e2d;
	}
	float posX;
	float posY;
	float scale;
	float opacity;
	float rotation;
};

struct Effect3DData
{
	Effect3DData(float pX, float pY,float pZ, float scale = 1.0f, float opacity = 1.0f, float roll = 0.0f, float pitch = 0.0f, float yaw = 0.0f) :
		posX(pX), posY(pY),posZ(pZ), scale(scale), opacity(opacity),roll(roll),pitch(pitch),yaw(yaw)  {}
	Effect3DData operator*(float f)
	{
		Effect3DData e3d(posX * f, posY * f, posZ * f, scale * f, opacity * f, roll * f, pitch * f, yaw * f);
		return e3d;
	}
	Effect3DData operator+(Effect3DData in)
	{
		Effect3DData e3d(posX + in.posX, posY + in.posY, posZ + in.posZ, scale + in.scale, opacity + in.opacity, roll + in.roll, pitch + in.pitch, yaw + in.yaw);
		return e3d;
	}
	float posX;
	float posY;
	float posZ;
	float scale;
	float opacity;
	float roll;
	float pitch;
	float yaw;
};

#ifndef EFFECT2DDATAINTERPOL
#define EFFECT2DDATAINTERPOL
Effect2DData Effect2DDataInterpolate(Effect2DData e1, Effect2DData e2, float progress);
#endif

#ifndef EFFECT3DDATAINTERPOL
#define EFFECT3DDATAINTERPOL
Effect3DData Effect3DDataInterpolate(Effect3DData e1, Effect3DData e2, float progress);
#endif

class Effect2DSequence			//sequence of Effect2DData
{
public:
	Effect2DSequence() = default;				//this object must be constructed manually due to undefined number of arguments (and my incompetence in handling such things with template metagrogramming)
	void AddStage(Effect2DData e2d, float time)			//time is the time of iterplation from this stage to the next one. for last stage time is supposed to be 0;
	{ stages.push_back(e2d); stageTimes.push_back(time); }
	std::optional<Effect2DData> Update(float dt);
private:
	std::vector<Effect2DData> stages;		
	std::vector<float>stageTimes;
	float currentStageTime = 0.0f;
};

class Effect3DSequence			
{
public:
	Effect3DSequence() = default;				//this object must be constructed manually due to undefined number of arguments (and my incompetence in handling such things with template metagrogramming)
	void AddStage(Effect3DData e3d, float time)			//time is the time of iterplation from this stage to the next one. for last stage time is supposed to be 0;
	{
		stages.push_back(e3d); stageTimes.push_back(time);
	}
	std::optional<Effect3DData> Update(float dt);
private:
	std::vector<Effect3DData> stages;
	std::vector<float>stageTimes;
	float currentStageTime = 0.0f;
};

struct ParticleBurstData
{
	size_t count;
	PositionData origin;
	float speedMin;
	float speedMax; 
	float lifeTime; 
	float friction;
};

struct ParticleSpreadData
{
	Vec3 minRollPitchYaw;
	Vec3 maxRollPitchYaw;
	Vec3 minXYZDir;
	Vec3 maxXYZDir;
};

class Effect2DPhysics			//Effect2DData, that moves with friction
{
public:
	Effect2DPhysics(ParticleBurstData burstData,  std::wstring sprite, DXGraphics& gfx);
	Effect2DPhysics(Effect2DPhysics&&) = default;
	Effect2DPhysics& operator=(Effect2DPhysics&&) = default;
	void SubmitAndUpdateParticles(FrameCommander& fc, float dt);	
	bool isDead = false;
private:
	std::vector<PositionData> partPos;
	std::vector<PositionData> partVel;		//here PositionData.pos represents velocities and .rot - acceleration
	HudElement particleSprite;
	float curTime = 0.0f;
	const float lifeTime;
	static constexpr float partScale = 0.025f;
};

class Effect3DPhysics			
{
public:
	Effect3DPhysics(ParticleBurstData burstData, ParticleSpreadData partSpread, std::wstring model, DXGraphics& gfx);
	Effect3DPhysics(Effect3DPhysics&&) = default;
	Effect3DPhysics& operator=(Effect3DPhysics&&) = default;
	void SubmitAndUpdateParticles(FrameCommander& fc, float dt);		//make friend of handler
	bool isDying = false;
	bool isDead = false;
private:
	struct OpacCBuff
	{
		float val;
		float pad1;
		float pad2;
		float pad3;
	};
	std::vector<PositionData> partPos;
	std::vector<PositionData> partVel;	//here PositionData.pos represents velocities and .rot - acceleration
	PlaneTexNorm particleModel;
	std::shared_ptr <Bind::PixelConstantBuffer<OpacCBuff>> cbuf = nullptr;
	OpacCBuff cbData;
	float curTime = 0.0f;
	const float friction = 0.97f;			//actually the opposite of friction
	const float lifeTime;
	static constexpr float deathTime = 1.0f;
	DXGraphics& gfx;
};

template<typename T, typename Base>
class HudEffect : public Base
{
public:
	HudEffect(DXGraphics& gfx, T data, std::wstring spriteName);
	HudEffect(HudEffect&&);
	HudEffect(const HudEffect&) = delete;
	HudEffect& operator = (HudEffect&&) = default;
	HudEffect& operator = (const HudEffect&) = delete;
	bool UpdateData(float dt);			//returns 1 if object is dead
private:
	struct EffectCBuff cbData0;
	struct EffectCBuff cbData1;
	std::vector<PositionData> refPosData;
	std::shared_ptr <Bind::PixelConstantBuffer<struct EffectCBuff>> cbuf = nullptr;
	std::shared_ptr <Bind::VertexConstantBuffer<struct EffectCBuff>> scaleCbuf = nullptr;
	T effectSequence;
	DXGraphics& gfx;
};


class EffectScenario				//is used to output same Effect3DPhysics several times with delay
{
public:
	enum class EffectStep
	{
		PlayerSandThrow,
		PlayerSandStorm,
		AmbientPermanentBackgroundSmoke
	};
	EffectScenario(EffectStep e, size_t timesRepeat, float delay);
	EffectScenario(EffectScenario&&) = default;
	EffectScenario& operator=(EffectScenario&&) = default;
	EffectScenario& operator=(const EffectScenario&) = delete;
	bool Execute(float dt);							//returnes 1 if effect scenario has ended

private:
	size_t curStage = 0u;
	size_t totalStages;
	float curTime = 0u;
	float stageTime;				//not constant so that this type was assignable
	EffectStep effect;
};
#define EH EffectHandler
class EffectHandler
{
public:
	enum class Effect
	{
		Shine,
		ShineOrange,
		Sparks,
		GhoulIntentShine_Attack,
		GhoulIntentShine_Buff,
		GhoulIntentShine_Defence,
		GhoulAttack,
		GhoulHowl,
		PlayerSwordAttack,
		PlayerSwordAttackUpgraded,
		PlayerSandThrowDamage,
		PlayerSandThrow,
		PlayerSandStorm,
		AmbientPermanentBackgroundSmoke
	};
	EffectHandler(DXGraphics& gfx);
	static void AddEffect2D(Effect2DSequence data, std::wstring spriteName);
	static void AddEffect2D(PositionData posData, Effect e);
	static void AddEffect2D(Effect e);		//for effects that are always in the same location
	static void AddEffect3D(Effect3DSequence data, std::wstring spriteName, bool CamFacing = 0);
	static void AddEffect3D(Effect e);		//for effects that are always in the same location
	static void AddScenario(EffectScenario::EffectStep e, size_t timesRepeat, float delay);
	static void SetNextAngle(float ang);		//camera manager sets this when decides in what direction an action will be executed 
	void SubmitAndUpdate(FrameCommander& fc, float dt);		//can be made a friend
	static void AddEffect3D(PositionData posData, Effect e);
private:
	static std::vector<std::unique_ptr<HudEffect<Effect2DSequence,HudElement>>> e2d;			//uses pointers because HudElement cant be copied
	static std::vector<std::unique_ptr<Effect2DPhysics>> e2df;
	static std::vector<std::unique_ptr<HudEffect<Effect3DSequence,PlaneTexNorm>>> e3d;					//for normal 3d effects
	static std::vector<std::unique_ptr<HudEffect<Effect3DSequence, PlaneTexNorm>>> e3dCamFace;			//for cam facing 3d effects
	static std::vector<std::unique_ptr<Effect3DPhysics>> e3df;
	static std::vector<EffectScenario> scenarios;
	static float nextAngle;

	static DXGraphics* gfx;
};




