#pragma once
class PressableMenu
{
public:
	enum class Key
	{
		W, A, S, D, All, None
	};
	Key GetPressedKey() const {return pressedKey; };
	void KeyAcquired() const { pressedKey = Key::None; };
protected:
	void SetPressedKey(Key in) { pressedKey = in; }
private:
	mutable Key pressedKey = Key::None;
};