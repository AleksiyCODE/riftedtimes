#include "StatusBar.h"

StatusBar::StatusBar(const CombatData& cd, DXGraphics& gfx, PositionData pos, StatusOwner owner):
	cd(cd),
	owner(owner)
{
	if (owner == StatusOwner::Player)
	{
		bar = std::make_unique<PlayerBar>(gfx);
		healthOffset = Vec2{ -0.65f,-0.87f };
		blockOffset = Vec2 { -0.59f,-0.823f};
	}
	else
	{
		bar = std::make_unique<EnemyBar>(gfx);
		healthOffset = Vec2{ -0.68f,0.748f };
		blockOffset = Vec2 { -0.67f,0.705f };
	}
}

void StatusBar::UpdateStatus(DXGraphics& gfx)
{
	if (owner == StatusOwner::Player)
	{
		bar->UpdateFillness((float)cd.playerHP / (float)cd.playerMaxHP, (float)cd.playerBlock / ((float)cd.playerMaxHP/5.0f), gfx);
	}
	else
	{
		bar->UpdateFillness((float)cd.enemyHP / (float)cd.enemyMaxHP, (float)cd.enemyBlock / (float)cd.enemyMaxHP, gfx,cd.enemySpeed);
	}
}

void PlayerStatusBar::SmartSubmit(FrameCommander& fc) const
{
	dodgeIcon.UpdateData(1, cd.playerStatusEffects->at(StatusEffects::Dodge).first);
	critIcon.UpdateData(2, cd.playerStatusEffects->at(StatusEffects::CritChance).first);
	if (cd.playerStatusEffects->find(StatusEffects::Bleed) != cd.playerStatusEffects->end())
	{
		bleedIcon.UpdateData(3, cd.playerStatusEffects->at(StatusEffects::Bleed).first, cd.playerStatusEffects->at(StatusEffects::Bleed).second);
		bleedIcon.SmartSubmit(fc);
	}
	bar->Submit(fc);
	critIcon.SmartSubmit(fc);
	dodgeIcon.SmartSubmit(fc);
	//text drawing
		TD::DrawString(std::to_wstring(cd.playerHP) + L"/" + std::to_wstring(cd.playerMaxHP),				 healthOffset, Colors::White,   textSize);
	if(cd.playerBlock!=0u)
		TD::DrawString(std::to_wstring(cd.playerBlock),														 blockOffset, Colors::White,	textSize);
	//handling ascending text
	if (previousCD.playerBlock > cd.playerBlock)		
		TD::DrawStringAscending(		std::to_wstring((int)cd.playerBlock - (int)previousCD.playerBlock),	 blockOffset, Colors::LightBlue, ascSize);
	else if(previousCD.playerBlock < cd.playerBlock)	
		TD::DrawStringAscending(L"+" +  std::to_wstring(	cd.playerBlock - previousCD.playerBlock),		 blockOffset, Colors::LightBlue, ascSize);

	if (previousCD.playerHP > cd.playerHP)				
		TD::DrawStringAscending(		std::to_wstring((int)cd.playerHP - (int)previousCD.playerHP),		 healthOffset, Colors::Red,	   	 ascSize);
	else if (previousCD.playerHP < cd.playerHP)			
		TD::DrawStringAscending(L"+" +  std::to_wstring(cd.playerHP - previousCD.playerHP),					 healthOffset, Colors::Green,	 ascSize);

	previousCD.playerHP	=	 cd.playerHP;
	previousCD.playerMaxHP = cd.playerMaxHP;
	previousCD.playerBlock = cd.playerBlock;
	previousCD.playerSpeed = cd.playerSpeed;
}

void EnemyStatusBar::SmartSubmit(FrameCommander& fc) const
{
	accIcon.UpdateData(1, cd.enemyStatusEffects->at(StatusEffects::Accuracy).first);
	if (cd.enemyStatusEffects->find(StatusEffects::Bleed) != cd.enemyStatusEffects->end())
	{
		bleedIcon.UpdateData(2, cd.enemyStatusEffects->at(StatusEffects::Bleed).first, cd.enemyStatusEffects->at(StatusEffects::Bleed).second);
		bleedIcon.SmartSubmit(fc);
	}
	accIcon.SmartSubmit(fc);
	bar->Submit(fc);
	//text drawing
		TD::DrawString(std::to_wstring(cd.enemyHP) + L"/" + std::to_wstring(cd.enemyMaxHP),					healthOffset, Colors::White,	textSize);
	if (cd.enemyBlock != 0u) 
		TD::DrawString(std::to_wstring(cd.enemyBlock),														blockOffset, Colors::White,		textSize);

	//handling ascending text
	if (previousCD.enemyBlock > cd.enemyBlock)
		TD::DrawStringAscending(		std::to_wstring((int)cd.enemyBlock - (int)previousCD.enemyBlock),	blockOffset, Colors::LightBlue, ascSize, 1.0f, -textSpeed);
	else if (previousCD.enemyBlock < cd.enemyBlock)
		TD::DrawStringAscending(L"+" +	std::to_wstring(cd.enemyBlock - previousCD.enemyBlock),				blockOffset, Colors::LightBlue, ascSize, 1.0f, -textSpeed);

	if (previousCD.enemyHP > cd.enemyHP)
		TD::DrawStringAscending(		std::to_wstring((int)cd.enemyHP - (int)previousCD.enemyHP),			healthOffset, Colors::Red,		ascSize, 1.0f, -textSpeed);
	else if (previousCD.enemyHP < cd.enemyHP)
		TD::DrawStringAscending(L"+" +  std::to_wstring(cd.enemyHP - previousCD.enemyHP),					healthOffset, Colors::Green,	ascSize, 1.0f, -textSpeed);

	previousCD.enemyHP =	cd.enemyHP;
	previousCD.enemyMaxHP = cd.enemyMaxHP;
	previousCD.enemyBlock = cd.enemyBlock;
	previousCD.enemySpeed = cd.enemySpeed;
}
