//coded by Chili
#include "Topology.h"
#include "BindableCodex.h"
#include "ChiliUtil.h"

namespace Bind
{
	Topology::Topology(DXGraphics& gfx,D3D11_PRIMITIVE_TOPOLOGY type )
		:
		type( type )
	{}

	void Topology::Bind(DXGraphics& gfx ) noexcept
	{
		GetContext( gfx )->IASetPrimitiveTopology( type );
	}
	std::shared_ptr<Topology> Topology::Resolve(DXGraphics& gfx,D3D11_PRIMITIVE_TOPOLOGY type )
	{
		return Codex::Resolve<Topology>( gfx,type );
	}
	std::wstring Topology::GenerateUID( D3D11_PRIMITIVE_TOPOLOGY type )
	{
		return ToWide(typeid(Topology).name()) + L"#" + std::to_wstring(type);
	}
	std::wstring Topology::GetUID() const noexcept
	{
		return GenerateUID( type );
	}
}
