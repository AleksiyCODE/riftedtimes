#pragma once
#include "ConstantBuffers.h"
#include "Drawable.h"
#include <DirectXMath.h>

namespace Bind
{
	class TransformCbuf2D : public Bindable
	{
	protected:
		struct Transforms2D
		{
			DirectX::XMFLOAT4 transform;	//first 2 floats - x,y position; third - scale, fourth - rotation
		};
	public:
		TransformCbuf2D(DXGraphics& gfx, UINT slot = 0u);
		void Bind(DXGraphics& gfx) noexcept override;
		void InitializeParentReference(const Drawable& parent) noexcept override;
		virtual std::wstring GetUID() const  noexcept override
		{
			return L"TransformCbuf2D";
		}
	protected:
		void UpdateBindImpl(DXGraphics& gfx, const Transforms2D& tf) noexcept;
		Transforms2D GetTransforms(DXGraphics& gfx) noexcept;
	private:
		static std::unique_ptr<VertexConstantBuffer<Transforms2D>> pVcbuf;
		const Drawable* pParent = nullptr;
	};
}