#pragma once
#define WIDE2(x) L##x					//no idea whats going on here
#define WIDE1(x) WIDE2(x)
#define WFILE WIDE1(__FILE__)

#define CHWND_EXCEPT( hr ) MainWindow::HrException( __LINE__,WFILE,(hr) )
#define CHWND_LAST_EXCEPT() MainWindow::HrException( __LINE__,WFILE, GetLastError() )
#define CHWND_NOGFX_EXCEPT() MainWindow::NoGfxException( __LINE__,WFILE )
