#include "SkillMicroicons.h"

SkillMicroicon::SkillMicroicon(DXGraphics& gfx, std::wstring spriteName) :
	HudElement(spriteName, gfx)
{
	BindPositionData(&posData);
}

void SkillMicroicon::AddDrawingPosition(size_t ind, const HudElement* parent, size_t parentInd)
{
	const float y = 0.73f;
	const float x =  (xOffset+size) * ind - 1.0f+xInitOffset;
	posData.emplace_back(x, y, size);
	AddParent(parent, parentInd, posData.size() - 1u);
}

void SkillMicroicon::ClearDrawingPositions()
{
	UnbindParents();
	posData.clear();
}
