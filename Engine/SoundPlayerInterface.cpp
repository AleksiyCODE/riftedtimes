#include "SoundPlayerInterface.h"
#include <assert.h>
void SoundPlayerInterface::LinkToPlayer(SoundPlayer * in_player)
{
	assert(in_player);
	player = in_player;	
}

void SoundPlayerInterface::Play(Sounds snd, float freqMod, float volMod, float playbackSpeed)
{
	assert(player);
	player->Play(snd, freqMod, volMod, playbackSpeed);
}

void SoundPlayerInterface::Stop(Sounds snd)
{
	assert(player);
	player->Stop(snd);	
}

void SoundPlayerInterface::StopAllInstances(Sounds snd)
{
	assert(player);
	player->StopAllInstances(snd);
}
 SoundPlayer* SoundPlayerInterface::player = nullptr;