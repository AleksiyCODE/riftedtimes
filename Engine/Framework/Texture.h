//coded by Chili
#pragma once
#include "Bindable.h"

class Surface;

namespace Bind
{
	class Texture : public Bindable
	{
	public:
		Texture(DXGraphics& gfx,const std::wstring& path,UINT slot = 0 );
		void Bind(DXGraphics& gfx ) noexcept override;
		static std::shared_ptr<Texture> Resolve(DXGraphics& gfx,const std::wstring& path,UINT slot = 0 );
		static std::wstring GenerateUID( const std::wstring& path,UINT slot = 0 );
		std::wstring GetUID() const noexcept override;
		bool HasAlpha() const noexcept;
		size_t GetWidth() const noexcept;
		size_t GetHeight() const noexcept;
	private:
		unsigned int slot;
	protected:
		bool hasAlpha = false;
		std::wstring path;
		Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> pTextureView;
		size_t width;
		size_t height;
	};
}
