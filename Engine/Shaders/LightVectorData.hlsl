#include "NumOfLights.hlsl"

struct LightVectorData
{
    float3 vToL;
};

struct FullLightVectorData
{
    float3 vToL;
    float3 dirToL;
    float distToL;
};

cbuffer PointLightPosition : register(b5)
{
    float3 viewLightPos[NUM_OF_LIGHTS];
};

FullLightVectorData CalculateLightVectorData(const in float3 vecToLight)
{
    FullLightVectorData lv;
    lv.vToL = vecToLight;
    lv.distToL = length(lv.vToL);
    lv.dirToL = lv.vToL / lv.distToL;
    return lv;
}