#include "HudOverlay.h"
#include <algorithm>
#include "Framework\ConstantBuffersEx.h"
#include "Framework\PixelShader.h"
#include "BlurManager.h"


HudOverlay::HudOverlay(DXGraphics& gfx, const CombatData& cd):
	defaultOverlay(L"Media//Sprites//overlay_default.png", gfx),
	bloodOverlay(L"Media//Sprites//overlay_blood.png",gfx),
	gfx(gfx),
	cd(cd)
{
	auto& s = bloodOverlay.GetTechniquesReference()[0].GetSteps()[0];
	s.DeleteBindableByUID(L"class Bind::PixelShader#Shaders\\HudPS.cso");
	s.AddBindable(Bind::PixelShader::Resolve(gfx, L"Shaders\\HUDEffectPS.cso"));	
	auto pcb = std::make_shared<Bind::PixelConstantBuffer<Opac>>(gfx, 3);
	cbuf = pcb;
	s.AddBindable(pcb);
	posBlood.emplace_back(0.0f, 0.0f, 1.0f);
	bloodOverlay.BindPositionData(&posBlood);
	posDefault.emplace_back(0.0f, 0.0f, 1.0f);
	defaultOverlay.BindPositionData(&posDefault);
}

void HudOverlay::BoostOverlay(float boost)			//is called from PlayerManager::SufferPhysicalDamage, EntityManager::SufferPureDamage
{
	boost = std::max(0.05f, boost);
	pendingPower += (boost * boostPower);
}

void HudOverlay::SmartSubmit(FrameCommander& fc) const
{
	defaultOverlay.SmartSubmit(fc);
	if (currentPower != 0.0f)
	{
		cbuf->Update(gfx, { currentPower,0.0f,0.0f,0.0f });
		bloodOverlay.SmartSubmit(fc);
	}
}

void HudOverlay::Animate(float dt)			//animated by HUD
{	
	Pulse(dt);
	pendingPower = std::min(pendingPower * (falloff * dt), pendingPower * 0.99999f);
	float coeff = changeSensitivity * dt;
	currentPower = currentPower * (1.0f - coeff) + pendingPower * coeff;
	if (currentPower <= 0.001f)currentPower = 0.0f;
}

void HudOverlay::Pulse(float dt)
{
	if (cd.isValid)
	{
		const float health = (float)cd.playerHP / (float)cd.playerMaxHP;
		if (health != 0.0f)
		{
			if (health <= 0.1f)
			{
				curPulseTime += dt;
				if (curPulseTime >= pulseTime)
				{
					curPulseTime -= pulseTime;
					if (health >= 0.05f)		//if between 10% and 5%
					{
						BlurManager::AddBlur(0.3);
						BoostOverlay(0.3f);
					}
					else if (health > 0.0f)	//if between 0 and 5%
					{
						BlurManager::AddBlur(0.7);
						BoostOverlay(0.5f);
					}
				}
			}
		}
		else
		{
			BlurManager::AddBlur(0.06);	//player is dead
			BoostOverlay(0.05f);
		}
	}
}

float  HudOverlay::pendingPower = 0.0f;