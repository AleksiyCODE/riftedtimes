#pragma once
#include "..\Framework\HudElement.h"

class SkillMicroicon : protected HudElement
{
public:
	using HudElement::SmartSubmit;
	virtual ~SkillMicroicon() = default;
	SkillMicroicon(DXGraphics& gfx, std::wstring spriteName);
	SkillMicroicon() = default;
	SkillMicroicon(SkillMicroicon&&) = default;
	SkillMicroicon& operator = (SkillMicroicon&&) = delete;
	void AddDrawingPosition(size_t ind, const HudElement* parent, size_t parentInd);	//ind indicates the number (left-to-right) of this icon
	void ClearDrawingPositions();
protected:
	std::vector<PositionData> posData;
	static constexpr float xOffset = 0.04f;
	static constexpr float xInitOffset = 0.60f;
	static constexpr float size = 0.25f;
};