#pragma once
#include "Skill.h"
#include "../SoundPlayerInterface.h" 
#include <assert.h>
#include "../AchievementManager.h"
#include "../EffectHandler.h"

enum class Skill0			//order matters. is used for indexing 
{
	Cut,
	Deflect,
	SandStorm,
	SharpenTheBlade,
	SandsOfTime,
	DustThrow,
	CutBlocked,
	DeflectBlocked,
	SandStormBlocked,
	SharpenTheBladeBlocked,
	SandsOfTimeBlocked,
	DustThrowBlocked,
	Inactive
};

class Cut : public Skill			//skill ID has to match skills position in Skill0 enum
{
public:
	Cut(CombatEntity* owner);
	void Activate() override;
	void Upgrade() override;
	void OnPend()override;
	static float step0;
	static float step1;
};

class Deflect : public Skill
{
public:
	Deflect(CombatEntity* owner);
	void Activate() override;
	void Upgrade() override;
	void OnPend()override;
	static float step0;
};

class SandStorm : public Skill
{
public:
	  SandStorm(CombatEntity* owner);
	void Activate() override;
	void Upgrade() override;
	void OnPend()override;
	static float step0;
	static float step1;
};


class SharpenTheBlade : public Skill
{
public:
	SharpenTheBlade(CombatEntity* owner);
	void Activate() override;
	void Upgrade() override;
	void OnPend()override;
	static constexpr float block = 3.0f;
	static float step0;
};

class SandsOfTime : public Skill
{
public:
	SandsOfTime(CombatEntity* owner);
	void Activate() override;
	void Upgrade() override;
	void OnPend()override;
	static float step0;
};

class DustThrow : public Skill
{
public:
	DustThrow(CombatEntity* owner);
	void OnPend()override;
	void Activate() override;
	void Upgrade() override;
	static constexpr float dmg = 4.0f;
	static float step0;
	static float step1;
};
