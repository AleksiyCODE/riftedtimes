#pragma once
#include "ConditionalNoexcept.h"

class DXGraphics;

namespace Bind
{
	class BufferResource
	{
	public:
		virtual ~BufferResource() = default;
		virtual void BindAsBuffer(DXGraphics&) noxnd = 0;
		virtual void BindAsBuffer(DXGraphics&, BufferResource*) noxnd = 0;
		virtual void Clear(DXGraphics&) noxnd = 0;
	};
}