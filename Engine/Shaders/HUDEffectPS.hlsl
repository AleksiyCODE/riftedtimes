cbuffer TransformCBuf : register(b3)
{
    float opacity;
    float3 padding;
};
Texture2D tex;
SamplerState splr;
float4 main(float2 tc : Texcoord, float4 pos : SV_Position) : SV_Target
{
    float4 dtex;
        dtex = tex.Sample(splr, tc);
    return float4((opacity * dtex).rgba);
}



