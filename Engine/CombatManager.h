#pragma once
#include <assert.h>
#include<vector>
#include "./Framework/Keyboard.h"
#include "PlayerManager.h"
#include "EnemyCombat.h"
#include "CombatTimers.h"
#include "TurnAnnouncer.h"

struct CombatData
{
	size_t playerMaxHP;
	size_t playerHP;
	size_t playerBlock;
	float playerSpeed;
	const std::map <StatusEffects, std::pair<float, bool>>* playerStatusEffects;

	size_t enemyMaxHP;
	size_t enemyHP;
	size_t enemyBlock;
	float enemySpeed;
	const std::map <StatusEffects, std::pair<float, bool>>* enemyStatusEffects;
	AbilityType enemyIntent;

	bool isValid = false;
};
class CombatManager	
{
public:									
	CombatManager();
	void StartBattle(size_t enemyID);
	void EndBattle();
	PlayerManager& GetPlayerManager();
	EnemyCombat& GetEnemyCombat();
	const CombatData& GetCombatDataReference() const;
	bool IsReturningToMenu() const;
	void ReturnedToMenu();
	bool LightDistortingActionHappened();
	void OnPlayerDeath();
	void OnEnemyDeath(); 
	void EndPlayerTurn();
	void StartNewTurn();
	void StartPlayerTurn();		
	void StartEnemyTurn();		
	int  ProcessInput(Keyboard::Event in_event);	
	void Draw(class DXGraphics& gfx);	
	void UpdateCombatState(float dt);	
	EnemyCombat::State GetEnemyState()const;
	bool GetRewardMenuActiveness() const;
	void ChoseBandage();
	void ChoseCrit();
	void ChoseDodge();
	bool ChoseUpgrade();
private:
	enum class Turn							
	{
		Prefase,
		Player,
		Enemy,
		PlayerCoountdown,
		StartingEnemy,
		PostActionPlayerPause,
		PostActionEnemyPause,
		AnnouncingPlayerTurn,
		AnnouncingEnemyTurn,								
		ExpressingEnemyIntent,
		EnemyDied,
		PlayerDied,
		StartingTurn,
		ChoosingReward,
		NotInCombat
	};	
	enum class TurnBeginningState
	{
		PlayerStatusEffects,
		EnemyStatusEffects,
		StatusEffectDumping,		
		None
	};
	void IncreaceKillcount();
	void DrawDamage(bool missed = false, size_t dmg = 0u, bool crited = false, bool bleed = false, bool playerProduced = true) const;
	void UpdateCombatData();
	void HandleEvents();
	void HandleEvent(SkillEvent ev);
	CombatData combatData;
	Turn curTurn = Turn::NotInCombat;
	TurnBeginningState turnStartState = TurnBeginningState::None;
	CombatTimers timers;
	TurnAnnouncer turnAnnouncer;
	std::vector<char>keysForActionBoxes;			
	std::unique_ptr<PlayerManager>playMan;
	std::unique_ptr<EnemyCombat>enMan;
	bool returningToMenu = false;			
	bool rewardMenuIsActive = false;
	size_t currentProcingEffect = 0u;
	size_t killcount = 0u;
	size_t currentTurn = 0u;
	float accumulatedTime = 0.0f;
	float accumumPostPlayerTime = 0.0f;
	bool playerTurnIsEnded = false;
	bool lightDistortingActionHappened = false;
	static constexpr float enemyTurnAnimationLength = 1.0f;
	static constexpr float statusEffetAnnounceTime = 1.0f;
	static constexpr float pauseAfterPlayerTurn = 1.0f;
	static constexpr float critBuffEmount = 0.05f;		//from reward menu
	static constexpr float dodgeBuffEmount = 0.05f;
	static constexpr float enemySpeedIncRate = 0.185f; 
};