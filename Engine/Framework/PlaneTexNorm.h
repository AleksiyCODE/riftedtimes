#pragma once
#include "Drawable.h"
#include "Bindable.h"
#include "IndexBuffer.h"
#include "ConstantBuffers.h"
#include <type_traits>
#include <filesystem>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

class PlaneTexNorm : public Drawable		//a plane with a diffuse map and possibly a normal map
{
public:
	PlaneTexNorm(std::wstring modelName, DXGraphics& gfx, float scale = 1.0f, float roll = 0.0f, float pitch = 0.0f, float yaw = 0.0f);
	PlaneTexNorm() = default;
	PlaneTexNorm(PlaneTexNorm&&);
	PlaneTexNorm& operator=(PlaneTexNorm&&) = delete;
	PlaneTexNorm& operator=(const PlaneTexNorm&) = default;
	DirectX::XMMATRIX GetTransformXM() const noexcept override;
	void BindPositionData(std::vector<PositionData>* posData);
	void SmartSubmit(FrameCommander& fc, Techniques mainTec);		//first draw call will happen with mainTec and the rest with consecutive
	virtual ~PlaneTexNorm() = default;
private:
	const std::vector<PositionData>* posData;		//one position for one object 
	mutable size_t currentObjectBeingDrawn = 0;
};

