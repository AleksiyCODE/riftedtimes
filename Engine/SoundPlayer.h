#pragma once
#include <map>
#include<memory>
#include"Framework/Sound.h"
enum class Sounds		
{
	CombatCountdown,
	ABoxSkillPress,
	ABoxBlockedPress,
	MissLittleDamage,
	MissBigDamage,
	CountdownTick,
	Cut,
	Defend,
	SharpenTheBlade,
	SandStorm,
	SandsOfTime,
	Ghoul_Block,
	Ambient_Dungeon,
	Applause,
	Ambient_Torch,
	Dust_Throw,
	Player_Block,
	Player_Hit,
	tic_tac,
	TimerEnd,
	TimerCritical,
	Block,
	Ghoul_attack_01,
	Ghoul_attack_02,
	Ghoul_attack_03,
	Ghoul_Death,
	Ghoul_oh_01,
	Ghoul_oh_02,
	Ghoul_Enrage,
	Ghoul_Enforce,
	SkillUpgrade,
	Bandage,
	StartBattle,
	StartPlayerTurn,
	StartEnemyTurn,
	CritWave
};

class SoundPlayer
{
public:
	SoundPlayer();
	~SoundPlayer() = default;
private:
	friend class SoundPlayerInterface;
	SoundPlayer(const  SoundPlayer&) = delete;
	SoundPlayer& operator=(const  SoundPlayer&) = delete;
	void Play(Sounds, float freqMod = 1.0f, float volMod = 1.0, float playbackSpeed = 1.0f)const;
	void Stop(Sounds)const;
	void StopAllInstances(Sounds)const;  //is used to stop all of the same sound playing r/n 
	std::map<Sounds, std::unique_ptr<Sound>> pSounds;
};
