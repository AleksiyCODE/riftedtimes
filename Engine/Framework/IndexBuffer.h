//coded by Chili
#pragma once
#include "Bindable.h"

namespace Bind
{
	class IndexBuffer : public Bindable
	{
	public:
		IndexBuffer(DXGraphics& gfx,const std::vector<unsigned short>& indices );
		IndexBuffer(DXGraphics& gfx,std::wstring tag,const std::vector<unsigned short>& indices );
		void Bind(DXGraphics& gfx ) noexcept override;
		UINT GetCount() const noexcept;
		static std::shared_ptr<IndexBuffer> Resolve(DXGraphics& gfx,const std::wstring& tag,
			const std::vector<unsigned short>& indices );
		template<typename...Ignore>
		static std::wstring GenerateUID( const std::wstring& tag,Ignore&&...ignore )
		{
			return GenerateUID_( tag );
		}
		std::wstring GetUID() const noexcept override;
	private:
		static std::wstring GenerateUID_( const std::wstring& tag );
	protected:
		std::wstring tag;
		UINT count;
		Microsoft::WRL::ComPtr<ID3D11Buffer> pIndexBuffer;
	};
}