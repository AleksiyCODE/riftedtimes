#include "CeilingHangers.h"
#include "RandomUnit.h"

CeilingHangers::CeilingHangers(DXGraphics& gfx, RectF spawnArea, size_t numHangers) :
	model(L"Media//Models//CeilingHanger0.obj",gfx,0.25f)
{
	for (size_t i = 0; i < numHangers; i++)
	{
		posData.emplace_back(RU.GetFloat(spawnArea.bottom, spawnArea.top), 1.81f, RU.GetFloat(spawnArea.right, spawnArea.left), 0.0f, 3 * PI / 2, PI / 2);
	}
	model.BindPositionData(&posData);
}

void CeilingHangers::Submit(FrameCommander& fc)
{
	model.SmartSubmit(fc, Techniques::CamFacing);
}
