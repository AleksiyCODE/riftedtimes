#include "WASDPanels.h"
#include "EffectHandler.h"

WASDPanel::WASDPanel(DXGraphics& gfx):
	HudElement(L"Media\\Sprites\\WASDPanel.png", gfx)
{
	for (size_t i = 0; i < 4u; i++)panelPositions.emplace_back();
	panelPositions[0].emplace_back(-0.002f,   0.56f,  0.445f);
	panelPositions[1].emplace_back(-0.666f,  -0.638f, 0.445f);
	panelPositions[2].emplace_back(-0.004f,  -0.626f, 0.445f);
	panelPositions[3].emplace_back(0.666f,   -0.62f,  0.445f);
	BindPositionData(&myPosData);
}

void WASDPanel::SmartSubmit(FrameCommander& fc) const
{
	for (auto& s : panels) s.second->SmartSubmit(fc);
	this->Submit(fc, Techniques::Standart);
}

MainMenuHolder::MainMenuHolder(DXGraphics& gfx, const Menu& menu):
	WASDPanel(gfx),
	menu(menu)
{
	myPosData.emplace_back(0.0f, 0.0f, BigScale);
	panels.emplace(std::make_pair(PressableMenu::Key::W, std::make_unique<HudElement>(L"Media\\Sprites\\menu_play.png", gfx)));
	panels.emplace(std::make_pair(PressableMenu::Key::A, std::make_unique<HudElement>(L"Media\\Sprites\\menu_achievements.png", gfx)));
	panels.emplace(std::make_pair(PressableMenu::Key::S, std::make_unique<HudElement>(L"Media\\Sprites\\menu_tutorial.png", gfx)));
	panels.emplace(std::make_pair(PressableMenu::Key::D, std::make_unique<HudElement>(L"Media\\Sprites\\menu_quit.png", gfx)));

	panels.at(PressableMenu::Key::W)->BindPositionData(&panelPositions[0]);
	panels.at(PressableMenu::Key::A)->BindPositionData(&panelPositions[1]);
	panels.at(PressableMenu::Key::S)->BindPositionData(&panelPositions[2]);
	panels.at(PressableMenu::Key::D)->BindPositionData(&panelPositions[3]);
	for (auto& pan : panels) pan.second->AddParent(this, 0u);
}

void MainMenuHolder::SmartSubmit(FrameCommander& fc) const
{
	for (auto& s : panels) s.second->SmartSubmit(fc);
	this->Submit(fc, Techniques::Standart);
	const auto key = menu.GetPressedKey();
	if (key != PressableMenu::Key::None && key != PressableMenu::Key::All)
	{
		EffectHandler::AddEffect2D(panels.at(key)->GetCumulatedPosData(), EffectHandler::Effect::Shine);
		EffectHandler::AddEffect2D(panels.at(key)->GetCumulatedPosData(), EffectHandler::Effect::Sparks);
		menu.KeyAcquired();
	}
}

TutorialHolder::TutorialHolder(DXGraphics& gfx, const Menu& menu):
	WASDPanel(gfx),
	menu(menu)
{
	myPosData.emplace_back(0.0f, 0.0f, BigScale);
	panels.emplace(std::make_pair(PressableMenu::Key::W, std::make_unique<HudElement>(L"Media\\Sprites\\menu_howToPlay.png", gfx)));
	panels.emplace(std::make_pair(PressableMenu::Key::A, std::make_unique<HudElement>(L"Media\\Sprites\\menu_skills.png", gfx)));
	panels.emplace(std::make_pair(PressableMenu::Key::S, std::make_unique<HudElement>(L"Media\\Sprites\\menu_other.png", gfx)));
	panels.emplace(std::make_pair(PressableMenu::Key::D, std::make_unique<HudElement>(L"Media\\Sprites\\menu_back.png", gfx)));

	panels.at(PressableMenu::Key::W)->BindPositionData(&panelPositions[0]);
	panels.at(PressableMenu::Key::A)->BindPositionData(&panelPositions[1]);
	panels.at(PressableMenu::Key::S)->BindPositionData(&panelPositions[2]);
	panels.at(PressableMenu::Key::D)->BindPositionData(&panelPositions[3]);
	for (auto& pan : panels) pan.second->AddParent(this, 0u);
}

void TutorialHolder::SmartSubmit(FrameCommander& fc) const
{
	for (auto& s : panels) s.second->SmartSubmit(fc);
	this->Submit(fc, Techniques::Standart);
	const auto key = menu.GetPressedKey();
	if (key != PressableMenu::Key::None && key != PressableMenu::Key::All)
	{
		EffectHandler::AddEffect2D(panels.at(key)->GetCumulatedPosData(), EffectHandler::Effect::Shine);
		EffectHandler::AddEffect2D(panels.at(key)->GetCumulatedPosData(), EffectHandler::Effect::Sparks);
		menu.KeyAcquired();
	}
}

RewardMenu::RewardMenu(DXGraphics& gfx, const CombatManager& cm, const Menu& menu) :
	WASDPanel(gfx),
	cm(cm),
	menu(menu)
{
	myPosData.emplace_back(0.0f, 0.0f, MediumScale);
	panels.emplace(std::make_pair(PressableMenu::Key::W, std::make_unique<HudElement>(L"Media\\Sprites\\RewardBandage.png", gfx)));
	panels.emplace(std::make_pair(PressableMenu::Key::A, std::make_unique<HudElement>(L"Media\\Sprites\\RewardDodge.png", gfx)));
	panels.emplace(std::make_pair(PressableMenu::Key::S, std::make_unique<HudElement>(L"Media\\Sprites\\RewardUpgrade.png", gfx)));
	panels.emplace(std::make_pair(PressableMenu::Key::D, std::make_unique<HudElement>(L"Media\\Sprites\\RewardCrit.png", gfx)));

	panels.at(PressableMenu::Key::W)->BindPositionData(&panelPositions[0]);
	panels.at(PressableMenu::Key::A)->BindPositionData(&panelPositions[1]);
	panels.at(PressableMenu::Key::S)->BindPositionData(&panelPositions[2]);
	panels.at(PressableMenu::Key::D)->BindPositionData(&panelPositions[3]);
	for (auto& pan : panels) pan.second->AddParent(this, 0u);
}

void RewardMenu::SmartSubmit(FrameCommander& fc) const
{
	if (cm.GetRewardMenuActiveness())
	{
		for (auto& s : panels) s.second->SmartSubmit(fc);
		this->Submit(fc, Techniques::Standart);
	}
	const auto key = menu.GetPressedKey();
	if (key != PressableMenu::Key::None && key != PressableMenu::Key::All)
	{
		EffectHandler::AddEffect2D(panels.at(key)->GetCumulatedPosData(), EffectHandler::Effect::Shine);
		EffectHandler::AddEffect2D(panels.at(key)->GetCumulatedPosData(), EffectHandler::Effect::Sparks);
		menu.KeyAcquired();
	}
}

ABox::ABox(DXGraphics& gfx, const ActionBox& actBox) :
	actBox(actBox),
	WASDPanel(gfx),
	skillIconInfo(actBox.GetPickedSkillsInfo()),
	skillMicroiconPositionReferences(L"Media\\Sprites\\blank.png", gfx)
{
	skills.emplace(std::make_pair(Skill0::Cut,				std::make_unique<HudElement>(L"Media//Sprites//SkillSheet.png", gfx, 1, 7, 1)));
	skills.emplace(std::make_pair(Skill0::Deflect,			std::make_unique<HudElement>(L"Media//Sprites//SkillSheet.png", gfx, 2, 7, 1)));
	skills.emplace(std::make_pair(Skill0::SandStorm,		std::make_unique<HudElement>(L"Media//Sprites//SkillSheet.png", gfx, 3, 7, 1)));
	skills.emplace(std::make_pair(Skill0::SharpenTheBlade,	std::make_unique<HudElement>(L"Media//Sprites//SkillSheet.png", gfx, 4, 7, 1)));
	skills.emplace(std::make_pair(Skill0::SandsOfTime,		std::make_unique<HudElement>(L"Media//Sprites//SkillSheet.png", gfx, 5, 7, 1)));
	skills.emplace(std::make_pair(Skill0::DustThrow,		std::make_unique<HudElement>(L"Media//Sprites//SkillSheet.png", gfx, 6, 7, 1)));

	skills.emplace(std::make_pair(Skill0::CutBlocked,				std::make_unique<HudElement>(L"Media//Sprites//SkillSheetBlocked.png", gfx, 1, 7, 1)));
	skills.emplace(std::make_pair(Skill0::DeflectBlocked,			std::make_unique<HudElement>(L"Media//Sprites//SkillSheetBlocked.png", gfx, 2, 7, 1)));
	skills.emplace(std::make_pair(Skill0::SandStormBlocked,			std::make_unique<HudElement>(L"Media//Sprites//SkillSheetBlocked.png", gfx, 3, 7, 1)));
	skills.emplace(std::make_pair(Skill0::SharpenTheBladeBlocked,	std::make_unique<HudElement>(L"Media//Sprites//SkillSheetBlocked.png", gfx, 4, 7, 1)));
	skills.emplace(std::make_pair(Skill0::SandsOfTimeBlocked,		std::make_unique<HudElement>(L"Media//Sprites//SkillSheetBlocked.png", gfx, 5, 7, 1)));
	skills.emplace(std::make_pair(Skill0::DustThrowBlocked,			std::make_unique<HudElement>(L"Media//Sprites//SkillSheetBlocked.png", gfx, 6, 7, 1)));
	for (size_t i = 0; i < skills.size(); i++)posSkill.emplace_back();	
	for (size_t i = 0; i < skills.size(); i++)
	{
		skills.at(static_cast<Skill0>(i))->BindPositionData(&posSkill[i]);
		skills.at(static_cast<Skill0>(i))->AddParent(this);
	}
	myPosData.emplace_back(0.0f, -0.75f, 0.2f);
	BindPositionData(&myPosData);

	pos4.emplace_back(panelPositions[0][0]);
	pos4.emplace_back(panelPositions[1][0]);
	pos4.emplace_back(panelPositions[2][0]);
	pos4.emplace_back(panelPositions[3][0]);
	skillMicroiconPositionReferences.BindPositionData(&pos4);
	skillMicroiconPositionReferences.AddParent(this, 0);

	skillMicroicons.emplace(std::make_pair(SkillInfoMicroicons::Quick,		std::make_unique<SkillMicroicon>(gfx, L"Media//Sprites//skillIcon_quick.png")));
	skillMicroicons.emplace(std::make_pair(SkillInfoMicroicons::Rewinding,	std::make_unique<SkillMicroicon>(gfx, L"Media//Sprites//skillIcon_rewinding.png")));
	skillMicroicons.emplace(std::make_pair(SkillInfoMicroicons::TurnEnder,	std::make_unique<SkillMicroicon>(gfx, L"Media//Sprites//skillIcon_turnEnder.png")));
	skillMicroicons.emplace(std::make_pair(SkillInfoMicroicons::Upgraded,	std::make_unique<SkillMicroicon>(gfx, L"Media//Sprites//skillIcon_upgraded.png")));

	//adding reference panels for effects
	panels.emplace(std::make_pair(PressableMenu::Key::W, std::make_unique<HudElement>(L"Media\\Sprites\\blank.png", gfx)));
	panels.emplace(std::make_pair(PressableMenu::Key::A, std::make_unique<HudElement>(L"Media\\Sprites\\blank.png", gfx)));
	panels.emplace(std::make_pair(PressableMenu::Key::S, std::make_unique<HudElement>(L"Media\\Sprites\\blank.png", gfx)));
	panels.emplace(std::make_pair(PressableMenu::Key::D, std::make_unique<HudElement>(L"Media\\Sprites\\blank.png", gfx)));

	panels.at(PressableMenu::Key::W)->BindPositionData(&panelPositions[0]);
	panels.at(PressableMenu::Key::A)->BindPositionData(&panelPositions[1]);
	panels.at(PressableMenu::Key::S)->BindPositionData(&panelPositions[2]);
	panels.at(PressableMenu::Key::D)->BindPositionData(&panelPositions[3]);
	for (auto& pan : panels) pan.second->AddParent(this, 0u);
}

void ABox::SmartSubmit(FrameCommander& fc) const
{
	const auto dispSkills = actBox.GetPickedSkills();
	const auto skillMicroInfo = actBox.GetPickedSkillsInfo();

	for (size_t i = 0; i < 4u; i++)
	{
		posSkill[static_cast<size_t>(dispSkills.s[i])].clear();
	}
	for (size_t i = 0; i < 4u; i++)						//has to be separate loops, because if there are several instances of the same skill, all but one will not be drwan
	{
		posSkill[static_cast<size_t>(dispSkills.s[i])].push_back(pos4[i]);
	}

	skills.at(dispSkills.s[0])->SmartSubmit(fc);					//the purpose of this contraption is to avoid multiple submissions of the same skill
	if (dispSkills.s[0] != dispSkills.s[1])																				skills.at(dispSkills.s[1])->SmartSubmit(fc);
	if (dispSkills.s[0] != dispSkills.s[2] && dispSkills.s[1] != dispSkills.s[2])										skills.at(dispSkills.s[2])->SmartSubmit(fc);	
	if (dispSkills.s[0] != dispSkills.s[3] && dispSkills.s[1] != dispSkills.s[3] && dispSkills.s[2] != dispSkills.s[3])	skills.at(dispSkills.s[3])->SmartSubmit(fc);

	//submiting skillMicroicons
	for (auto& s : skillMicroicons) s.second->ClearDrawingPositions();
	for (size_t i = 0; i < skillMicroInfo.size(); i++)
	{
		if (dispSkills.s[i] < Skill0::CutBlocked)
		{
			size_t curIconInd = 0u;
			if (skillMicroInfo[i].at(SkillInfoMicroicons::TurnEnder))
			{
				skillMicroicons.at(SkillInfoMicroicons::TurnEnder)->AddDrawingPosition(curIconInd, &skillMicroiconPositionReferences, i);
				curIconInd++;
			}
			if (skillMicroInfo[i].at(SkillInfoMicroicons::Quick))
			{
				skillMicroicons.at(SkillInfoMicroicons::Quick)->AddDrawingPosition(curIconInd, &skillMicroiconPositionReferences, i);
				curIconInd++;
			}
			if (skillMicroInfo[i].at(SkillInfoMicroicons::Rewinding))
			{
				skillMicroicons.at(SkillInfoMicroicons::Rewinding)->AddDrawingPosition(curIconInd, &skillMicroiconPositionReferences, i);
				curIconInd++;
			}
			if (skillMicroInfo[i].at(SkillInfoMicroicons::Upgraded))
			{
				skillMicroicons.at(SkillInfoMicroicons::Upgraded)->AddDrawingPosition(curIconInd, &skillMicroiconPositionReferences, i);
				curIconInd++;
			}
		}
	}
	for (auto& s : skillMicroicons) s.second->SmartSubmit(fc);
	this->Submit(fc, Techniques::Standart);
	//submiting effects
	const auto key = actBox.GetPressedKey();
	if (key != PressableMenu::Key::None && key != PressableMenu::Key::All)			//some key pressed
	{
		EffectHandler::AddEffect2D(panels.at(key)->GetCumulatedPosData(), EffectHandler::Effect::Shine);
		EffectHandler::AddEffect2D(panels.at(key)->GetCumulatedPosData(), EffectHandler::Effect::Sparks);
		actBox.KeyAcquired();
	}
	else if (key != PressableMenu::Key::None)							//pull new skills was called
	{
		EffectHandler::AddEffect2D(panels.at(PressableMenu::Key::W)->GetCumulatedPosData(), EffectHandler::Effect::ShineOrange);
		EffectHandler::AddEffect2D(panels.at(PressableMenu::Key::A)->GetCumulatedPosData(), EffectHandler::Effect::ShineOrange);
		EffectHandler::AddEffect2D(panels.at(PressableMenu::Key::S)->GetCumulatedPosData(), EffectHandler::Effect::ShineOrange);
		EffectHandler::AddEffect2D(panels.at(PressableMenu::Key::D)->GetCumulatedPosData(), EffectHandler::Effect::ShineOrange);
		actBox.KeyAcquired();
	}
}

std::vector<std::vector<PositionData>> ABox::posSkill;