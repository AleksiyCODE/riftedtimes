#include "RandomUnit.h"

size_t RandomUnit::GetUInt(size_t low, size_t high)  
{
	std::uniform_int_distribution<size_t> dist(low, high);
	return (size_t)dist(rng);
}

float RandomUnit::GetFloat(float low, float high) 
{
	std::uniform_real_distribution<float> dist(low, high);
	return dist(rng);
}

float RandomUnit::GetFloat0to1() 
{
	static std::uniform_real_distribution<float> dist(0.0f ,1.0f);
	return dist(rng);
}

bool RandomUnit::GetBool()
{
	static std::uniform_int_distribution<size_t> dist(0u,1u);
	return static_cast<bool>(dist(rng));
}

