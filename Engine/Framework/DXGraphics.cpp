//mostly coded by Chili
#include "DXGraphics.h"
#include "dxerr.h"
#include "GraphicsThrowMacros.h"
#include <sstream>
#include <cmath>
#include <DirectXMath.h>
#include "../imgui/imgui_impl_dx11.h"
#include "../imgui/imgui_impl_win32.h"
#include "DepthStencil.h"

namespace wrl = Microsoft::WRL;
namespace dx = DirectX;
#pragma comment (lib,"d3d11.lib")
#pragma comment (lib, "D3DCompiler.lib")

DXGraphics::DXGraphics(HWND hWnd,int width,int height)
{
	DXGI_SWAP_CHAIN_DESC sd = {};
	sd.BufferDesc.Width = width;
	screenWidth = width;
	sd.BufferDesc.Height = height;
	screenHeight = height;
	sd.BufferDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
	sd.BufferDesc.RefreshRate.Numerator = 0;
	sd.BufferDesc.RefreshRate.Denominator = 0;
	sd.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
	sd.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	sd.SampleDesc.Count = 1;
	sd.SampleDesc.Quality = 0;
	sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	sd.BufferCount = 1;
	sd.OutputWindow = hWnd;
	sd.Windowed = TRUE;
	sd.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
	sd.Flags = 0;

	UINT swapCreateFlags = 0u;
#ifndef NDEBUG
	swapCreateFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

	// for checking results of d3d functions
	HRESULT hr;

	// create device and front/back buffers, and swap chain and rendering context
	GFX_THROW_INFO(D3D11CreateDeviceAndSwapChain(
		nullptr,
		D3D_DRIVER_TYPE_HARDWARE,
		nullptr,
		swapCreateFlags,
		nullptr,
		0,
		D3D11_SDK_VERSION,
		&sd,
		&pSwap,
		&pDevice,
		nullptr,
		&pContext
	));

	wrl::ComPtr<ID3D11Resource> pBackBuffer;
	GFX_THROW_INFO(pSwap->GetBuffer(0, __uuidof(ID3D11Resource), &pBackBuffer));
	GFX_THROW_INFO(pDevice->CreateRenderTargetView(pBackBuffer.Get(), nullptr, &pTarget));

	// viewport always fullscreen (for now)
	D3D11_VIEWPORT vp;
	vp.Width = (float)width;
	vp.Height = (float)height;
	vp.MinDepth = 0.0f;
	vp.MaxDepth = 1.0f;
	vp.TopLeftX = 0.0f;
	vp.TopLeftY = 0.0f;
	pContext->RSSetViewports(1u, &vp);

	// init imgui d3d impl
	ImGui_ImplDX11_Init(pDevice.Get(), pContext.Get());
}

DXGraphics::~DXGraphics()
{
	ImGui_ImplDX11_Shutdown();
}

void DXGraphics::EndFrame()
{
	// imgui frame end
	if (imguiEnabled)
	{
		ImGui::Render();
		ImGui_ImplDX11_RenderDrawData(ImGui::GetDrawData());
	}

	HRESULT hr;
#ifndef NDEBUG
	infoManager.Set();
#endif
	if (FAILED(hr = pSwap->Present(1u, 0u)))
	{
		if (hr == DXGI_ERROR_DEVICE_REMOVED)
		{
			throw GFX_DEVICE_REMOVED_EXCEPT(pDevice->GetDeviceRemovedReason());
		}
		else
		{
			throw GFX_EXCEPT(hr);
		}
	}
}
void DXGraphics::BeginFrame(float red, float green, float blue) noexcept
{
	// imgui begin frame
	if (imguiEnabled)
	{
		ImGui_ImplDX11_NewFrame();
		ImGui_ImplWin32_NewFrame();
		ImGui::NewFrame();
	}

	const float color[] = { red,green,blue,0.0f };
	pContext->ClearRenderTargetView(pTarget.Get(), color);
}

void DXGraphics::BindSwapBuffer() noexcept
{
	pContext->OMSetRenderTargets(1u, pTarget.GetAddressOf(), nullptr);
}

void DXGraphics::BindSwapBuffer(const DepthStencil& ds) noexcept
{
	pContext->OMSetRenderTargets(1u, pTarget.GetAddressOf(), ds.pDepthStencilView.Get());
}

void DXGraphics::DrawIndexed(UINT count) noxnd
{
	GFX_THROW_INFO_ONLY(pContext->DrawIndexed(count, 0u, 0u));
}

void DXGraphics::SetProjection(DirectX::FXMMATRIX proj) noexcept
{
	projection = proj;
}

DirectX::XMMATRIX DXGraphics::GetProjection() const noexcept
{
	return projection;
}

void DXGraphics::SetCamera(DirectX::FXMMATRIX cam) noexcept
{
	camera = cam;
}

DirectX::XMMATRIX DXGraphics::GetCamera() const noexcept
{
	return camera;
}

void DXGraphics::SetCamPos(DirectX::XMFLOAT3 cam) noexcept
{
	camPos = cam;
}

DirectX::XMFLOAT3 DXGraphics::GetCamPos() const noexcept
{
	return camPos;
}

void DXGraphics::EnableImgui() noexcept
{
	imguiEnabled = true;
}

void DXGraphics::DisableImgui() noexcept
{
	imguiEnabled = false;
}

bool DXGraphics::IsImguiEnabled() const noexcept
{
	return imguiEnabled;
}

// DXGraphics exception stuff
DXGraphics::HrException::HrException(int line, const wchar_t * file, HRESULT hr, std::vector<std::wstring> infoMsgs) noexcept
	:
	Exception(line, file),
	hr(hr)
{
	// join all info messages with newlines into single string
	for (const auto& m : infoMsgs)
	{
		info += m;
		info.push_back(L'\n');
	}
	// remove final newline if exists
	if (!info.empty())
	{
		info.pop_back();
	}
}

const wchar_t* DXGraphics::HrException::MyWhat() const noexcept
{
	std::wstringstream oss;
	oss << GetType() << std::endl
		<< L"[Error Code] 0x" << std::hex << std::uppercase << GetErrorCode()
		<< std::dec << " (" << (unsigned long)GetErrorCode() << ")" << std::endl
		<< L"[Error string] " << GetErrorString() << std::endl
		<< L"[Description] " << GetErrorDescription() << std::endl;
	if (!info.empty())
	{
		oss << L"\n[Error Info]\n" << GetErrorInfo() << std::endl << std::endl;
	}
	oss << GetOriginString();
	whatBuffer = oss.str();
	return whatBuffer.c_str();
}

const wchar_t* DXGraphics::HrException::GetType() const noexcept
{
	return L"Chili DXGraphics Exception";
}

HRESULT DXGraphics::HrException::GetErrorCode() const noexcept
{
	return hr;
}

std::wstring DXGraphics::HrException::GetErrorString() const noexcept
{
	return DXGetErrorString(hr);
}

std::wstring DXGraphics::HrException::GetErrorDescription() const noexcept
{
	wchar_t buf[512];
	DXGetErrorDescription(hr, buf, sizeof(buf));
	return buf;
}

std::wstring DXGraphics::HrException::GetErrorInfo() const noexcept
{
	return info;
}

const wchar_t* DXGraphics::DeviceRemovedException::GetType() const noexcept
{
	return L"Chili DXGraphics Exception [Device Removed] (DXGI_ERROR_DEVICE_REMOVED)";
}
DXGraphics::InfoException::InfoException(int line, const wchar_t * file, std::vector<std::wstring> infoMsgs) noexcept
	:
	Exception(line, file)
{
	// join all info messages with newlines into single string
	for (const auto& m : infoMsgs)
	{
		info += m;
		info.push_back(L'\n');
	}
	// remove final newline if exists
	if (!info.empty())
	{
		info.pop_back();
	}
}

const wchar_t* DXGraphics::InfoException::MyWhat() const noexcept
{
	std::wstringstream oss;
	oss << GetType() << std::endl
		<< L"\n[Error Info]\n" << GetErrorInfo() << std::endl << std::endl;
	oss << GetOriginString();
	whatBuffer = oss.str();
	return whatBuffer.c_str();
}

const wchar_t* DXGraphics::InfoException::GetType() const noexcept
{
	return L"Chili DXGraphics Info Exception";
}

std::wstring DXGraphics::InfoException::GetErrorInfo() const noexcept
{
	return info;
}
