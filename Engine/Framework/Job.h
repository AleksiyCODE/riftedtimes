#pragma once
#include "ConditionalNoexcept.h"

//coded by Chili
class Job
{
public:
	Job( const class Step* pStep,const class Drawable* pDrawable );
	void Execute( class DXGraphics& gfx ) const noxnd;
private:
	const class Drawable* pDrawable;
	const class Step* pStep;
};