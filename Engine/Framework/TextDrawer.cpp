#include "TextDrawer.h"

TextDrawer::TextDrawer(DXGraphics& gfx, FrameCommander& fc):fc(fc)
{
	letData.reserve(numLetters);
	for (wchar_t i = 0; i < numLetters; i++)
	{
		std::vector<LetterData> lDat;
		letData.push_back(std::move(lDat));
		letData[i].reserve(15);				//test: try more than 15
		lettersToDraw.emplace_back(L"Media//Sprites//font.png", gfx, i+L' ');
		lettersToDraw[i].BindLetterData(&letData[i]);
	}
}

void TextDrawer::DrawString(std::wstring outputText, Vec2 position, Color c, float scale,float rot)
{
	Vec2 curPos = position;
	for (size_t nCh = 0; nCh < outputText.size(); nCh++)
	{
		if (outputText[nCh] == L'\n')
		{
			curPos.y -= charHeight * scale;
			curPos.x = position.x;
		}
		else
		{
			assert(outputText[nCh] >= firstChar);
			assert(outputText[nCh] <= lastChar);
			if (outputText[nCh] != ' ')
			{
				const int chIndex = outputText[nCh] - L' ';
				LetterData ld{ {curPos.x, curPos.y} , rot, scale, {(float)c.GetR()/256.0f, (float)c.GetG() / 256.0f, (float)c.GetB() / 256.0f } };
				letData[outputText[nCh]-L' '].push_back(std::move(ld));
			}
			curPos.x += charWidth*scale ;
		}
	}
}
void TextDrawer::DrawString(StringData sd)
{
	DrawString(sd.outputText, sd.position, sd.c, sd.scale);
}
void TextDrawer::DrawStringTimed(std::wstring outputText, Vec2 position, Color c, float scale,  float time)
{
	timedStrings.emplace_back(outputText, position, c, scale, time, 0.0f);
}
void TextDrawer::DrawStringAscending(std::wstring outputText, Vec2 position, Color c, float scale, float time, float speed)
{
	timedStrings.emplace_back(outputText, position, c, scale, time, speed);
}
void TextDrawer::SubmitText(float dt)
{
	for (auto& str : timedStrings)
	{
		str.time -= dt;
		DrawString(str);
	}
	timedStrings.erase(std::remove_if(timedStrings.begin(), timedStrings.end(),
		[](StringData& sd) {return sd.time < 0.0f; }), timedStrings.end());
	for (auto& ts : timedStrings)
	{
		ts.position.y += ts.ascendingSpeed * dt;
	}
	for (auto& l : lettersToDraw)
	{
		l.SmartSubmit(fc);
	}
}
void TextDrawer::EndFrame()
{
	for (auto& ld : letData)ld.clear();
}
 std::vector<Letter> TextDrawer::lettersToDraw;
 std::vector<std::vector<LetterData>>  TextDrawer::letData;
 std::vector<TextDrawer::StringData> TextDrawer::timedStrings;