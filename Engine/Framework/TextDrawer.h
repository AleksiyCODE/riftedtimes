#pragma once
#include "Vec2.h"
#include <string>
#include "Surface.h"
#include "Letter.h"
#define TD TextDrawer
// !"#$%&'()*+,-./0123456789:;<=>?
//@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_
//`abcdefghijklmnopqrstuvwxyz{|}~ 

class TextDrawer	//only draws fixed width text
{
public:
	struct StringData
	{
		StringData(std::wstring str, Vec2 pos, Color c, float scale, float time,float speed) :
			outputText(str), position(pos), c(c), scale(scale), time(time), ascendingSpeed(speed) {};
		std::wstring outputText;
		Vec2 position;
		Color c;
		float scale;
		float time;
		float ascendingSpeed;
	};
	TextDrawer(DXGraphics& gfx,FrameCommander& fc);
	static void DrawString(std::wstring outputText, Vec2 position, Color c, float scale = 0.05f,float rot = 0.0f);		//rotation is not implemented
	static void DrawString(StringData sd);
	static void DrawStringTimed(std::wstring outputText, Vec2 position, Color c, float scale = 0.02f,  float time = 1.0f);	//pust a string on the screen that lasts specified time
	static void DrawStringAscending(std::wstring outputText, Vec2 position, Color c, float scale = 0.02f,  float time = 1.0f, float speed = 0.3f);	//pust a string on the screen that lasts specified time and ascends with specified speed
	void SubmitText(float dt);
	void EndFrame();
private:
	static constexpr float charWidth = 0.6f;
	static constexpr float charHeight = 2.0f;
	FrameCommander& fc;
	static constexpr wchar_t firstChar = L' ';
	static constexpr wchar_t lastChar = L'~';
	static constexpr size_t numLetters = 96;
	static std::vector<StringData> timedStrings;
	static std::vector<Letter>lettersToDraw;			//one element per drawable symbol
	static std::vector<std::vector<LetterData>> letData;	//one vector of data per drawable symbol
};