#pragma once
#include "Framework\PlaneTexNorm.h"
#include "Framework\Rect.h"
class CeilingHangers
{
public:
	CeilingHangers(DXGraphics& gfx, RectF spawnArea, size_t numHangers);
	void Submit(FrameCommander& fc);
private:
	std::vector<PositionData> posData;
	PlaneTexNorm model;
};

