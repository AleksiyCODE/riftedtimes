#pragma once
enum class AbilityType
{
	NoType,
	Attack,
	Buff,
	Block
};