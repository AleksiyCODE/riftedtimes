//coded by Chili
#pragma once
#include "DXGraphics.h"
#include "ConditionalNoexcept.h"
#include "GraphicsResource.h"

class Drawable;

namespace Bind
{
	class Bindable : public GraphicsResource
	{
	public:
		virtual void Bind(DXGraphics& gfx) noexcept = 0;
		virtual void InitializeParentReference(const Drawable&) noexcept
		{}
		virtual std::wstring GetUID() const noexcept
		{
			assert(false&&"this Bindable does not have UID");
			return L"";
		}
		virtual ~Bindable() = default;
	};
}