#include "LevelLayout.h"
#include "RandomUnit.h"
#include "EffectHandler.h"

LevelLayout::Enemy::Enemy(DXGraphics& gfx, Monster en) 
{
	switch (en)
	{
	case LevelLayout::Enemy::Monster::Ghoul:
		planes[Model::GhoulAttack] = std::make_unique<PlaneTexNorm>(L"Media//Models//GhoulAttack.obj", gfx);
		planes[Model::GhoulAttack]->BindPositionData(&attackPosData);

		planes[Model::GhoulRage] = std::make_unique<PlaneTexNorm>(L"Media//Models//GhoulRage.obj", gfx);
		planes[Model::GhoulRage]->BindPositionData(&ragePosData);
		
		planes[Model::GhoulCorpse] = std::make_unique<PlaneTexNorm>(L"Media//Models//GhoulCorpse.obj", gfx,0.25f);
		planes[Model::GhoulCorpse]->BindPositionData(&corpsePosData);
		
		planes[Model::GhoulCover] = std::make_unique<PlaneTexNorm>(L"Media//Models//GhoulCover.obj", gfx);
		planes[Model::GhoulCover]->BindPositionData(&coverPosData);
		
		planes[Model::GhoulPreparingCover] = std::make_unique<PlaneTexNorm>(L"Media//Models//GhoulPrepareDefence.obj", gfx);
		planes[Model::GhoulPreparingCover]->BindPositionData(&prepareCoverPosData);
		
		planes[Model::GhoulPrepareAttack] = std::make_unique<PlaneTexNorm>(L"Media//Models//GhoulPrepareAttack.obj", gfx);
		planes[Model::GhoulPrepareAttack]->BindPositionData(&prepareAttackPosData);
		
		planes[Model::GhoulPrepareRage] = std::make_unique<PlaneTexNorm>(L"Media//Models//GhoulPrepareRage.obj", gfx);
		planes[Model::GhoulPrepareRage]->BindPositionData(&prepareRagePosData);

		planes[Model::GhoulIdle] = std::make_unique<PlaneTexNorm>(L"Media//Models//GhoulIdle.obj", gfx);
		planes[Model::GhoulIdle]->BindPositionData(&idlePosData);
		break;
	case LevelLayout::Enemy::Monster::Last:
		assert(false && "missed monster type in LevelLayout::Enemy");
		break;
	default:
		assert(false && "unspecifiedmonster type in LevelLayout::Enemy");
		break;
	}
}


void LevelLayout::Enemy::Submit(FrameCommander& fc)
{
	attackPosData.clear();
	ragePosData.clear();
	coverPosData.clear();
	corpsePosData.clear();
	prepareAttackPosData.clear();
	prepareRagePosData.clear();
	prepareCoverPosData.clear();
	idlePosData.clear();
	for (size_t i = 0; i < positionData->size(); i++)
	{
		switch (modelData->at(i))
		{
		case LevelLayout::Enemy::Model::GhoulAttack:
			attackPosData.push_back(positionData->at(i));
			break;
		case LevelLayout::Enemy::Model::GhoulRage:
			ragePosData.push_back(positionData->at(i));
			break;
		case LevelLayout::Enemy::Model::GhoulCover:
			coverPosData.push_back(positionData->at(i));
			break;
		case LevelLayout::Enemy::Model::GhoulCorpse:
			corpsePosData.push_back(positionData->at(i));
			break;
		case LevelLayout::Enemy::Model::GhoulPrepareAttack:
			prepareAttackPosData.push_back(positionData->at(i));
			break;
		case LevelLayout::Enemy::Model::GhoulPrepareRage:
			prepareRagePosData.push_back(positionData->at(i));
			break;
		case LevelLayout::Enemy::Model::GhoulPreparingCover:
			prepareCoverPosData.push_back(positionData->at(i));
			break;
		case LevelLayout::Enemy::Model::GhoulIdle:
			idlePosData.push_back(positionData->at(i));
			break;
		case LevelLayout::Enemy::Model::NotDrawing:
			break;
		case LevelLayout::Enemy::Model::Last:
			assert(false && "Bad model in LevelLayout::Enemy::Submit");
			break;
		default:
			assert(false && "missed a model case in LevelLayout::Enemy::Submit");
			break;
		}
	}
	if (!attackPosData.empty())			planes[Model::GhoulAttack]->		SmartSubmit(fc, Techniques::CamFacing);
	if (!ragePosData.empty())			planes[Model::GhoulRage]->			SmartSubmit(fc, Techniques::CamFacing);
	if (!coverPosData.empty())			planes[Model::GhoulCover]->			SmartSubmit(fc, Techniques::CamFacing);
	if (!corpsePosData.empty())			planes[Model::GhoulCorpse]->		SmartSubmit(fc, Techniques::CamFacing);
	if (!prepareAttackPosData.empty())	planes[Model::GhoulPrepareAttack]->	SmartSubmit(fc, Techniques::CamFacing);
	if (!prepareRagePosData.empty())	planes[Model::GhoulPrepareRage]->	SmartSubmit(fc, Techniques::CamFacing);
	if (!prepareCoverPosData.empty())	planes[Model::GhoulPreparingCover]->SmartSubmit(fc, Techniques::CamFacing);
	if (!idlePosData.empty())			planes[Model::GhoulIdle]->			SmartSubmit(fc, Techniques::CamFacing);
}

LevelLayout::LevelLayout(DXGraphics& gfx, FrameCommander& fc, const CombatManager& cm) :
	hangers(gfx, {7.0f, 4.0f, 2.9f, -0.9f}, 8u),
	gfx(gfx),
	fc(fc),
	cm(cm),
	sl(gfx)
{
	wallModels.emplace_back(L"Media\\Models\\stone_wall.obj", gfx);
	wallModels.emplace_back(L"Media\\Models\\stone_floor.obj", gfx);
	wallModels.emplace_back(L"Media\\Models\\stone_ceiling.obj", gfx);	
	enemies.emplace_back(gfx);

	wallPositions.reserve(static_cast<size_t>(Wall::Model::Last));
	for (size_t i = static_cast<size_t>(Wall::Model::StoneWall);
		i < static_cast<size_t>(Wall::Model::Last); i++)
	{
		std::vector<PositionData> vPos;
		wallPositions.push_back(std::move(vPos));
		wallPositions[i].reserve(50);		//put expected number of walls of the same type here
		wallModels[i].BindModelToPositions(&wallPositions[i]);
	}
	enemiesForDrawing.reserve(static_cast<size_t>(Enemy::Monster::Last));
	for (size_t i = static_cast<size_t>(Enemy::Monster::Ghoul);
		i < static_cast<size_t>(Enemy::Monster::Last); i++)
	{
		std::pair<std::vector<PositionData>, std::vector<Enemy::Model>> vPos;
		enemiesForDrawing.push_back(vPos);
		enemiesForDrawing[i].first.reserve(7);		//put expected number of enemies (including dead) of the same type here
		enemiesForDrawing[i].second.reserve(7);		
		enemies[i].SetDrawingData(&enemiesForDrawing[i].first, &enemiesForDrawing[i].second);
	}
	//behid camera
	AddWall(Wall::Model::StoneWall, Wall::Type::Wall, { 0,2 }, Wall::Direction::Forward);
	AddWall(Wall::Model::StoneWall, Wall::Type::Wall, { 0,0 }, Wall::Direction::Forward);
	//coridor			 
	AddWall(Wall::Model::StoneWall, Wall::Type::Wall, { 1,3 }, Wall::Direction::Right);
	AddWall(Wall::Model::StoneWall, Wall::Type::Wall, { 1,-1 }, Wall::Direction::Left);
	AddWall(Wall::Model::StoneWall, Wall::Type::Wall, { 3,3 }, Wall::Direction::Right);
	AddWall(Wall::Model::StoneWall, Wall::Type::Wall, { 3,-1 }, Wall::Direction::Left);
	AddWall(Wall::Model::StoneWall, Wall::Type::Wall, { 5,3 }, Wall::Direction::Right);
	AddWall(Wall::Model::StoneWall, Wall::Type::Wall, { 5,-1 }, Wall::Direction::Left);
	AddWall(Wall::Model::StoneWall, Wall::Type::Wall, { 7,3 }, Wall::Direction::Right);
	AddWall(Wall::Model::StoneWall, Wall::Type::Wall, { 7,-1 }, Wall::Direction::Left);
	//in front of camera 
	AddWall(Wall::Model::StoneWall, Wall::Type::Wall, { 8,0 }, Wall::Direction::Backwards);
	AddWall(Wall::Model::StoneWall, Wall::Type::Wall, { 8,2 }, Wall::Direction::Backwards);
	//floor
	AddWall(Wall::Model::StoneFloor, Wall::Type::Floor, { 1,0 });
	AddWall(Wall::Model::StoneFloor, Wall::Type::Floor, { 1,2 });
	AddWall(Wall::Model::StoneFloor, Wall::Type::Floor, { 3,0 });
	AddWall(Wall::Model::StoneFloor, Wall::Type::Floor, { 3,2 });
	AddWall(Wall::Model::StoneFloor, Wall::Type::Floor, { 5,0 });
	AddWall(Wall::Model::StoneFloor, Wall::Type::Floor, { 5,2 });
	AddWall(Wall::Model::StoneFloor, Wall::Type::Floor, { 7,0 });
	AddWall(Wall::Model::StoneFloor, Wall::Type::Floor, { 7,2 });
	//ceiling
	AddWall(Wall::Model::StoneCeiling, Wall::Type::Ceiling, { 1,0 });
	AddWall(Wall::Model::StoneCeiling, Wall::Type::Ceiling, { 1,2 });
	AddWall(Wall::Model::StoneCeiling, Wall::Type::Ceiling, { 3,0 });
	AddWall(Wall::Model::StoneCeiling, Wall::Type::Ceiling, { 3,2 });
	AddWall(Wall::Model::StoneCeiling, Wall::Type::Ceiling, { 5,0 });
	AddWall(Wall::Model::StoneCeiling, Wall::Type::Ceiling, { 5,2 });
	AddWall(Wall::Model::StoneCeiling, Wall::Type::Ceiling, { 7,0 });
	AddWall(Wall::Model::StoneCeiling, Wall::Type::Ceiling, { 7,2 });

	AddEnemy(Enemy::Monster::Ghoul, { 4,1 });

	EH::AddEffect3D(EH::Effect::AmbientPermanentBackgroundSmoke);			//just adding permanent smoke
}

void LevelLayout::AddWall(Wall::Model mod, Wall::Type type, DirectX::XMFLOAT2 pos, Wall::Direction dir)
{
	PositionData posData;
	switch (type)
	{
	case Wall::Type::Wall:
		switch (dir)
		{
			//rotating
		case Wall::Direction::Left:
			posData.rot = { 0.0f, PI / 2, PI / 2 };
			break;
		case Wall::Direction::Right:
			posData.rot = { 0.0f, 3 * PI / 2, PI / 2 };
			break;
		case Wall::Direction::Forward:
			posData.rot = { 0.0f, PI, PI / 2 };
			break;
		case Wall::Direction::Backwards:
			posData.rot = { 0.0f, 0.0f, PI / 2 };
			break;
		default:
			assert(false && "WallFacesInvalidDirection");
			break;
		}
		//positioning
		posData.pos = { pos.x,1.0f,pos.y };
		break;
	case Wall::Type::Floor:
		//already rotated properly
		//positioning
		posData.pos = { pos.x,0.0f,pos.y };
		break;
	case Wall::Type::Ceiling:
		//rotating
		posData.rot = { 0.0f, 0.0f, PI };
		//positioning
		posData.pos = { pos.x,2.0f,pos.y };
		break;
	default:
		assert(false && "InvalidWallType");
		break;
	}
	wallPositions[static_cast<size_t>(mod)].push_back(posData);
}

void LevelLayout::AddEnemy(Enemy::Monster monster, DirectX::XMFLOAT2 pos, bool isDead)		//right now only adds corpses
{
	PositionData posData;
	posData.rot = { 0.0f,  3 * PI / 2, PI / 2 };				//this rotation is used for the camera facing technique to work correctly
	posData.pos = { pos.x,1.0f ,pos.y };						//collapse?
	const auto model = isDead ? Enemy::Model::GhoulCorpse : Enemy::Model::GhoulRage;
	enemiesForDrawing[static_cast<size_t>(monster)].first.push_back(std::move(posData));
	enemiesForDrawing[static_cast<size_t>(monster)].second.push_back(model);
}

void LevelLayout::RemoveCorpses()
{
	while(enemiesForDrawing[0].first.size()>1u)
	{
		enemiesForDrawing[0].first.pop_back();
		enemiesForDrawing[0].second.pop_back();
	}
}

void LevelLayout::SubmitDrawables()
{
	auto state = cm.GetEnemyState();
	if (state == EnemyCombat::State::GhoulDead && !enemyWasDead)		//adding corpses
	{
		enemyWasDead = true;
	}
	else if (state != EnemyCombat::State::GhoulDead && enemyWasDead)
	{
		enemyWasDead = false;
		AddEnemy(LevelLayout::Enemy::Monster::Ghoul, { RU.GetFloat(6.0f, 7.5f), RU.GetFloat(0.0f,2.0f) }, true);
	}
	else if (state == EnemyCombat::State::NoEntity && !cm.GetRewardMenuActiveness())		//removing corpses
	{
		RemoveCorpses();
	}
	switch (state)
	{
	case EnemyCombat::State::GhoulAttack:
		enemiesForDrawing[0].second[0] = Enemy::Model::GhoulAttack;		//first ghoul that was added to this container will be the one changing poses
		if (previousState != EnemyCombat::State::GhoulAttack)EH::AddEffect3D(EH::Effect::GhoulAttack);
		break;
	case EnemyCombat::State::GhoulRage:
		enemiesForDrawing[0].second[0] = Enemy::Model::GhoulRage;
		break;
	case EnemyCombat::State::GhoulDead:
		enemiesForDrawing[0].second[0] = Enemy::Model::GhoulCorpse;
		break;
	case EnemyCombat::State::GhoulPreparingAttack:
		enemiesForDrawing[0].second[0] = Enemy::Model::GhoulPrepareAttack;
		break;
	case EnemyCombat::State::GhoulPreparingRage:
		enemiesForDrawing[0].second[0] = Enemy::Model::GhoulPrepareRage;
		break;
	case EnemyCombat::State::GhoulCovering:
		enemiesForDrawing[0].second[0] = Enemy::Model::GhoulCover;
		break;
	case EnemyCombat::State::GhoulPreparingCover:
		enemiesForDrawing[0].second[0] = Enemy::Model::GhoulPreparingCover;
		break;
	case EnemyCombat::State::GhoulIdle:
		enemiesForDrawing[0].second[0] = Enemy::Model::GhoulIdle;
		break;
	case EnemyCombat::State::NoEntity:
	case EnemyCombat::State::GhoulInvisible:
		enemiesForDrawing[0].second[0] = Enemy::Model::NotDrawing;
		break;
	default:
		assert(false && "missed the satate in LevelLayout::SubmitDrawables");
		break;
	}
	for (auto& w : wallModels) w.Submit(fc);		
	enemies[0].Submit(fc);	
	sl.SubmitLightHolders(fc);
	hangers.Submit(fc);
	previousState = state;
}

void LevelLayout::UpdateAndBindLight(float dt, DirectX::XMMATRIX cameraTransform)
{
	sl.UpdateAndBind(dt, gfx, cameraTransform);
	camMan.Update(dt);
}

DirectX::XMMATRIX LevelLayout::GetCameraMatrix() const noexcept
{
	return camMan.GetMatrix();
}

DirectX::XMFLOAT3 LevelLayout::GetCameraPosition() const noexcept
{
	return camMan.GetPosition();
}
