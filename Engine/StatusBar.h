#pragma once
#include "HPBar.h"
#include "CombatManager.h"
#include "StatusIcons.h"
class StatusBar : public HudElement
{
public:
	StatusBar(const CombatData&  cd,DXGraphics& gfx, PositionData pos, StatusOwner owner);
	virtual ~StatusBar() = default;

	virtual void UpdateStatus(DXGraphics& gfx);

	void SmartSubmit(FrameCommander& fc)const override = 0;	
protected:
	std::unique_ptr<EntityBar> bar;		
	const CombatData& cd;
	mutable CombatData previousCD;
	StatusOwner owner;
	Vec2 blockOffset;
	Vec2 healthOffset;
	static constexpr float textSpeed = 0.15f;
	static constexpr float textSize  = 0.028f;
	static constexpr float ascSize = 0.036f;
};

class PlayerStatusBar : public StatusBar
{
public:
	PlayerStatusBar(const CombatData& cd, DXGraphics& gfx) :
		StatusBar(cd, gfx, PositionData(-0.6f, -0.8f, 0.35f),StatusOwner::Player),
		bleedIcon(gfx, StatusOwner::Player),
		critIcon( gfx, StatusOwner::Player),
		dodgeIcon(gfx, StatusOwner::Player)
	{};
	void SmartSubmit(FrameCommander& fc)const override;
	mutable BleedIcon bleedIcon;
	mutable CritIcon critIcon;
	mutable DodgeIcon dodgeIcon;
};

class EnemyStatusBar : public StatusBar
{
public:
	EnemyStatusBar(const CombatData& cd, DXGraphics& gfx) :
		StatusBar(cd, gfx, PositionData(-0.6f, 0.8f, 0.35f), StatusOwner::Enemy),
		accIcon  (gfx, StatusOwner::Enemy),
		bleedIcon(gfx, StatusOwner::Enemy)
	{};
	void SmartSubmit(FrameCommander& fc)const override;
private:
	mutable AccuracyIcon accIcon;
	mutable BleedIcon bleedIcon;
};