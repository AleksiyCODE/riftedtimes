//partly coded by Chili
#pragma once
#include "DXGraphics.h"

struct CamTilt
{
	float roll = 0.0f;
	float pitch = 0.0f;
	float yaw = 0.0f;
	CamTilt& operator+=(const CamTilt& rhs)
	{
		roll += rhs.roll;
		pitch += rhs.pitch;
		yaw += rhs.yaw;
		return *this;
	} 
	friend CamTilt operator+(CamTilt lhs, const CamTilt& rhs)
	{
		lhs += rhs; 
		return lhs; 
	}
	CamTilt operator*(float f)
	{
		CamTilt ct = *this;
		ct.roll *= f;
		ct.pitch *= f;
		ct.yaw *= f;
		return ct;
	}
	CamTilt& operator*=(float f)
	{
		roll *=  f;
		pitch *= f;
		yaw *=   f;
		return *this;
	}
};
struct CamMove
{
	float x = 0.0f;
	float y = 0.0f;
	float z = 0.0f;
	CamMove& operator+=(const CamMove& rhs)
	{
		x += rhs.x;
		y += rhs.y;
		z += rhs.z;
		return *this;
	}
	friend CamMove operator+(CamMove lhs, const CamMove& rhs)
	{
		lhs += rhs;
		return lhs;
	}	
	CamMove operator*(float f)
	{
		CamMove cm = *this;
		cm.x *= f;
		cm.y *= f;
		cm.z *= f;
		return cm;
	}	
	CamMove& operator*=(float f)
	{
		x *= f;
		y *= f;
		z *= f;
		return *this;
	}
};
struct CamTrans
{
	CamTilt tilt;
	CamMove pos;
	CamTrans& operator+=(const CamTrans& rhs)
	{
		tilt += rhs.tilt;
		pos += rhs.pos;
		return *this;
	}
	friend CamTrans operator+(CamTrans lhs, const CamTrans& rhs)
	{
		lhs += rhs;
		return lhs;
	}
	CamTrans operator*(float f)
	{
		CamTrans ct = *this;
		ct.tilt *= f;
		ct.pos *= f;
		return ct;
	}	
};
class Camera
{
public:
	Camera() noexcept;
	DirectX::XMMATRIX GetMatrix() const noexcept;
	DirectX::XMFLOAT3 GetPosition() const noexcept;
	void SpawnControlWindow() noexcept;
	void Reset() noexcept;
	void SetFullTransform(CamTrans in_trans);
	void Rotate( float dx,float dy ) noexcept;
	void Translate( DirectX::XMFLOAT3 translation ) noexcept;
private:
	DirectX::XMFLOAT3 pos;
	float roll;
	float pitch;
	float yaw;
	static constexpr float travelSpeed = 7.0f;
	static constexpr float rotationSpeed = 0.004f;
};