#pragma once
#include "Framework/Camera.h"
#include <queue>
#define CM CameraManager
class CameraManager
{
	typedef std::queue<std::tuple<CamTilt, CamMove, float>> CamSequence;
	class CameraScenario
	{
	public:
		CameraScenario(CameraManager::CamSequence cs);
		CameraScenario(CameraScenario&&) = default;
		CameraScenario(const CameraScenario&) = default;
		CameraScenario& operator=(CameraScenario&&) = default;
		CameraScenario& operator=(const CameraScenario&) = default;
		bool Update(float dt);				//returns 1 if all stages are passed
		CamTrans GetCamData();
	private:
		CameraManager::CamSequence stages;
		float stageTime = 0.0f;
	};
public:
	CameraManager();
	enum class Scenario
	{
		ThrowSand,
		UnsettleDust,
		Strike,
		Defend,
		GhoulRage,
		GhoulAttack,
		GhoulAttackMiss,
		GhoulAttackBlock,
		BattleBegins,
		Death
	};
	static void PendScenario(Scenario s);
	void Update(float dt);
	DirectX::XMMATRIX GetMatrix() const noexcept;			
	DirectX::XMFLOAT3 GetPosition() const noexcept;

	//for debugging
	void Rotate(float dx, float dy) noexcept
	{
		baseTrans.tilt.yaw +=dx;
		baseTrans.tilt.pitch += dx;
	}
	void Translate(DirectX::XMFLOAT3 translation) noexcept
	{
		baseTrans.pos.x += translation.x;
		baseTrans.pos.y += translation.y;
		baseTrans.pos.z += translation.z;
	}

private:
	CamTrans baseTrans;
	CamTrans intermedTrans;
	CamTrans curTrans;
	static constexpr float speedCoeff = 10.0f;
	Camera cam;
	std::vector<CameraScenario> scenarious;
	static std::vector<Scenario> pendingScenarios;
};        