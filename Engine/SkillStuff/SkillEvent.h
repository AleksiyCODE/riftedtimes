#pragma once
#include "../EventType.h"
struct SkillEvent
{	
	SkillEvent(EventType eType, float valLow=0, float valHigh=0):
		valLow(valLow),
		valHigh(valHigh),
		eventType(eType)
	{};
	~SkillEvent() = default;
	float valLow;	
	float valHigh;
	EventType eventType;
};