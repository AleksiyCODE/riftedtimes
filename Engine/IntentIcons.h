#pragma once
#include "Framework\HudElement.h"
#include "SkillStuff\AbilityGeneralTypes.h"
#include "CombatManager.h"
#include "EffectHandler.h"

class IntentIconManager : public HudElement
{
public:
	IntentIconManager(const  CombatData& cd, DXGraphics& gfx):
		cd(cd)
	{};
	void SmartSubmit(FrameCommander& fc) const override;
private:
	const CombatData& cd;
	mutable AbilityType currentIcon = AbilityType::NoType;
	mutable AbilityType previousIcon = AbilityType::NoType;
};

