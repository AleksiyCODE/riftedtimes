//coded by Chili
#pragma once
#include "BindableCommon.h"
#include "ChiliMath.h"

class BlurPack
{
public:
	BlurPack(DXGraphics& gfx, int radius = 7, float sigma = 2.6f, std::wstring shader = L"Shaders\\BlurPS.cso")	:
		shader(gfx, shader),
		pcb(gfx, 0u),
		ccb(gfx, 1u),
		radius(radius),
		sigma(sigma)
	{
		SetKernelGauss(gfx, radius, sigma);
	}
	void Bind(DXGraphics& gfx) noexcept
	{
		shader.Bind(gfx);
		pcb.Bind(gfx);
		ccb.Bind(gfx);
	}
	void SetHorizontal(DXGraphics& gfx)
	{
		ccb.Update(gfx, { TRUE });
	}
	void SetVertical(DXGraphics& gfx)
	{
		ccb.Update(gfx, { FALSE });
	}
	// for more accurate coefs, need to integrate, but meh :/
	void SetKernelGauss(DXGraphics& gfx, size_t radius, float sigma) noxnd
	{
		assert(radius <= maxRadius);
		Kernel k;
		k.nTaps = (UINT)radius * 2u + 1u;
		float sum = 0.0f;
		for (UINT i = 0; i < k.nTaps; i++)
		{
			const auto x = float((int)i - (int)radius);
			const auto g = gauss(x, sigma);
			sum += g;
			k.coefficients[i].x = g;
		}
		for (UINT i = 0; i < k.nTaps; i++)
		{
			k.coefficients[i].x /= sum;
		}
		pcb.Update(gfx, k);
	}
	void SetKernelBox(DXGraphics& gfx, int radius) noxnd
	{
		assert(radius <= maxRadius);
		Kernel k;
		k.nTaps = radius * 2 + 1;
		const float c = 1.0f / k.nTaps;
		for (UINT i = 0; i < k.nTaps; i++)
		{
			k.coefficients[i].x = c;
		}
		pcb.Update(gfx, k);
	}
private:
	enum class KernelType
	{
		Gauss,
		Box,
	};
	static constexpr size_t maxRadius = 15;
	int radius;
	float sigma;
	KernelType kernelType = KernelType::Gauss;
	struct Kernel
	{
		UINT nTaps;
		float padding[3];
		DirectX::XMFLOAT4 coefficients[maxRadius * 2 + 1];
	};
	struct Control
	{
		BOOL horizontal;
		float padding[3];
	};
	Bind::PixelShader shader;
	Bind::PixelConstantBuffer<Kernel> pcb;
	Bind::PixelConstantBuffer<Control> ccb;
};