#include "CameraManager.h"
#include "Framework\ChiliMath.h"
#include "SkillStuff/Skills0.h"
#include "EffectHandler.h"

CameraManager::CameraManager():
	baseTrans({0.0f,PI/2.0f,0.0f,1.0f,1.0f,1.0f})
{
	curTrans = baseTrans;
	intermedTrans = baseTrans;
}

void CameraManager::PendScenario(Scenario s)
{
	pendingScenarios.push_back(s);
}

void CameraManager::Update(float dt)
{
	if (!pendingScenarios.empty())
	{
		switch (pendingScenarios.back())
		{
		case Scenario:: ThrowSand:				//called from player skill
		{
			CamSequence cs;
			float v0 = 0.18f;
			float v1 = 0.67f;
			auto angle = RU.GetFloat(0.0f,2.0f*PI);
			cs.push(std::tuple<CamTilt, CamMove, float>({ v0*sin(angle),v0*cos(angle),0.0f },	    { 0.0f,0.0f,0.0f }, DustThrow::step0));
			cs.push(std::tuple<CamTilt, CamMove, float>({ -v1 * sin(angle),-v1 * cos(angle),0.0f }, { 0.0f,0.0f,0.0f }, DustThrow::step1));
			scenarious.emplace_back(cs);
			break;
		}
		case Scenario::UnsettleDust:			//called from player skill
		{
			CamSequence cs;
			cs.push(std::tuple<CamTilt, CamMove, float>({ 0.2f,0.0f,0.0f }, { 0.0f,-0.4f,0.0f }, SandStorm::step0));
			cs.push(std::tuple<CamTilt, CamMove, float>({ -0.1f,-0.0f,0.0f }, { 0.0f,0.0f,0.0f }, SandStorm::step1));
			scenarious.emplace_back(cs);
			break;
		}
		case Scenario::Strike:				//called from player skill
		{
			CamSequence cs; 
			float v0 = 0.2f;
			float v1 = 2.0f;
			auto angle = RU.GetFloat(0.0f, PI);
			EH::SetNextAngle(angle+PI/3.0f);
			cs.push(std::tuple<CamTilt, CamMove, float>({ v0 * sin(angle),v0 * cos(angle),0.0f }, { 0.0f, 0.0f, 0.0f }, Cut::step0));
			cs.push(std::tuple<CamTilt, CamMove, float>({ -v1 * sin(angle),-v1 * cos(angle),0.0f }, { 3.0f, 0.0f, 0.0f }, Cut::step1));
			scenarious.emplace_back(cs);
			break;
		}
		case Scenario::Defend:				//called from player skill
		{
			CamSequence cs;
			float dir = RU.GetBool() ? 0.2f : -0.2f;
			cs.push(std::tuple<CamTilt, CamMove, float>({ 0.0f,dir,0.0f }, { -0.3f,0.0f,0.0f }, Deflect::step0));
			scenarious.emplace_back(cs);
			break;
		}
		case Scenario::GhoulRage:				//called from Ghoul Enrage Skill
		{
			CamSequence cs;
			float v0 = 0.01f;
			auto angle1 = RU.GetFloat(0.0f, PI*2.0f);
			auto angle2 = RU.GetFloat(0.0f, PI*2.0f);
			cs.push(std::tuple<CamTilt, CamMove, float>({ 0.0f,0.0f,0.0f },   { -0.05f,-0.03f,0.0f }, 0.3f));
			cs.push(std::tuple<CamTilt, CamMove, float>({ -v0 * sin(angle1),-v0 * cos(angle1),0.0f }, { -0.05f,-0.03f,0.0f }, 0.15f));
			cs.push(std::tuple<CamTilt, CamMove, float>({ -v0 * sin(angle2),-v0 * cos(angle2),0.0f }, { -0.05f,-0.03f,0.0f }, 0.15f));
			cs.push(std::tuple<CamTilt, CamMove, float>({ -v0 * cos(angle1),-v0 * sin(angle1),0.0f }, { -0.05f,-0.03f,0.0f }, 0.15f));
			cs.push(std::tuple<CamTilt, CamMove, float>({ -v0 * cos(angle2),-v0 * sin(angle2),0.0f }, { -0.05f,-0.03f,0.0f }, 0.15f));

			angle1 = RU.GetFloat(0.0f, PI * 2.0f);
			angle2 = RU.GetFloat(0.0f, PI * 2.0f);
			CamSequence cs1;
			cs1.push(std::tuple<CamTilt, CamMove, float>({ -v0 * sin(angle1),-v0 * cos(angle1),0.0f }, { 0.0f,0.0f,0.0f }, 0.15f));
			cs1.push(std::tuple<CamTilt, CamMove, float>({ -v0 * sin(angle2),-v0 * cos(angle2),0.0f }, { 0.0f,0.0f,0.0f }, 0.15f));
			cs1.push(std::tuple<CamTilt, CamMove, float>({ -v0 * cos(angle1),-v0 * sin(angle1),0.0f }, { 0.0f,0.0f,0.0f }, 0.15f));
			cs1.push(std::tuple<CamTilt, CamMove, float>({ -v0 * cos(angle2),-v0 * sin(angle2),0.0f }, { 0.0f,0.0f,0.0f }, 0.15f));
			cs1.push(std::tuple<CamTilt, CamMove, float>({ -v0 * sin(angle1),-v0 * cos(angle1),0.0f }, { 0.0f,0.0f,0.0f }, 0.15f));
			cs1.push(std::tuple<CamTilt, CamMove, float>({ -v0 * cos(angle1),-v0 * sin(angle1),0.0f }, { 0.0f,0.0f,0.0f }, 0.15f));
			cs1.push(std::tuple<CamTilt, CamMove, float>({ -v0 * sin(angle2),-v0 * cos(angle2),0.0f }, { 0.0f,0.0f,0.0f }, 0.25f));
			cs1.push(std::tuple<CamTilt, CamMove, float>({ -v0 * cos(angle1),-v0 * sin(angle1),0.0f }, { 0.0f,0.0f,0.0f }, 0.25f));

			scenarious.emplace_back(cs);
			scenarious.emplace_back(cs1);
			break;
		}
		case Scenario::GhoulAttack:					//called from CombatManager
		{
			CamSequence cs;
			float angle = RU.GetFloat(-0.5f, 0.5f);
			cs.push(std::tuple<CamTilt, CamMove, float>({ 0.3,angle,0.0f }, { -0.3f,0.0,0.0f }, 0.2f));
			scenarious.emplace_back(cs);
			break;
		}
		case Scenario::GhoulAttackMiss:				//called from CombatManager
		{
			CamSequence cs;
			float dir = RU.GetBool() ? 0.4f : -0.4f;
			cs.push(std::tuple<CamTilt, CamMove, float>({ 0.0f,dir,0.0f }, { -0.4f,0.0f,dir*3.0f },0.2f));
			scenarious.emplace_back(cs);
			break;
		}
		case Scenario::GhoulAttackBlock:			//called from CombatManager
		{
			CamSequence cs;
			float angle = RU.GetFloat(-0.2f, 0.2f);
			cs.push(std::tuple<CamTilt, CamMove, float>({ -0.35f,angle,0.0f }, { 0.6f,0.0,0.0f }, 0.2f));
			scenarious.emplace_back(cs);
			break;
		}
		}
		pendingScenarios.pop_back();
	}
	scenarious.erase(std::remove_if(scenarious.begin(), scenarious.end(), [dt](CameraScenario& s) {return s.Update(dt); }), scenarious.end());
	CamTrans targetTrans = baseTrans;				
	for (auto& s : scenarious)targetTrans += s.GetCamData();	//assembling target transform
	auto coeff = dt * speedCoeff;
	intermedTrans = intermedTrans * (1.0f - coeff) + targetTrans * coeff;
	curTrans = curTrans * (1.0f - coeff)  + intermedTrans * coeff;
	cam.SetFullTransform(curTrans);
}

DirectX::XMMATRIX CameraManager::GetMatrix() const noexcept
{
	return cam.GetMatrix();
}

DirectX::XMFLOAT3 CameraManager::GetPosition() const noexcept
{
	return cam.GetPosition();
}

CameraManager::CameraScenario::CameraScenario(CameraManager::CamSequence cs):
	stages(std::move(cs))
{}

bool CameraManager::CameraScenario::Update(float dt)
{
	stageTime += dt;
	if (stageTime > std::get<float>(stages.front()))
	{
		stages.pop();
		stageTime = 0.0f;
	}
	if (stages.empty())return true;
	else return false;
}

CamTrans CameraManager::CameraScenario::GetCamData()
{
	return { std::get<CamTilt>(stages.front()), std::get<CamMove>(stages.front()) };
}

std::vector<CameraManager::Scenario> CameraManager::pendingScenarios;