#pragma once
#include "Drawable.h"
#include "Bindable.h"
#include "IndexBuffer.h"
#include "ConstantBuffers.h"
#include <optional>

class HudElement : protected Drawable		
{	
	static constexpr size_t applyParentingToAllObjects = 666u;
public:
	using Drawable::GetTechniquesReference;
	HudElement(std::wstring spriteName, DXGraphics& gfx);
	HudElement(std::wstring spriteSheet, DXGraphics& gfx, wchar_t spriteNum, wchar_t spritesPerRow, wchar_t spritesPerColumn); //for constructing objects using sprite sheets
	HudElement() = default;
	HudElement(HudElement&&);		
	virtual ~HudElement() = default;
	DirectX::XMMATRIX GetTransformXM() const noexcept override;
	DirectX::XMMATRIX GetTransformXMSpecific(size_t ind) const noexcept;	//extracts transform of specific object and does not desturb the currentObjectBeingDrawn variable
	PositionData GetCumulatedPosData(size_t ind = 0u) const noexcept;		//extracts pos data according to parent's transforms
	void BindPositionData(const std::vector<PositionData>*  posData);
	void AddParent(const HudElement* parent, size_t parentInd = 0u, size_t objectInd = applyParentingToAllObjects); //index in posData that corresponds to the needed object
	void UnbindParents();
	virtual void SmartSubmit(FrameCommander& fc) const;		//uses Submit with standart tecqunice for first draw call 
															//and consecutive for all the rest in posData
	virtual void Animate(float dt) {}
protected:
	std::unordered_map<size_t, std::pair<const HudElement*, size_t>> parentData; //object index than parent object than parent object index
	mutable size_t currentObjectBeingDrawn = 0;
	const std::vector<PositionData>* posData = nullptr;			//all positions where you want to draw this go here
};

enum class StatusOwner		//is plased here because nicely includes everywhere
{
	Player,
	Enemy,
	NoOne
};
