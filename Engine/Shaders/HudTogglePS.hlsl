cbuffer Fade : register(b4)
{
    bool isToggled; 
    float3 padding;
};

Texture2D tex;
SamplerState splr;

float4 main(float2 tc : Texcoord, float4 pos : SV_Position) : SV_Target
{
    float4 dtex = tex.Sample(splr, tc);	
    if (!isToggled)return float4(dtex.rgba);
    else
    {
        float nDiv = 6.0f;
        uint each = uint((1.0f / nDiv) * 1000);
        uint tci = uint(tc.y * 1000.0f);
        float depth = 0.001 * (tci % each);
        uint ind = tci / each;
        uint mid = nDiv / 2;
        if (ind >= mid)
        {
            uint dif = ind - mid;
            ind -= 2 * dif;
        }
        else
        {
            uint dif = mid - ind;
            ind += 2 * dif;
        }
        tc.y = (0.001f * (float) each) * ind + depth;
        float4 dtex = tex.Sample(splr, tc);
        return float4(0.1f + dtex.b*0.9f, 0.1f, 0.1f, 1.0f);
    }
}