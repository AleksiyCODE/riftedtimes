#include "ShaderOps.hlsl"
#include "LightVectorData.hlsl"
#include "PointLight.hlsl"
#include "Opacity.hlsl"

cbuffer ObjectCBuf 
{
    float3 texSpecCol;
    float specularPower;
    float3 texDifColInt;
    bool normalMapEnabled;
};

Texture2D tex;
Texture2D nmap : register(t2);

SamplerState splr;

#define diffuseIntensity intensityParam[i][0]
#define attConst         intensityParam[i][1]
#define attLin           intensityParam[i][2]
#define attQuad          intensityParam[i][3]


float4 main(float3 viewFragPos : Position, float3 viewNormal : Normal, float3 viewTan : Tangent, float3 viewBitan : Bitangent, float2 tc : Texcoord, float4 pos : SV_Position, float3 ld[NUM_OF_LIGHTS] : Lightvectordata) : SV_Target
{
    float4 dtex = tex.Sample(splr, tc);
    // normalize the mesh normal
    viewNormal = normalize(viewNormal);    
    #ifdef MASK_BOI
    // bail if highly translucent
    clip(dtex.a < 0.001f ? -1 : 1);
    #endif    
    // replace normal with mapped if normal mapping enabled
    if (normalMapEnabled)
    {
        viewNormal = MapNormal(normalize(viewTan), normalize(viewBitan), viewNormal, tc, nmap, splr);
    }
    float3 diffuse = { 0.0f, 0.0f, 0.0f };
    float3 specular = { 0.0f, 0.0f, 0.0f };
    for (int i = 0; i < NUM_OF_LIGHTS; i++)
    {
        FullLightVectorData lv = CalculateLightVectorData(ld[i]);
	// attenuation
        const float att = Attenuate(attConst, attLin, attQuad, lv.distToL);
	// diffuse
        diffuse += Diffuse(diffuseColor[i], diffuseIntensity, att, lv.dirToL, viewNormal);
    // specular
        specular += Speculate(
        texSpecCol, diffuseIntensity, viewNormal,
        lv.vToL, viewFragPos, att, specularPower
        );
    }
    
    #ifdef OPAC_BOI    
    return float4(saturate((diffuse + ambient[0]) * dtex.rgb * texDifColInt + specular), dtex.a*opacity);
    #endif    
    return float4(saturate((diffuse + ambient[0]) * dtex.rgb * texDifColInt + specular), dtex.a);    
}