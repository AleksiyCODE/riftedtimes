#include "Letter.h"
#include "BindableCommon.h"
#include "../imgui/imgui.h"
#include "TextConstantBuffer.h"
#include "Vertex.h"

Letter::Letter(std::wstring fontSprite, DXGraphics& gfx, wchar_t letter)
{
	//getting main drawable data
	using namespace Bind;
	namespace dx = DirectX;
	using Dvtx::VertexLayout;

	Technique standard;
	{
		Step only(4);
		constexpr wchar_t charsInRow = 32;
		constexpr wchar_t charsInColumn = 3;
		auto tex = Texture::Resolve(gfx, fontSprite);
		float letterAspectRatio = ((float)tex->GetWidth()/charsInRow)
							/ //-----------------------------------------
								((float)tex->GetHeight()/charsInColumn);
		float screenAspecrRatio = (float)gfx.GetScreenWidth() / (float)gfx.GetScreenHeight();
		float totalAR = letterAspectRatio/screenAspecrRatio;
		only.AddBindable(std::move(tex));

		Dvtx::VertexBuffer vbuf(std::move(
			VertexLayout{}
			.Append(VertexLayout::Position2D)
			.Append(VertexLayout::Texture2D)
		));
		constexpr float singleWdth = 1.0f / 32.0f;
		constexpr float singleHeight = 1.0f / 3.0f;
		const wchar_t charID = letter - L' ';
		const float leftX = (charID  - (charID / charsInRow) * charsInRow) * singleWdth;//for texture
		const float topY = (charID / charsInRow) * singleHeight;
		const float rightX = leftX + singleWdth;
		const float bottomY = topY + singleHeight;
		vbuf.EmplaceBack(dx::XMFLOAT2{totalAR * -1.0f, 1.0f },	dx::XMFLOAT2{leftX  - 0.001f, topY	 +0.001f });		//assembling a rectangle of 2 triangles
		vbuf.EmplaceBack(dx::XMFLOAT2{totalAR,		  -1.0f },	dx::XMFLOAT2{rightX - 0.001f, bottomY-0.001f });
		vbuf.EmplaceBack(dx::XMFLOAT2{totalAR * -1.0f,-1.0f },	dx::XMFLOAT2{leftX  - 0.001f, bottomY-0.001f });
		vbuf.EmplaceBack(dx::XMFLOAT2{totalAR * -1.0f, 1.0f },	dx::XMFLOAT2{leftX  - 0.001f, topY	 +0.001f });
		vbuf.EmplaceBack(dx::XMFLOAT2{totalAR,		   1.0f },	dx::XMFLOAT2{rightX - 0.001f, topY	 +0.001f });
		vbuf.EmplaceBack(dx::XMFLOAT2{totalAR,		  -1.0f },	dx::XMFLOAT2{rightX - 0.001f, bottomY-0.001f });
		std::vector<unsigned short> indices{ 0,1,2,3,4,5 };

		only.AddBindable(PixelShader::Resolve(gfx, L"Shaders\\TextPS.cso"));
		auto pvs = VertexShader::Resolve(gfx, L"Shaders\\TextVS.cso");
		auto pvsbc = pvs->GetBytecode();
		only.AddBindable(InputLayout::Resolve(gfx, vbuf.GetLayout(), pvsbc));
		only.AddBindable(std::move(pvs));
		only.AddBindable(Blender::Resolve(gfx, true));
		only.AddBindable(std::make_shared<TextConstantBuffer>(gfx));
		standard.AddStep(std::move(only));		

		//these two should be bound outside steps because they stay the same no matter the Technique
		const auto meshTag = fontSprite + L"%" + letter;
		pVertices = VertexBuffer::Resolve(gfx, meshTag, vbuf);
		pIndices = IndexBuffer::Resolve(gfx, meshTag, indices);
	}
	AddTechnique(std::move(standard));
	Technique ConsecutiveStandart;		//use this only in case where you are drawing the same drawable multiple times in one frame
										//it greatly decreaces the abount of binding done by the pipeline
	{
		Step only(4);
		only.AddBindable(std::make_shared<TextConstantBuffer>(gfx));
		ConsecutiveStandart.AddStep(std::move(only));
	}
	AddTechnique(std::move(ConsecutiveStandart));
}

Letter::Letter(Letter&& mvFrom):
	letData(std::move(mvFrom.letData)),
	Drawable(std::move(mvFrom))
{
	currentObjectBeingDrawn = mvFrom.currentObjectBeingDrawn;
	this->RebindParent();
}

DirectX::XMMATRIX Letter::GetTransformXM() const noexcept
{
	assert(letData->size() && "no positions bound for this object - can't draw");
	currentObjectBeingDrawn++;
	if (currentObjectBeingDrawn == letData->size())currentObjectBeingDrawn = 0;
	auto curObjData = (*letData)[currentObjectBeingDrawn];
	DirectX::XMFLOAT4X4 store{ 
	curObjData.pos.x, curObjData.pos.y, curObjData.rot,curObjData.scale,
	curObjData.color.x, curObjData.color.y, curObjData.color.z, 0.0f,
	0.0f, 0.0f, 0.0f, 0.0f,
	0.0f, 0.0f, 0.0f, 0.0f };
	DirectX::XMMATRIX encoded = DirectX::XMLoadFloat4x4(&store);
	return std::move(encoded);
}

void Letter::BindLetterData(std::vector<LetterData>* posData)
{
	this->letData = posData;
}

void Letter::SmartSubmit(FrameCommander& fc)
{
	if (letData->size())
	{
		this->Submit(fc, Techniques::Standart);
		for (size_t i = 1; i < letData->size(); i++)this->Submit(fc, Techniques::ConsecutiveStandart);
	}
}




