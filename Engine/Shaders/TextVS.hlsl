cbuffer TransformCBuf
{
    float2 translation;
    float rotation;
    float scale;
};
struct VSOut
{
    float2 pos : Position;
    float2 tc : Texcoord;
    float4 SVpos : SV_Position;
};

VSOut main(float2 pos : Position, float2 tc : Texcoord)
{
    VSOut vso;
    vso.pos = translation + pos * scale;
    vso.tc = tc;
    vso.SVpos = float4(vso.pos, 1.0f, 1.0f);
    return vso;
}