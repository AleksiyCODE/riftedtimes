#pragma once
#include "Framework/DXGraphics.h"
#include "Framework/PlaneTexNorm.h"
#include "Framework/FrameCommander.h"
#include "Framework/ChiliMath.h"
#include <unordered_map>
#include "CombatManager.h"
#include "SceneLight.h"
#include "CameraManager.h"
#include "CeilingHangers.h"

class LevelLayout
{
public:
	//models for wall are expected to be 1.0f by 1.0f planes facing upwards
	class Wall
	{
	public:
		enum class Type
		{
			Wall,
			Floor,
			Ceiling
		};
		enum class Direction		//the direction the wall facing
		{
			Left,
			Right,
			Forward,
			Backwards
		};
		enum class Model
		{
			StoneWall,
			StoneFloor,
			StoneCeiling,
			Last
		};
		void BindModelToPositions(std::vector<PositionData>* posData)	
		{
			plane.BindPositionData(posData);
		}
		Wall(std::wstring modelName, DXGraphics& gfx) :
			plane(modelName, gfx)
		{}		
		Wall(const Wall&) = default;
		Wall(Wall&&) = default;
		~Wall() = default;
		void Submit(FrameCommander& fc) { plane.SmartSubmit(fc, Techniques::Standart); }
	private:
		PlaneTexNorm plane;
	};
	class Enemy							//for now the system only supports Ghouls, but some parts (not all) are ready to operate multiple enemy types
	{
	public:
		enum class Model
		{
			GhoulAttack,
			GhoulRage,
			GhoulCover,
			GhoulCorpse,
			GhoulPrepareAttack,
			GhoulPrepareRage,
			GhoulPreparingCover,
			GhoulIdle,
			NotDrawing,
			Last
		};
		enum class Monster
		{
			Ghoul,
			Last
		};
		void SetDrawingData(const std::vector<PositionData>* posData, const std::vector<Model>* modData)
		{
			positionData = posData;
			modelData = modData;
		}
		Enemy(DXGraphics& gfx, Monster en = Monster::Ghoul);
		Enemy(const Enemy&) = delete;
		Enemy& operator=(const Enemy&) = delete;
		Enemy(Enemy&&) = default;
		~Enemy() = default;
		void Submit(FrameCommander& fc);
	private:
		std::unordered_map<Model,std::unique_ptr<PlaneTexNorm>> planes;
		const std::vector<PositionData>* positionData;
		const std::vector<Model>* modelData;
		std::vector<PositionData> attackPosData;		
		std::vector<PositionData> ragePosData;
		std::vector<PositionData> coverPosData;
		std::vector<PositionData> corpsePosData;
		std::vector<PositionData> prepareAttackPosData;
		std::vector<PositionData> prepareRagePosData;
		std::vector<PositionData> prepareCoverPosData;
		std::vector<PositionData> idlePosData;
	};
public:
	LevelLayout(DXGraphics& gfx, FrameCommander& fc,const CombatManager& cm);
	LevelLayout(const LevelLayout&) = delete;
	LevelLayout(LevelLayout&&) = delete;
	LevelLayout& operator=(const LevelLayout) = delete;
	LevelLayout& operator=(LevelLayout&&) = delete;
	~LevelLayout() = default;
	void AddWall(Wall::Model mod, Wall::Type type, DirectX::XMFLOAT2 pos = { 0,0 }, Wall::Direction dir = Wall::Direction::Backwards);
	void ClearWallPositions()
	{
		wallPositions.clear();
	}
	void SubmitDrawables();
	void UpdateAndBindLight(float dt, DirectX::XMMATRIX cameraTransform);
	DirectX::XMMATRIX GetCameraMatrix() const noexcept;			//only called from Game class
	DirectX::XMFLOAT3 GetCameraPosition() const noexcept;
	//for debugging
	void Rotate(float dx, float dy) noexcept
	{
		camMan.Rotate(dx, dy);
	}
	void Translate(DirectX::XMFLOAT3 translation) noexcept
	{
		camMan.Translate(translation);
	}
private:
	void AddEnemy(Enemy::Monster monster, DirectX::XMFLOAT2 pos = { 0,0 }, bool isDead = false);
	void RemoveCorpses();
	DXGraphics& gfx;	
	FrameCommander& fc;
	const CombatManager& cm;
	std::vector<Wall> wallModels;
	std::vector<std::vector<PositionData>> wallPositions;
	std::vector<Enemy> enemies;
	std::vector<std::pair<std::vector<PositionData>, std::vector<Enemy::Model>>> enemiesForDrawing;
	bool enemyWasDead = false;
	SceneLight sl;
	CameraManager camMan;
	EnemyCombat::State previousState;
	CeilingHangers hangers;
};