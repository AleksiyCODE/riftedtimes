#include "IntentIcons.h"

void IntentIconManager::SmartSubmit(FrameCommander& fc) const
{	
	currentIcon = cd.enemyIntent;
	if (currentIcon != previousIcon)
	{
		switch (currentIcon)
		{
		case AbilityType::NoType:
			break;
		case AbilityType::Attack:
			EH::AddEffect2D(EH::Effect::GhoulIntentShine_Attack);
			break;
		case AbilityType::Buff:
			EH::AddEffect2D(EH::Effect::GhoulIntentShine_Buff);
			break;
		case AbilityType::Block:
			EH::AddEffect2D(EH::Effect::GhoulIntentShine_Defence);
			break;
		default:
			break;
		}
		previousIcon = currentIcon;
	}
}