#pragma once
#include "Framework\HudElement.h"
#include "CombatManager.h"
class HudOverlay : public HudElement
{
public:
	HudOverlay(DXGraphics& gfx, const CombatData& cd);
	static void BoostOverlay(float boost);	//1.0f - a lot of boost
	void SmartSubmit(FrameCommander& fc)const override;
	void Animate(float dt) override;
private:
	struct Opac
	{
		float val;
		float pad0;
		float pad1;
		float pad2;
	};
	void Pulse(float dt);		// when player is at low hp overlay begins to pulse
	HudElement bloodOverlay;
	HudElement defaultOverlay;
	float currentPower = 0.0f;
	std::vector<PositionData> posDefault;
	std::vector<PositionData> posBlood;
	std::shared_ptr <Bind::PixelConstantBuffer<Opac>> cbuf = nullptr;
	static float pendingPower;
	static constexpr float falloff = 58.5f;
	static constexpr float changeSensitivity = 5.99f;
	DXGraphics& gfx;
	const CombatData& cd;
	float curPulseTime = 0.0f;
	static constexpr float pulseTime = 1.0f;
	static constexpr float boostPower = 3.0f;
};
