#include "CombatManager.h"
#include "RandomUnit.h"
#include "SoundPlayerInterface.h"
#include "Ghoul.h"			
#include "AchievementManager.h"
#include "CameraManager.h"
#include "SceneLight.h"
#include "BlurManager.h"

CombatManager::CombatManager() 
{
	SPI::Play(Sounds::Ambient_Dungeon,1.0f,0.2f);
	SPI::Play(Sounds::Ambient_Torch, 1.0f, 0.2f);
	playMan = std::make_unique<PlayerManager>();
	keysForActionBoxes.push_back('W');
	keysForActionBoxes.push_back('A');
	keysForActionBoxes.push_back('S');
	keysForActionBoxes.push_back('D');
	killcount = 0u;
}

void CombatManager::StartBattle(size_t enemyID)
{
	currentTurn = 0u;
	playMan->BeginBattle();
	enMan = std::make_unique<Ghoul>();
	enMan->AddSpeed((float)killcount * (float)killcount * enemySpeedIncRate);		//the more enemies you kill - the faster they become
	combatData.enemyIntent = AbilityType::NoType;
	combatData.isValid = true;
	CombatManager::curTurn = Turn::Prefase;
	turnAnnouncer.SetAnnouncment(TurnAnnouncer::AnnTurn::SpawningEnemy);
	UpdateCombatData();
	timers.Stop();
	SL::PendDistortion(SL::LightDistortionType::ExtraLongAndDark);
}

void CombatManager::EndBattle()
{
	turnAnnouncer.SetAnnouncment(TurnAnnouncer::AnnTurn::Void);
	enMan = nullptr;
	playMan->Reset();
	combatData.enemyIntent = AbilityType::NoType;
	combatData.isValid = false;
	CombatManager::curTurn = Turn::NotInCombat;
	timers.SetActiveTimer(CombatTimers::ActiveTimer::NoActive);
	timers.Stop();
	turnAnnouncer.SetAnnouncment(TurnAnnouncer::AnnTurn::Void);
	curTurn = Turn::NotInCombat;
	turnStartState = TurnBeginningState::None;
	returningToMenu = true;
	rewardMenuIsActive = false;
	currentProcingEffect = 0u;
	killcount = 0u;
	currentTurn = 0u;
	accumulatedTime = 0.0f;
	AM::ResetState();
}

PlayerManager& CombatManager::GetPlayerManager()
{
	return *playMan.get();
}

EnemyCombat& CombatManager::GetEnemyCombat()
{
	return  *enMan.get();
}

const CombatData& CombatManager::GetCombatDataReference() const
{
	return combatData;
}

bool CombatManager::IsReturningToMenu() const
{
	return returningToMenu;
}

void CombatManager::ReturnedToMenu()	//is called from the outside by the menu (make friend)?
{
	returningToMenu = false;
}

bool CombatManager::LightDistortingActionHappened()
{
	return lightDistortingActionHappened;
}

void CombatManager::StartPlayerTurn()
{
	assert(curTurn != Turn::Player);		//a bit old for the standarts
}

void CombatManager::StartEnemyTurn()
{
	playerTurnIsEnded = false;
	timers.SetActiveTimer(CombatTimers::ActiveTimer::NoActive);
	assert(curTurn != Turn::Enemy && "bad combat manager logic. Start enemy turn was kalled when it was already enemy turn");
	curTurn = Turn::AnnouncingEnemyTurn;
	turnAnnouncer.SetAnnouncment(TurnAnnouncer::AnnTurn::Enemy);
	SPI::Play(Sounds::StartEnemyTurn, 1.0f, 0.3f);
}

void CombatManager::OnPlayerDeath()
{
	turnAnnouncer.SetAnnouncment(TurnAnnouncer::AnnTurn::PlayerDead);
	curTurn = Turn::PlayerDied;
}

void CombatManager::OnEnemyDeath()
{
	if (enMan->GetState() != EnemyCombat::State::GhoulDead)
	{
		SPI::Play(Sounds::Applause);
		IncreaceKillcount();
		turnAnnouncer.SetAnnouncment(TurnAnnouncer::AnnTurn::EnemyDead);
		curTurn = Turn::EnemyDied;
		enMan->SetState(EnemyCombat::State::GhoulDead);
		playMan->EndTurn();
		playMan->UpgradeRandomSkill();					//after each kill one skill is upgraded
		AM::Alert(AM::Event::EnemyDied);
		AM::Alert(AM::Event::TurnEnd);
	}
}

void CombatManager::EndPlayerTurn()
{
	playerTurnIsEnded = true;
}

void CombatManager::StartNewTurn()				//global new turn
{										//perk all turn based events here (first player - then enemy)	
	curTurn = Turn::StartingTurn;			
	turnStartState = TurnBeginningState::StatusEffectDumping;
	enMan->Choose(true);		//reseting enemy posture
	currentTurn++;
	AM::Alert(AM::Event::TurnBegin);
}

int CombatManager::ProcessInput(Keyboard::Event in_event)			//why does it return int and not bool?
{
	if (in_event.GetCode() == 27u)			//escape pressed
	{
		EndBattle();
	}	   	 
	if (curTurn != Turn::NotInCombat)
	{
		if (curTurn == Turn::Player)
		{
			if (in_event.GetCode() == ' ')							//skip timer		
			{
				timers.SkipTimer();
			}
			for (auto key : keysForActionBoxes)
			{
				if (in_event.GetCode() == key)
				{
					if (playMan->ProcessInput(key))
					{
						timers.Stop();
						curTurn = Turn::PostActionPlayerPause;				//this ends turn when a finisher is pressed (see another turn ender)
						playMan->EndTurn();
					}
				}
			}
		}
		else if (curTurn == Turn::PlayerCoountdown)
		{
			if (in_event.GetCode() == ' ' && in_event.IsPress())							//skip timer
			{
				timers.SkipTimer();
			}
		}
	}
	return 0;
}



void CombatManager::Draw(DXGraphics& gfx)
{
	if (curTurn != Turn::NotInCombat)
	{
		timers.DrawTimer();
		turnAnnouncer.Draw(killcount);
	}
}

void CombatManager::UpdateCombatState(float dt)
 {
	if (curTurn != Turn::NotInCombat)
	{
 		switch (curTurn)
		{
		case CombatManager::Turn::Prefase:		//right after there is a need to spawn monster
			if (turnAnnouncer.Update(dt))
			{									//enemy spawn has been announced
				StartNewTurn();
				SPI::Play(Sounds::StartBattle, 1.0f, 0.3f);
				enMan->SetState(EnemyCombat::State::GhoulRage);
			}
			break;
		case CombatManager::Turn::StartingTurn:
			UpdateCombatData();
			accumulatedTime += dt;
			switch (turnStartState)
			{
			case CombatManager::TurnBeginningState::StatusEffectDumping:
				playMan->DumpTemporaryStatus();
				enMan->DumpTemporaryStatus();
				turnStartState = TurnBeginningState::PlayerStatusEffects;
				break;
			case CombatManager::TurnBeginningState::PlayerStatusEffects:
				if (accumulatedTime >= statusEffetAnnounceTime)		//statusEffetAnnounceTime passed since the end of turn
				{
					size_t numEffects;
					numEffects = playMan->ProkEndTurnStatusEffects(currentProcingEffect);
					currentProcingEffect++;
					accumulatedTime = 0.0f;
					if (currentProcingEffect > numEffects)		//if proced all status effects
					{
						playMan->ProkEndTurnStatusEffects(currentProcingEffect);		//call proc with increaced index to show that all effects have been proced
						currentProcingEffect = 0u;
						turnStartState = TurnBeginningState::EnemyStatusEffects;
					}
					else					//if some are left
					{
						currentProcingEffect++;
					}
				}
				else if(currentProcingEffect==0u)				//the turn just began
				{
					TD::DrawString(L"Turn" + std::to_wstring(currentTurn), { -0.75f,0.0f }, Colors::LightGray, 0.14f);
				}
				break;
			case CombatManager::TurnBeginningState::EnemyStatusEffects:
				if (accumulatedTime >= statusEffetAnnounceTime)		//statusEffetAnnounceTime passed since the end of turn
				{
					size_t numEffects;
					numEffects = enMan->ProkEndTurnStatusEffects(currentProcingEffect);
					currentProcingEffect++;
					accumulatedTime = 0.0f;
					if (currentProcingEffect > numEffects)		//if proced all status effects
					{
						enMan->ProkEndTurnStatusEffects(currentProcingEffect);		//call proc with increaced index to show that all effects have been proced
						currentProcingEffect = 0u;
						curTurn = Turn::ExpressingEnemyIntent;
					}
					else					//if some are left
					{
						currentProcingEffect++;
					}					
				}
				break;
			case CombatManager::TurnBeginningState::None:
				break;
			default:
				assert(false && "missed a case in CombatManager::TurnBeginningState:: switch");
				break;
			}
			break;

		case  CombatManager::Turn::ExpressingEnemyIntent:
			combatData.enemyIntent = enMan->Choose();
			UpdateCombatData();
			curTurn = Turn::AnnouncingPlayerTurn;
			turnAnnouncer.SetAnnouncment(TurnAnnouncer::AnnTurn::Player);
			SPI::Play(Sounds::StartPlayerTurn, 1.0f, 0.3f);
			break;

		case CombatManager::Turn::AnnouncingPlayerTurn:
			if (turnAnnouncer.Update(dt))
			{
				curTurn = Turn::PlayerCoountdown;
				timers.SetActiveTimer(CombatTimers::ActiveTimer::Countdown);
				turnAnnouncer.SetAnnouncment(TurnAnnouncer::AnnTurn::Void);
			}
			break;

		case CombatManager::Turn::PlayerCoountdown:
			if (timers.UpdateTimer(dt))
			{
				curTurn = Turn::Player;
				timers.SetActiveTimer(CombatTimers::ActiveTimer::Turn, ((playMan.get()->GetTurnTime())*playMan.get()->GetSpeed()) / enMan.get()->GetSpeed());        
				playMan->StartTurn();
			}
			break;

		case CombatManager::Turn::Player:
		{
			assert(!playerTurnIsEnded && "caught combat manager logic error");	//just in case
			ActionBox::TurnState boxState = playMan->UpdateTurnState(dt);
			bool timerEnd = timers.UpdateTimer(dt);
			if (timerEnd ||  boxState == ActionBox::TurnState::Done || boxState==ActionBox::TurnState::DoneAnimation)
			{
				timers.Stop();																
				timers.SetActiveTimer(CombatTimers::ActiveTimer::NoActive);
				curTurn = Turn::PostActionPlayerPause;
				playMan->EndTurn();
			}
			HandleEvents();
			UpdateCombatData();
			break;
		}
		case CombatManager::Turn::PostActionPlayerPause:	
		{
			HandleEvents();
			UpdateCombatData();
			ActionBox::TurnState boxState = playMan->UpdateTurnState(dt);
			if (boxState != ActionBox::TurnState::DoneAnimation)		//if animation is done
			{
				if (accumumPostPlayerTime > pauseAfterPlayerTurn)
				{
					accumumPostPlayerTime = 0.0f;
					if (!enMan->IsDead())StartEnemyTurn();
				}
				else
				{
					accumumPostPlayerTime += dt;
				}
			}
			break;
		}
		case CombatManager::Turn::AnnouncingEnemyTurn:
			if (turnAnnouncer.Update(dt))
			{
				curTurn = Turn::StartingEnemy;
				combatData.enemyIntent = AbilityType::NoType;
			}
			break;

		case CombatManager::Turn::StartingEnemy:
			curTurn = Turn::Enemy;
			break;

		case CombatManager::Turn::Enemy:
			enMan.get()->Act();
			HandleEvents();
			curTurn = Turn::PostActionEnemyPause;
			UpdateCombatData();
			break;

		case CombatManager::Turn::PostActionEnemyPause:
			accumulatedTime += dt;
			if (accumulatedTime > enemyTurnAnimationLength)
			{
				AM::Alert(AM::Event::TurnEnd);
				StartNewTurn();
				UpdateCombatData();
				accumulatedTime = 0.0f;
			}
			break;

		case CombatManager::Turn::PlayerDied:
			UpdateCombatData();
			break;

		case CombatManager::Turn::EnemyDied:
			if (turnAnnouncer.Update(dt))
			{
				curTurn = Turn::ChoosingReward;
				rewardMenuIsActive = true;
			}
			break;

		case CombatManager::Turn::ChoosingReward:
			if (!rewardMenuIsActive)
			{
				StartBattle(0);
			}
			break;

		default:
			assert(false && "Missed a combat state");
			break;
		}
	}
}

EnemyCombat::State CombatManager::GetEnemyState()const
{
	if (enMan) return  enMan->GetState(); else return EnemyCombat::State::NoEntity;
}

void CombatManager::ChoseBandage()
{
	playMan->CureBleed();
	playMan->Heal(size_t((playMan->GetmaxHP() - playMan->GetHP()) * 0.1f));		//heal for 10% of missing hp
	rewardMenuIsActive = false;
}

void CombatManager::ChoseCrit()
{
	playMan->AddPassiveCrit(critBuffEmount);
	rewardMenuIsActive = false;
}

void CombatManager::ChoseDodge()
{
	playMan->AddPassiveDodge(critBuffEmount);
	rewardMenuIsActive = false;
}

bool CombatManager::ChoseUpgrade()
{
	if (playMan->UpgradeRandomSkill())	//if all skills are already upgraded
	{
		return true;
	}
	else
	{
		rewardMenuIsActive = false;
		return false;
	}
}

void CombatManager::IncreaceKillcount()
{
	killcount++;
	if (killcount >= 8u)AM::Alert(AM::Event::PlayerKilled8Enemies);
}

void CombatManager::DrawDamage(bool missed, size_t dmg, bool crited, bool bleed, bool playerProduced) const
{	
	if (missed)
	{
		TD::DrawStringTimed(L"MISS!", Vec2{ -0.1f, -0.8f }, Colors::White, 0.15f);
	}
	else
	{
		if (playerProduced)
		{
			if (crited)TD::DrawStringTimed(L"CRIT!", Vec2(0.1f, 0.5f), Colors::Cyan, 0.11f);
			if (bleed) TD::DrawStringTimed(L"BLEED!", Vec2(-0.1f, 0.69f), Colors::Red, 0.11f);
			if(dmg!=0u)
				if(crited||bleed)
					TD::DrawStringTimed(std::to_wstring(dmg), Vec2(-0.1f, 0.5f), Colors::Red, 0.13f);
				else
					TD::DrawStringTimed(std::to_wstring(dmg), Vec2(-0.1f, 0.5f), Colors::Yellow, 0.13f);
			else
				TD::DrawStringTimed(L"BLOCKED!", Vec2(-0.1f, 0.5f), Colors::LightBlue, 0.11f);
		}
		else
		{
			if (bleed) TD::DrawStringTimed(L"BLEED!", Vec2{ -0.12f, -0.6f }, Colors::Red, 0.12f);
			if (dmg != 0u)
				if(bleed)
					TD::DrawStringTimed(std::to_wstring(dmg), Vec2(-0.05f, -0.84f), Colors::Red, 0.13f);
				else
					TD::DrawStringTimed(std::to_wstring(dmg), Vec2(-0.05f, -0.84f), Colors::Yellow, 0.13f);
			else
				TD::DrawStringTimed(L"BLOCKED!", Vec2(-0.14f, -0.88f), Colors::LightBlue, 0.11f);
		}
	}	
}

void CombatManager::UpdateCombatData()
{
	combatData.playerHP = playMan->GetHP();
	combatData.playerMaxHP = playMan->GetmaxHP();
	combatData.playerBlock = playMan->GetBlock();
	combatData.playerSpeed = playMan->GetSpeed();
	combatData.playerStatusEffects = playMan->GetStatusEffectsReference();

	combatData.enemyHP = enMan->GetHP();
	combatData.enemyMaxHP = enMan->GetmaxHP();
	combatData.enemyBlock = enMan->GetBlock();
	combatData.enemySpeed = enMan->GetSpeed();
	combatData.enemyStatusEffects = enMan->GetStatusEffectsReference();

	if (enMan->IsDead() && curTurn != CombatManager::Turn::EnemyDied && curTurn != CombatManager::Turn::PlayerDied)	OnEnemyDeath();
	if (playMan->IsDead()&&curTurn!=CombatManager::Turn::PlayerDied) OnPlayerDeath();
}

void CombatManager::HandleEvents()
{
	auto& playEvents = playMan->GetEvents();		//handeling  events spawned by player
	for (auto& ev : playEvents)
	{
		HandleEvent(ev);
	}
	playEvents.clear();

	auto& enemEvents = enMan->GetEvents();			//handeling events spawned by enemy
	for (auto& ev : enemEvents)
	{
		HandleEvent(ev);
	}
	enemEvents.clear();
}

void CombatManager::HandleEvent(SkillEvent ev)
{
	float fRoll;
	size_t sDmg = 0u;
	size_t dealtDmg = 0u;
	bool attacked = false;
	bool crit = false;
	bool miss = false;
	bool bleed = false;
	bool playerProduced = false;
	switch (ev.eventType)
	{
		//player related events
	case EventType::Player_PhysicalDamage:				
		attacked = true;
		fRoll = RU.GetFloat0to1();
		if (fRoll < (enMan->GetCurAcc() * (1 - playMan->GetDodge())))
		{																//hit
			sDmg = RU.GetUInt((size_t)ev.valLow, (size_t)ev.valHigh);
			if (dealtDmg = playMan->SufferPhysicalDamage(sDmg))
				CM::PendScenario(CM::Scenario::GhoulAttack);
			else
				CM::PendScenario(CM::Scenario::GhoulAttackBlock);
			BlurManager::AddBlur(0.5f);
			AM::Alert(AM::Event::PlayerGotHit);
		}
		else
		{													//miss	
			SPI::Play(Sounds::MissBigDamage);										
			miss = true;
			CM::PendScenario(CM::Scenario::GhoulAttackMiss);
		}
		break;
	case EventType::Player_AddDodge:
		playMan->AddDodge(ev.valHigh);		
		break;
	case EventType::Player_AddBlock:
		playMan->AddBlock(RU.GetUInt((size_t)ev.valLow, (size_t)ev.valHigh));
		break;
	case EventType::Player_AddCrit:
		playMan->AddCrit(ev.valHigh);
		break;
	case EventType::Player_Heal:
		playMan->Heal((size_t)ev.valHigh);
		break;
	case EventType::Player_PhysicalAttackedWithBleed:
		attacked = true;
		fRoll = RU.GetFloat0to1();
		if (fRoll < (enMan->GetCurAcc() * (1 - playMan->GetDodge())))
		{													//hit
			sDmg = RU.GetUInt((size_t)ev.valLow, (size_t)ev.valHigh);	
			if (playMan->GetBlock() < sDmg)
			{
				playMan->AddBleed(OneBleedStackEmount);
				dealtDmg = playMan->SufferPhysicalDamage(sDmg);	//	bleed got through block
				bleed = true;
				CM::PendScenario(CM::Scenario::GhoulAttack);
			}
			else
			{
				dealtDmg = playMan->SufferPhysicalDamage(sDmg);	//	bleed blocked
				CM::PendScenario(CM::Scenario::GhoulAttackBlock);
			}			
			AM::Alert(AM::Event::PlayerGotHit);
		}
		else
		{
			SPI::Play(Sounds::MissBigDamage);		//miss
			CM::PendScenario(CM::Scenario::GhoulAttackMiss);
			miss = true;
		}
		break;
	case EventType::Player_CureBleed:
		playMan->CureBleed();
		break;
	case EventType::Player_AddSpeed:
		playMan->AddSpeed(ev.valHigh);
		break;

		//enemy related events
	case EventType::Enemy_PhysicalDamage:
		playerProduced = true;
		attacked = true;
		fRoll = RU.GetFloat0to1();
		enMan->SetState(EnemyCombat::State::GhoulPreparingCover);
		if (fRoll < playMan->GetCritMod())
		{
			sDmg = 2u * RU.GetUInt((size_t)ev.valLow, (size_t)ev.valHigh);  //crit
			dealtDmg = enMan->SufferPhysicalDamage(sDmg);
			crit = true;
			
			SPI::Play(Sounds::CritWave, 0.5f, 1.4f);
		}
		else
		{
			sDmg = RU.GetUInt((size_t)ev.valLow, (size_t)ev.valHigh);
			dealtDmg = enMan->SufferPhysicalDamage(sDmg);
		}
		break;
	case EventType::Enemy_DecreaceAcc:
		enMan->DecreaceAcc(ev.valHigh);
		break;
	case EventType::Enemy_AddBlock:
		enMan->AddBlock(RU.GetUInt((size_t)ev.valLow, (size_t)ev.valHigh));	
		break;
	case EventType::Enemy_Heal:
		enMan->Heal((size_t)ev.valHigh);
		break;
	case EventType::Enemy_PhysicalAttackedWithBleed:
		playerProduced = true;
		attacked = true;
		fRoll = RU.GetFloat0to1();
		if (fRoll < playMan->GetCritMod())
		{
			enMan->SetState(EnemyCombat::State::GhoulPreparingCover);
			sDmg = 2u*RU.GetUInt((size_t)ev.valLow, (size_t)ev.valHigh);	//crit
			if (enMan->GetBlock() < sDmg)
			{
				enMan->AddBleed(OneBleedStackEmount);
				dealtDmg = enMan->SufferPhysicalDamage(sDmg);				//bleed got through block
				crit = true;
				bleed = true;				
				SPI::Play(Sounds::CritWave, 0.6f, 1.5f);
			}
			else
			{
				dealtDmg = enMan->SufferPhysicalDamage(sDmg);				//bleed blocked
				crit = true;
				SPI::Play(Sounds::CritWave, 0.6f, 1.5f);
			}
		}
		else
		{
			sDmg = RU.GetUInt((size_t)ev.valLow, (size_t)ev.valHigh);	//hit
			if (enMan->GetBlock() < sDmg)
			{
				enMan->AddBleed(OneBleedStackEmount);
				dealtDmg = enMan->SufferPhysicalDamage(sDmg);				//bleed got through block
				bleed = true;
			}
			else
			{
				dealtDmg = enMan->SufferPhysicalDamage(sDmg);				//bleed blocked
			}
		}
		break;
	case EventType::Enemy_CureBleed:
		enMan->CureBleed();
		break;
	case EventType::Enemy_AddSpeed:
		enMan->AddSpeed(ev.valHigh);
		break;

	case EventType::Invalid:
		assert(false && "invalid combat event");
		break;
	default:
		assert(false && "stray combat event detected! check switch statement");
		break;
	}
	//displaying damage
	if (attacked)
			DrawDamage(miss, dealtDmg, crit, bleed, playerProduced);
	//hanling additional player crit stuff
	if (playerProduced && crit)
	{
		AM::Alert(AM::Event::PlayerCrited);
		SL::PendDistortion(SL::LightDistortionType::DarkeningFlicker);
	}
}

bool CombatManager::GetRewardMenuActiveness() const
{
	return rewardMenuIsActive;
}