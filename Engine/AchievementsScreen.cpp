#include "AchievementsScreen.h"
#include "Framework\Bindable.h"
#include "Framework\PixelShader.h"
#include "Framework\ConstantBuffers.h"

AchievementPanel::AchievementPanel(DXGraphics& gfx, size_t achIndex, bool isUnlocked) :
	border(L"Media//Sprites//Cell.png", gfx),
	picture(L"Media//Sprites//Achievements.png", gfx, (wchar_t)achIndex, NUM_ACHIEVEMENTS, 1u),
	text(L"Media//Sprites//AchievementText.png", gfx, (wchar_t)achIndex, NUM_ACHIEVEMENTS, 1u)
{
	picture.AddParent(&border);
	text.AddParent(&border);
	picturePos.emplace_back(0.0f, 0.431f, 0.43f);
	textPos.emplace_back   (0.0f,-0.431f, 0.43f);
	borderPos.emplace_back(-1.0f + float(((int)achIndex - (int)((achIndex / cardsInARow)* cardsInARow)+1) * cardOffsetHorisontal), -(float)(achIndex / cardsInARow) + 0.5f, 0.43f);
	text.BindPositionData(&textPos);
	picture.BindPositionData(&picturePos);
	border.BindPositionData(&borderPos);
	cbuf = std::make_shared<Bind::PixelConstantBuffer<fadeCbuf>>(gfx, 4);
	auto& s = picture.GetTechniquesReference()[0].GetSteps()[0];
	s.DeleteBindableByUID(L"class Bind::PixelShader#Shaders\\HudPS.cso");
	s.AddBindable(cbuf);
	s.AddBindable(Bind::PixelShader::Resolve(gfx, L"Shaders\\HudTogglePS.cso"));
}

AchievementPanel::AchievementPanel(AchievementPanel&&mv) :
	borderPos(std::move(mv.borderPos)),
	picturePos(std::move(mv.picturePos)), 
	textPos(std::move(mv.textPos)),
	border(std::move(mv.border)),
	picture(std::move(mv.picture)),
	text(std::move(mv.text)),
	cBufContent(std::move(mv.cBufContent)),
	cbuf(mv.cbuf),
	HudElement(std::move(mv))
{
	picture.AddParent(&border);
	text.AddParent(&border);
	border.BindPositionData(&borderPos);
	picture.BindPositionData(&picturePos);
	text.BindPositionData(&textPos);
	currentObjectBeingDrawn = mv.currentObjectBeingDrawn;
	this->RebindParent();	
}

void AchievementPanel::UpdateFadedness(DXGraphics& gfx, bool in_isFaded)
{
	cBufContent.isToggled = in_isFaded;
	cbuf->Update(gfx, cBufContent);
}

void AchievementPanel::SmartSubmit(FrameCommander& fc) const
{
	text.SmartSubmit(fc);
	picture.SmartSubmit(fc);
	border.SmartSubmit(fc);
}

AchievementsScreen::AchievementsScreen(DXGraphics& gfx):
	am(AM::GetAchievementModul())
{
	for (size_t i = 0u; i < NUM_ACHIEVEMENTS; i++)
	{
		panels.emplace_back(gfx, i, false);
	}
}

void AchievementsScreen::UpdateLockedSkills(DXGraphics& gfx)
{
	auto ach = am.GetAchievements();
	for (size_t i = 0u; i < NUM_ACHIEVEMENTS; i++)
	{
		panels[i].UpdateFadedness(gfx, !ach.at(static_cast<AchievementModul::Achievement>(i + '0')));
	}
}

void AchievementsScreen::SmartSubmit(FrameCommander& fc) const
{
	for (auto& p : panels)p.SmartSubmit(fc);
}
