#pragma once
#include "EnemyCombat.h"
#include "./SkillStuff/AbilityGeneralTypes.h"
class Ghoul : public EnemyCombat
{
public:
	Ghoul();
	void Act() override;	
private:
	AbilityType Choose(bool justReset = false) override;
};