#pragma once
#include "SkillPressProcessReturnEnum.h"
#include "AbilityGeneralTypes.h"
#include "../CombatEntity.h"
class Skill
{
public:		
	Skill(CombatEntity* owner) :owner(owner) {};
	virtual ~Skill() = default;	
	SkillProcessResult ProcessPress()const;	
	virtual void OnPend() {}
	virtual void Activate() = 0;
	void SetAvalibility(bool upd);
	bool GetAvalibility() const;
	size_t GetID() const;
	AbilityType GetType() const 
	{
		return type;
	}
	bool IsRewinding() const
	{
		return rewinding;
	}
	bool IsAvalible() const
	{
		return avalible;
	}
	bool IsQuick() const
	{
		return quick;
	}
	bool IsUpgraded() const
	{
		return upgraded;
	}
	virtual void Upgrade() {};
	void Rewind();
	void ResetTurn();
	float preActTime = 0.0f;
	float actTime = 0.0f;
protected:
	size_t id = 0;			//is used to extract sprite from file
	bool quick = false;
	bool rewinding = false;
	bool avalible = true;
	bool upgraded = false;
	bool wasRewound = false;
	AbilityType type = AbilityType::NoType;		
	CombatEntity* owner = nullptr;
	float valLow;			//set to zeroes if not a number based event
	float valHigh;
	float succChance;
};
