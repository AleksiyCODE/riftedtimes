//partly coded by Chili
#pragma once
#include <array>
#include "BindableCommon.h"
#include "DXGraphics.h"
#include "Job.h"
#include "Pass.h"
#include "DepthStencil.h"
#include "RenderTarget.h"
#include "BlurPack.h"

class FrameCommander		//submits drawables
{
public:
	FrameCommander(DXGraphics& gfx);;
	void Accept( Job job, size_t targetPass ) noexcept
	{
		passes[targetPass].Accept( job );
	}
	void Execute(DXGraphics& gfx, float dt) noxnd;
	void Reset() noexcept;
	static void SetBlurMod(float blur);			//make friend of blur commander or something
private:
	static float blurMod;			//0.0f - no blur; 1.0f - very dank blur
	std::array<Pass, 5> passes;
	DXGraphics& gfx;
	DepthStencil ds;
	RenderTarget rt1;
	RenderTarget rt2;
	std::shared_ptr<Bind::VertexBuffer> pVbFull;
	std::shared_ptr<Bind::IndexBuffer> pIbFull;
	std::shared_ptr<Bind::VertexShader> pVsFull;
	std::shared_ptr<Bind::InputLayout> pLayoutFull;
	BlurPack bp;
};