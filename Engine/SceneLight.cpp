#include "SceneLight.h"
#include "RandomUnit.h"

SceneLight::SceneLight(DXGraphics& gfx): 
	pl(gfx,dataLightBuffer,posLightBuffer)
{
	float unpacked[4];
	_mm_storeu_ps(unpacked, posLight0[0]);
	lightHolderPositionData.emplace_back(unpacked[0] + 0.01f, unpacked[1] - 0.16f, unpacked[2] - 0.06f, 0.0f, 0.0f, PI / 2.0f);
	_mm_storeu_ps(unpacked, posLight0[1]);
	lightHolderPositionData.emplace_back(unpacked[0] - 0.01f, unpacked[1] - 0.16f, unpacked[2] + 0.06f, 0.0f, PI, PI / 2.0f);
	_mm_storeu_ps(unpacked, posLight0[2]);
	lightHolderPositionData.emplace_back(unpacked[0] - 0.01f, unpacked[1] - 0.16f, unpacked[2] + 0.06f, 0.0f, PI, PI / 2.0f);
	_mm_storeu_ps(unpacked, posLight0[3]);
	lightHolderPositionData.emplace_back(unpacked[0] + 0.01f, unpacked[1] - 0.16f, unpacked[2] - 0.06f, 0.0f, 0.0f, PI / 2.0f);
	lightHolder = std::make_unique<PlaneTexNorm>(L"Media//Models//TorchHolder.obj", gfx);
	lightHolder->BindPositionData(&lightHolderPositionData);
}

void SceneLight::SubmitLightHolders(FrameCommander& fc)
{
	lightHolder->SmartSubmit(fc,Techniques::Standart);
}

void SceneLight::UpdateAndBind(float dt, DXGraphics& gfx, DirectX::XMMATRIX cameraTransform)
{
	if (pendingDistortion != LightDistortionType::NoDistortion)
	{
		currentDistortion = pendingDistortion;
		currentDistortedTime = 0.0f;
		if (currentDistortion == LightDistortionType::GreenWave || currentDistortion == LightDistortionType::YellowSpark) currentDistortedTime += 0.7f;		//reducing lifetime of some effects
		else if (currentDistortion == LightDistortionType::ExtraLongAndDark)currentDistortedTime -= extradarkProlongation;									//increacing lifetime 
		pendingDistortion = LightDistortionType::NoDistortion;
	}
	if (currentDistortion != LightDistortionType::NoDistortion)
	{
		currentDistortedTime += dt;
		if (currentDistortedTime > distortionTime)
		{
			currentDistortedTime = 0.0f;
			currentDistortion = LightDistortionType::NoDistortion;
		}
	}
	for (size_t i = 0; i < NUM_OF_LIGHTS; i++)
	{
		currentFlickerTime[i] += dt;
		if (currentFlickerTime[i] >= totalFlickerTime[i])
		{
			initialPosLightBuffer[i] = flickerEndPosLightBuffer[i];
			initialIntBuffer[i] = endIntBuffer[i];
			initDifBuffer[i] = endDifBuffer[i];
			currentFlickerTime[i] = 0.0f;
			totalFlickerTime[i] = RU.GetFloat(minFlickerTime, maxFlickerTime);
			auto& bplb = basePosLightBuffer[i].m128_f32;
			switch (currentDistortion)
			{
			case SceneLight::LightDistortionType::NoDistortion:
			{
				flickerEndPosLightBuffer[i] = {
				basePosLightBuffer[i].m128_f32[0],		//is not  taking part in interpolation
				RU.GetFloat(basePosLightBuffer[i].m128_f32[1] - positionFlickerAmount, basePosLightBuffer[i].m128_f32[1] + positionFlickerAmount),
				RU.GetFloat(basePosLightBuffer[i].m128_f32[2] - positionFlickerAmount, basePosLightBuffer[i].m128_f32[2] + positionFlickerAmount),
				0.0f
				};

				endIntBuffer[i] = RU.GetFloat(baseIntBuffer[i] - intencityFlickerAmount, baseIntBuffer[i] + intencityFlickerAmount);

				float f = RU.GetFloat0to1();				//what color(red-to-yellow) will be target color for this flick
				endDifBuffer[i] = _mm_add_ps(_mm_mul_ps(red, _mm_set1_ps(1.0f - f)), _mm_mul_ps(yellow, _mm_set1_ps(f)));
				break;
			}
			case SceneLight::LightDistortionType::DarkeningFlicker:
			{
				flickerEndPosLightBuffer[i] = {
				bplb[0],		//is not  taking part in interpolation
				RU.GetFloat(bplb[1] - positionFlickerAmount * darkeningMod, bplb[1] + positionFlickerAmount * darkeningMod),
				RU.GetFloat(bplb[2] - positionFlickerAmount * darkeningMod, bplb[2] + positionFlickerAmount * darkeningMod),
				0.0f
				};
				endIntBuffer[i] = RU.GetFloat(baseIntBuffer[i] - intencityFlickerAmount *3.0f, baseIntBuffer[i] - intencityFlickerAmount);

				float f = RU.GetFloat0to1();				//what color(red-to-yellow) will be target color for this flick
				endDifBuffer[i] = _mm_add_ps(_mm_mul_ps(red, _mm_set1_ps(1.0f - f)), _mm_mul_ps(yellow, _mm_set1_ps(f)));
				break;
				}
			case SceneLight::LightDistortionType::RednessBoost:
			{
				flickerEndPosLightBuffer[i] = {
				bplb[0],			//is not  taking part in interpolation
				RU.GetFloat(bplb[1] - positionFlickerAmount * redBoostMod, bplb[1] + positionFlickerAmount * redBoostMod),
				RU.GetFloat(bplb[2] - positionFlickerAmount * redBoostMod, bplb[2] + positionFlickerAmount * redBoostMod),
				0.0f
				};
				endIntBuffer[i] = RU.GetFloat(baseIntBuffer[i] - intencityFlickerAmount * redBoostMod, baseIntBuffer[i] - intencityFlickerAmount);

				float f = RU.GetFloat0to1();				//what color(red-to-yellow) will be target color for this flick
				endDifBuffer[i] = _mm_add_ps(_mm_mul_ps(red, _mm_set1_ps(1.0f - f)), _mm_mul_ps(yellow, _mm_set1_ps(f)));
				endDifBuffer[i].m128_f32[0] *= redColortMod;
				break;
			}
			case SceneLight::LightDistortionType::ColdEmbrace:
			{
				flickerEndPosLightBuffer[i] = {
				bplb[0],		//is not  taking part in interpolation
				RU.GetFloat(bplb[1] - positionFlickerAmount * coldMoveMod, bplb[1] + positionFlickerAmount * coldMoveMod),
				RU.GetFloat(bplb[2] - positionFlickerAmount * coldMoveMod, bplb[2] + positionFlickerAmount * coldMoveMod),
				0.0f
				};
				endIntBuffer[i] = RU.GetFloat(baseIntBuffer[i] + intencityFlickerAmount * coldBrightMod, baseIntBuffer[i] + intencityFlickerAmount * coldBrightMod);

				endDifBuffer[i] = _mm_mul_ps(red,_mm_setr_ps(coldColortModR, coldColortModG, coldColortModB, 1.0f));
				break;
			}
			case SceneLight::LightDistortionType::GreenWave:
			{
				flickerEndPosLightBuffer[i] = {
				basePosLightBuffer[i].m128_f32[0],		//is not  taking part in interpolation
				RU.GetFloat(basePosLightBuffer[i].m128_f32[1] - positionFlickerAmount, basePosLightBuffer[i].m128_f32[1] + positionFlickerAmount),
				RU.GetFloat(basePosLightBuffer[i].m128_f32[2] - positionFlickerAmount, basePosLightBuffer[i].m128_f32[2] + positionFlickerAmount),
				0.0f
				};

				endIntBuffer[i] = RU.GetFloat(baseIntBuffer[i] - intencityFlickerAmount, baseIntBuffer[i] + intencityFlickerAmount);

				float f = RU.GetFloat0to1();				//what color(red-to-yellow) will be target color for this flick
				endDifBuffer[i] = _mm_add_ps(_mm_mul_ps(red, _mm_set1_ps(1.0f - f)), _mm_mul_ps(yellow, _mm_set1_ps(f)));
				endDifBuffer[i].m128_f32[1] *= greenBoostMode;
				break;
			}
			case SceneLight::LightDistortionType::YellowSpark:
			{
				flickerEndPosLightBuffer[i] = {
				basePosLightBuffer[i].m128_f32[0],		//is not  taking part in interpolation
				RU.GetFloat(basePosLightBuffer[i].m128_f32[1] - positionFlickerAmount, basePosLightBuffer[i].m128_f32[1] + positionFlickerAmount),
				RU.GetFloat(basePosLightBuffer[i].m128_f32[2] - positionFlickerAmount, basePosLightBuffer[i].m128_f32[2] + positionFlickerAmount),
				0.0f
				};

				endIntBuffer[i] = RU.GetFloat(baseIntBuffer[i] - intencityFlickerAmount, baseIntBuffer[i] + intencityFlickerAmount);

				float f = RU.GetFloat0to1();				//what color(red-to-yellow) will be target color for this flick
				endDifBuffer[i] = _mm_add_ps(_mm_mul_ps(red, _mm_set1_ps(1.0f - f)), _mm_mul_ps(yellow, _mm_set1_ps(f)));
				endDifBuffer[i].m128_f32[0] *= yellowBoostMode;
				endDifBuffer[i].m128_f32[1] *= yellowBoostMode;
				break;
			}		
			case SceneLight::LightDistortionType::ExtraLongAndDark:
			{
				flickerEndPosLightBuffer[i] = {
				basePosLightBuffer[i].m128_f32[0],		//is not  taking part in interpolation
				RU.GetFloat(basePosLightBuffer[i].m128_f32[1] - positionFlickerAmount, basePosLightBuffer[i].m128_f32[1] + positionFlickerAmount),
				RU.GetFloat(basePosLightBuffer[i].m128_f32[2] - positionFlickerAmount, basePosLightBuffer[i].m128_f32[2] + positionFlickerAmount),
				0.0f
				};

				endIntBuffer[i] = RU.GetFloat(baseIntBuffer[i]*extradarkPower - intencityFlickerAmount, baseIntBuffer[i] * extradarkPower + intencityFlickerAmount);

				float f = RU.GetFloat0to1();				//what color(red-to-yellow) will be target color for this flick
				endDifBuffer[i] = _mm_add_ps(_mm_mul_ps(red, _mm_set1_ps(1.0f - f)), _mm_mul_ps(yellow, _mm_set1_ps(f)));
				break;
			}
			default:
				assert(false && "missed case in SceneLight switch (currentDistortion)");
				break;
			}
		}
		else
		{
			float flickProgress = currentFlickerTime[i] / totalFlickerTime[i];
			posLightBuffer[i] = _mm_add_ps(_mm_mul_ps(initialPosLightBuffer[i], _mm_set1_ps(1.0f - flickProgress)), _mm_mul_ps(flickerEndPosLightBuffer[i], _mm_set1_ps(flickProgress)));
			dataLightBuffer.diffuseColor[i] = _mm_add_ps(_mm_mul_ps(initDifBuffer[i], _mm_set1_ps(1.0f - flickProgress)), _mm_mul_ps(endDifBuffer[i], _mm_set1_ps(flickProgress)));
			dataLightBuffer.intensityParam[i].x = initialIntBuffer[i] * (1.0f - flickProgress) + endIntBuffer[i] * flickProgress;
		}
	}
	pl.UpdatePosition(gfx, cameraTransform);
	pl.Bind(gfx);
}

SceneLight::LightDistortionType SceneLight::pendingDistortion = SceneLight::LightDistortionType::NoDistortion;