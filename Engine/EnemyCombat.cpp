#include "EnemyCombat.h"
#include <iostream>
#include "./SkillStuff/AbilityGeneralTypes.h"
#include "SoundPlayerInterface.h"

EnemyCombat::EnemyCombat()
{
	activeStatusEffects[StatusEffects::Accuracy].first = baseAcc;
}

size_t EnemyCombat::SufferPhysicalDamage(size_t incDmg)
{
	if (incDmg < HP + block)		//if does not kill
	{
		if (block >= incDmg)
		{
			SPI::Play(Sounds::Ghoul_Block, 1.0f, (std::max)(0.1f, (std::min)(0.6f, incDmg*0.06f)));
			SPI::Play(Sounds::Ghoul_oh_02, 1.0f, 0.1f);			
			return 0u; 
		}
		else
		{
			incDmg -= block;
			HP -= incDmg;
			if (block) SPI::Play(Sounds::Ghoul_Block, 1.0f, (std::max)(0.1f, (std::min)(0.6f, block*0.035f)));
			SPI::Play(Sounds::Cut, 1.0f, (std::max)(0.1f,(std::min)(0.6f,incDmg*0.08f)));		
			SPI::Play(Sounds::Ghoul_oh_01, 1.0f, (std::max)(0.1f, (std::min)(0.6f, incDmg*0.08f)));	
			return incDmg;
		}
	}
	else
	{
		auto dmg = HP;
		SPI::Play(Sounds::Ghoul_Death);
		HP = 0;
		OnDeath();
		return dmg;
	}
}
float EnemyCombat::GetCurAcc() const
{
	return activeStatusEffects.at(StatusEffects::Accuracy).first;
}

void EnemyCombat::DecreaceAcc(float acc)
{
	activeStatusEffects[StatusEffects::Accuracy].first = (std::max)(0.05f, activeStatusEffects[StatusEffects::Accuracy].first - acc);
}

void EnemyCombat::DumpTemporaryStatus()
{
	activeStatusEffects[StatusEffects::Accuracy].first = baseAcc;
	previousBlock != 1u ? block -= 3u * previousBlock / 4u : block -= 1u;			//this combats immediate block falloff due to turn ending
	previousBlock = block;
}

void EnemyCombat::OnPlayerDeath()
{
	curState = State::GhoulWin;
}

void EnemyCombat::SetState(State st)
{
	curState = st;
}

EnemyCombat::State EnemyCombat::GetState() const
{
	return curState;
}
