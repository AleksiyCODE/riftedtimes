//coded by Chili
#pragma once
#include "Bindable.h"
#include <array>

namespace Bind
{
	class Rasterizer : public Bindable
	{
	public:
		Rasterizer( DXGraphics& gfx,bool twoSided );
		void Bind( DXGraphics& gfx ) noexcept override;
		static std::shared_ptr<Rasterizer> Resolve( DXGraphics& gfx,bool twoSided );
		static std::wstring GenerateUID( bool twoSided );
		std::wstring GetUID() const noexcept override;
	protected:
		Microsoft::WRL::ComPtr<ID3D11RasterizerState> pRasterizer;
		bool twoSided;
	};
}
