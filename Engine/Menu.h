#pragma once
#include "Framework/Keyboard.h"
#include "CombatManager.h"
#include "PressableMenu.h"
class Menu : public PressableMenu		//handles main menu logic
{
public:
	Menu(CombatManager& cm) :cm(cm) {};
	enum class MenuState
	{
		Main,
		Achivements,
		Tutorial_Selection,
		SkillInfo,
		OtherInfo,
		Tutorial1,
		Tutorial2,
		Tutorial3,
		ChoosingReward,
		Inactive,
		None
	};
	MenuState GetMenuState() const 
	{
		return mState;
	}
	void SetMenuState(MenuState state)
	{
		mState = state;
	}
	bool Update(Keyboard::Event in_event);		//returns 1 if 'quit' was hit
private:
	MenuState mState = MenuState::Main;
	CombatManager& cm;
};