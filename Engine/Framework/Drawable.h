//partly coded by Chili
#pragma once
#include "DXGraphics.h"
#include <DirectXMath.h>
#include "ConditionalNoexcept.h"
#include <memory>
#include "Technique.h"

namespace Bind
{
	class IndexBuffer;
	class VertexBuffer;
	class Topology;
	class InputLayout;
}
struct PositionData
{
	PositionData() = default;
	PositionData(float x, float y, float z, float roll, float pitch, float yaw) :
		pos(x, y, z),
		rot(roll, pitch, yaw)
	{};
	PositionData(float x, float y, float scale) :		//for 2D objcts
		pos(x, y, scale)
	{};
	PositionData(float x, float y, float scale, float rotation) :		//for 2D objcts with rotation
		pos(x, y, scale),
		rot(rotation, 0.0f, 0.0f)
	{};
	PositionData(const PositionData&) = default;
	PositionData(PositionData&&) = default;
	~PositionData() = default;
	PositionData& operator=(PositionData pd);
	PositionData& operator=(PositionData&& pd);
	DirectX::XMFLOAT3 pos = { 0.0f,0.0f,0.0f };			//for 2d objects first 2 coordinates mean x and y, and the third - scale
	DirectX::XMFLOAT3 rot = { 0.0f,0.0f,0.0f };			//and first coordinate here means rotation
};
struct LetterData
{
	DirectX::XMFLOAT2 pos = { 0.0f,0.0f };
	float rot =  0.0f;						//rotation is not implemented
	float scale = 1.0f;
	DirectX::XMFLOAT3 color = { 0.0f,  0.0f,  0.0f };
};
enum class Techniques		//all drawables are to implement Techniques in this order
							//not all Techniques have to be implemented for every drawable
							//but all Techniques, preceding the one you want to use have to be added (at least as empty ones)
{
	Standart,	
	ConsecutiveStandart,			//use this technique only in case where you are drawing the same drawable multiple times in one frame
									//it greatly decreaces the abount of binding done by the pipeline
									//also for this technique to work correctly, GetTransformXM() must be overloaded similarly to the overload in PlaneTexNorm.cpp 
	CamFacing,						//object will be drawn always facing camera
	ConsecutiveCamFacing,
};
class Drawable
{
public:
	Drawable() = default;
	Drawable(Drawable&&) = default;	//if you want classes derived from this to be movable
									//don't forget to specify in their move constructor
									//the call to RebindParent function, otherwise TransformeCbuf will have an invalid pointer
	virtual DirectX::XMMATRIX GetTransformXM() const noexcept = 0;
	virtual DirectX::XMFLOAT4 GetAdditionalTransformData()const { return{ 0.0f,0.0f,0.0f,0.0f }; };
	void Submit(class FrameCommander& frame, Techniques tec) const noexcept;
	void Bind(DXGraphics& gfx) const noexcept;
	UINT GetIndexCount() const noxnd;
	virtual ~Drawable() = default;
	template<class T>
	T* QueryBindable(size_t tecId = 0u, size_t stepId = 0u) noexcept
	{
		for (auto& pb : techniques[tecId].GetSteps()[stepId].GetBindables())
		{
			if (auto pt = dynamic_cast<T*>(pb.get()))
			{
				return pt;
			}
		}
		return nullptr;
	}
	std::vector<Technique>& GetTechniquesReference();		//is meant to be used for slight modifications in Technique 
protected:
	void RebindParent();
	void AddTechnique(Technique tech_in) noexcept;
	std::shared_ptr<Bind::IndexBuffer> pIndices;
	std::shared_ptr<Bind::VertexBuffer> pVertices;
	std::vector<Technique> techniques;
};