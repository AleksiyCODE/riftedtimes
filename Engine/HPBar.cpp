#include "HPBar.h"
#include <assert.h>
#include "Framework/BindableCommon.h"

FillingBar::FillingBar(DXGraphics& gfx, std::wstring fullBarFilename, std::wstring emptyBarFilename):
	HudElement(fullBarFilename, gfx)
{
	auto& s = GetTechniquesReference()[0].GetSteps()[0];
	using namespace Bind;
	auto texEmpty = Texture::Resolve(gfx, emptyBarFilename, 3);	//binds to third slot
	s.AddBindable(std::move(texEmpty));
	s.DeleteBindableByUID(L"class Bind::PixelShader#Shaders\\HudPS.cso");
	s.AddBindable(PixelShader::Resolve(gfx, L"Shaders\\HPBarPS.cso"));
	auto pcb = std::make_shared<PixelConstantBuffer<HPBarCBuff>>(gfx, 3);
	cbuf = pcb;
	s.AddBindable(pcb);
	BindPositionData(&refPosData);
}

void FillingBar::AddDrawingPosition(PositionData pd)
{
	assert(refPosData.size() < 10 && "forgot to clear FillingBar::posRefDara");
	refPosData.push_back(pd);
}

void FillingBar::ClearPositions()
{
	refPosData.clear();
}

void FillingBar::UpdateFillness(float fill, DXGraphics& gfx)
{
	cbData.fillness = fill;
	cbuf->Update(gfx, cbData);
}

//enemy stuff

EnemyBar::EnemyArmorBar::EnemyArmorBar(DXGraphics& gfx):FillingBar(gfx, L"Media//Sprites//EnemyBarArmorFull.png", L"Media//Sprites//EnemyBarArmorEmpty.png"){}

EnemyBar::EnemyHPBar::EnemyHPBar(DXGraphics& gfx):FillingBar(gfx, L"Media//Sprites//EnemyBarHPFull.png", L"Media//Sprites//EnemyBarHPEmty.png") {}

EnemyBar::EnemyBarHolder::EnemyBarHolder(DXGraphics& gfx):HudElement(L"Media//Sprites//EnemyBarHolder.png",gfx){}

EnemyBar::EnemyBar(DXGraphics& gfx):
	arm(gfx),
	hp(gfx),
	holder(gfx),
	cover(gfx),
	eye(gfx)
{
	arm.	AddParent(&holder);
	hp.		AddParent(&holder);
	cover.	AddParent(&holder);
	eye.	AddParent(&holder);
	arm.AddDrawingPosition({ 0.067f,-0.265f,0.86f  });
	hp. AddDrawingPosition({ 0.10f, -0.11f, 0.892f });
	holder.BindPositionData(&holderPosition);
	cover. BindPositionData(&coverPosition);
	holderPosition.emplace_back( -0.69f,0.785f,0.315f );
	coverPosition.emplace_back(-0.77f, -0.12f, 0.29f);
}

void EnemyBar::Submit(FrameCommander& fc) const
{
	holder.SmartSubmit(fc);
	arm.SmartSubmit(fc);
	hp.SmartSubmit(fc);
	cover.SmartSubmit(fc);
	eye.SmartSubmit(fc);
}

void EnemyBar::UpdateFillness(float hpFill, float armFill, DXGraphics& gfx, float additionalData)
{
	arm.UpdateFillness(armFill, gfx);
	hp.UpdateFillness(hpFill, gfx);
	eye.UpdateScale(additionalData);
}

EnemyBar::EnemyBarCover::EnemyBarCover(DXGraphics& gfx) :HudElement(L"Media//Sprites//EnemyBarCover.png", gfx) {}

//player stuff

PlayerBar::PlayerArmorBar::PlayerArmorBar(DXGraphics& gfx) : FillingBar(gfx, L"Media//Sprites//PlayerBarArmorFull.png", L"Media//Sprites//PlayerBarArmorEmpty.png") {}
PlayerBar::PlayerHPBar::PlayerHPBar(DXGraphics& gfx) :		 FillingBar(gfx, L"Media//Sprites//PlayerBarHPFull.png", L"Media//Sprites//PlayerBarHPEmty.png") {}
PlayerBar::PlayerBarHolder::PlayerBarHolder(DXGraphics& gfx) :  HudElement(L"Media//Sprites//PlayerBarHolder.png", gfx) {}
PlayerBar::PlayerBarCover::PlayerBarCover(DXGraphics& gfx) :	HudElement(L"Media//Sprites//PlayerBarCover.png", gfx) {}

PlayerBar::PlayerBar(DXGraphics& gfx) :
	arm(gfx),
	hp(gfx),
	holder(gfx), 
	cover(gfx)
{
	arm.  AddParent(&holder);
	hp.   AddParent(&holder);
	cover.AddParent(&holder);
	arm.AddDrawingPosition({ 0.17f,-0.195f,0.76f });
	hp.AddDrawingPosition({ 0.19f,-0.328f,0.8f });
	holder.BindPositionData(&holderPosition);
	cover.BindPositionData(&coverPosition);
	holderPosition.emplace_back(-0.65f, -0.75f, 0.351f);
	coverPosition.emplace_back(-0.58f, -0.29f, 0.15f);
}

void PlayerBar::Submit(FrameCommander& fc) const
{
	holder.SmartSubmit(fc);
	arm.SmartSubmit(fc);
	hp.SmartSubmit(fc);
	cover.SmartSubmit(fc);
}

void PlayerBar::UpdateFillness(float hpFill, float armFill, DXGraphics& gfx, float additionalData)
{
	arm.UpdateFillness(armFill, gfx);
	hp.UpdateFillness(hpFill, gfx);
}

EnemyBar::EnemyEye::EnemyEye(DXGraphics& gfx):HudElement(L"Media//Sprites//EnemyBarEye.png",gfx)
{
	posData.emplace_back(-0.375f, 0.415f, 0.0f);
	BindPositionData(&posData);
}

void EnemyBar::EnemyEye::UpdateScale(float scale)
{
	posData[0].pos.z = (scale-0.5f)/3.0f;
}
