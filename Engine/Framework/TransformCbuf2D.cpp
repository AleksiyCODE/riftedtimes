#include "TransformCbuf2D.h"
#include "ChiliMath.h"
#include <math.h>
#include "Vec3.h"
namespace Bind
{
	TransformCbuf2D::TransformCbuf2D(DXGraphics& gfx, UINT slot) 
	{
		if (!pVcbuf)				//checking because it is a static variable
		{
			pVcbuf = std::make_unique<VertexConstantBuffer<Transforms2D>>(gfx, slot);
		}
	}

	void TransformCbuf2D::Bind(DXGraphics& gfx) noexcept
	{
		UpdateBindImpl(gfx, GetTransforms(gfx));
	}

	void TransformCbuf2D::InitializeParentReference(const Drawable& parent) noexcept
	{
		pParent = &parent;
	}

	void TransformCbuf2D::UpdateBindImpl(DXGraphics& gfx, const Transforms2D& tf) noexcept
	{
		assert(pParent != nullptr);
		pVcbuf->Update(gfx, tf);
		pVcbuf->Bind(gfx);
	}

	TransformCbuf2D::Transforms2D TransformCbuf2D::GetTransforms(DXGraphics& gfx) noexcept
	{
		assert(pParent != nullptr);
		const auto encoded = pParent->GetTransformXM();		
		DirectX::XMFLOAT4X4 stored;
		DirectX::XMStoreFloat4x4(&stored, encoded);
		Transforms2D tf;
		tf.transform.x = stored._11;
		tf.transform.y = stored._12;
		tf.transform.z = stored._13;
		tf.transform.w = stored._14;	//is used by some entities (for example HPBar)
		return std::move(tf);
	}
	std::unique_ptr<VertexConstantBuffer<TransformCbuf2D::Transforms2D>> TransformCbuf2D::pVcbuf;
}