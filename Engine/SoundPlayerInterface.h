#pragma once
#include "SoundPlayer.h"			//included here (and not in cpp) so that everyone learns bout Sounds enumeration
#define SPI SoundPlayerInterface	
class SoundPlayerInterface			
{
	friend class SoundPlayer;		
public:	
	static void Play(Sounds soundToPlay,float freqMod = 1.0f, float volMod = 0.4f, float playbackSpeed = 1.0f);
	static void Stop(Sounds soundToStop);
	static void StopAllInstances(Sounds);
private:
	static void LinkToPlayer(class SoundPlayer* in_player);
	static class SoundPlayer* player;				
};

