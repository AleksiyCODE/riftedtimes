#include "BlurManager.h"
#include <algorithm>
#include "Framework\FrameCommander.h"

void BlurManager::AddBlur(float blur)
{
	pendingBlur += blur;
}

void BlurManager::UpdateBlur(float dt)
{
	pendingBlur = std::min(pendingBlur*(blurFalloff*dt), pendingBlur*0.99999f);
	float coeff = blurChangeSensitivity * dt;
	currentBlur = currentBlur * (1.0f - coeff) + pendingBlur * coeff;
	if (currentBlur <= 0.001f)currentBlur = 0.0f;
	FrameCommander::SetBlurMod(currentBlur);
}

float  BlurManager::pendingBlur = 0.0f;
