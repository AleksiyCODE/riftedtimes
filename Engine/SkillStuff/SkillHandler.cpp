#include "SkillHandler.h"

bool SkillHandler::Update(float dt)
{
	if(!pendingSkills.empty())
	{		
		curTime += dt;
		if (!acted)
		{
			if (curTime > pendingSkills.front()->preActTime)
			{
				pendingSkills.front()->Activate();
				acted = true;
				curTime -= pendingSkills.front()->preActTime;
			}
		}
		else
		{
			if (curTime > pendingSkills.front()->actTime)
			{
				acted = false;
				curTime -= pendingSkills.front()->actTime;
				pendingSkills.pop(); 
				if (!pendingSkills.empty())pendingSkills.front()->OnPend();
			}
		}
		return false;
	}
	else
	{
		curTime = 0.0f;
		acted = false;
		return true;
	}
}

void SkillHandler::PendSkill(Skill* s)
{
	if(pendingSkills.empty())s->OnPend();
	pendingSkills.push(s);
}

void SkillHandler::DiscardPending()
{
	while (!pendingSkills.empty())
	{
		pendingSkills.pop();
	}
}
