#include "HudTransform.hlsl"
struct VSOut
{    
    float2 tc : Texcoord;
    float4 SVpos : SV_Position;
};

VSOut main(float2 pos : Position, float2 tc : Texcoord)             //!only works with model being a rectangle with center in origin
{
    VSOut vso;
    if (transform[3]!=0.0f)
    {        
        float ar = 1.7778f;
        pos[1] /= ar;                                   //yea, that is the way i handle sprite rotation
        float2 rotated =                                //no idea why it works
        {
        pos[0] * cos(transform[3]) - pos[1] * sin(transform[3]),
        pos[0] * sin(transform[3]) + pos[1] * cos(transform[3])
        };
        rotated[0] /= (ar);
        vso.SVpos[0] = transform[0] + rotated[0] * transform[2];
        vso.SVpos[1] = transform[1] + rotated[1] * transform[2];

    }
    else
    {
        vso.SVpos[0] = transform[0] + pos[0] * transform[2];
        vso.SVpos[1] = transform[1] + pos[1] * transform[2];
    }
    vso.SVpos[2] = 1.0f;
    vso.SVpos[3] = 1.0f;
    vso.tc = tc;
        return vso;
}