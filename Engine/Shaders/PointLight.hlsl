#include "NumOfLights.hlsl"

cbuffer PointLightCBuf
{
    float3 ambient          [NUM_OF_LIGHTS];
    float3 diffuseColor     [NUM_OF_LIGHTS];
    float4 intensityParam  [NUM_OF_LIGHTS];         //first float - intencity, second - constanf attenuation, third - linear, fourth - quadratic
};