#include "AchievementManager.h"
#include <assert.h>
#include <iostream>
#include <fstream>
#include <string>

void AchievementManager::Alert(Event e, float emount)
{
	switch (e)
	{
    case AchievementManager::Event::PlayerUpgradedAllSkills:     //alerted by ActionBox::UpgradeRandomSkill()
        unlocksThisFrame.push_back(AchievementModul::Achievement::LockedAndLoaded);
        break;

    case AchievementManager::Event::PlayerUsedSandStorm:         //alerted by SandStorm::Activate()
        sandStormWasUsed = true;
        break;

    case AchievementManager::Event::PlayerLostHP:                //alerted by PlayerManager::SufferPhysicalDamage and CombatEntity::SufferPureDamage(size_t incDmg)
        turnsWithoutDamage = 0u;
        playerTookDamageThisTurn = true;
        break;

    case AchievementManager::Event::PLayerUsedDustThrow:        //alerted by DustThrow::Activate()
        dustThrowWasUsed = true;
        break;

    case AchievementManager::Event::EnemyDied:                  //alerted by CombatManager::OnEnemyDeath()
        enemyDiedThisTurn = true;
        break;

    case AchievementManager::Event::PlayerUsedSwordStrike:       //alerted by Cut::Activate()
        playerUsedSwordStrike = true;
        break;

    case AchievementManager::Event::PlayerUsed6Skills:          //alerted by ActionBox
        unlocksThisFrame.push_back(AchievementModul::Achievement::Quickdraw);
        break;

    case AchievementManager::Event::PlayerCrited:               //alerted by CombatManager::HandleEvent
        playerCrited = true;
        break;

    case AchievementManager::Event::PlayerUsedBlock:            //alerted by Deflect::Activate()
        playerUsedBlock = true;
        break;

    case AchievementManager::Event::GhoulChoseToBlock:          //alerted by Ghoul::Choose
        enemyDecidedToBlock = true;
        break;

    case AchievementManager::Event::PlayerDiedToBloodloss:      //alerted by CombatEntity::SufferPureDamage
        unlocksThisFrame.push_back(AchievementModul::Achievement::Bleedout);
        break;

    case AchievementManager::Event::PlayerHealed20Bleed:        //alerted by CombatEntity::CureBleed()
        unlocksThisFrame.push_back(AchievementModul::Achievement::FirstAid);
        break;

    case AchievementManager::Event::PlayerKilled8Enemies:       //alerted by CombatManager::IncreaseKillcount()
        unlocksThisFrame.push_back(AchievementModul::Achievement::Slayer);
        break;

    case AchievementManager::Event::PlayerGotHit:               //alerted by CombatManager::HandleEvent(SkillEvent ev)
        playerWasHitThisTurn = true;
        break;

    case AchievementManager::Event::PlayerTurnBegan:
        break;

    case AchievementManager::Event::PlayerTurnEnded:
        break;

    case AchievementManager::Event::TurnBegin:                  //alerted by CombatManager::StartNewTurn()
        enemyDecidedToBlock = false;
        playerUsedSwordStrike = false;     
        playerUsedBlock = false;
        enemyDiedThisTurn = false;
        playerTookDamageThisTurn = false;
        playerWasHitThisTurn = false;
        sandStormWasUsed = false;
        dustThrowWasUsed = false;
        playerCrited = false;
        break;

    case AchievementManager::Event::TurnEnd:                    //alerted by CombatManager::UpdateCombatState(float dt) and CombatManager::OnEnemyDeath()
        if (!playerTookDamageThisTurn)turnsWithoutDamage++;
        if (turnsWithoutDamage >= 5u)unlocksThisFrame.push_back(AchievementModul::Achievement::Untouchable);
        if (playerWasHitThisTurn && sandStormWasUsed && dustThrowWasUsed)unlocksThisFrame.push_back(AchievementModul::Achievement::SnakeEyes);
        if(!playerUsedSwordStrike&&enemyDiedThisTurn&&dustThrowWasUsed)unlocksThisFrame.push_back(AchievementModul::Achievement::EatMyDust);
        if(playerUsedBlock&&enemyDecidedToBlock)unlocksThisFrame.push_back(AchievementModul::Achievement::Copycat);
        if(playerCrited&&enemyDiedThisTurn)unlocksThisFrame.push_back(AchievementModul::Achievement::HeavyBlow);
        break;
    default:
        assert(false && "missed an event case in AchievementManager::Alert");
        break;
	}
}

void AchievementManager::ResetState()
{
   isPlayerTurn = false;
   turnsWithoutDamage = 0u;
   playerTookDamageThisTurn = false;
   playerWasHitThisTurn = false;
   sandStormWasUsed = false;
   turnsUsedSandStormAndWasHit = 0u;
   dustThrowWasUsed = false;
   enemyDiedThisTurn = false;
   playerUsedSwordStrike = false;
   enemyDecidedToBlock = false;
   playerUsedBlock = false;
   playerCrited = false;
}

const AchievementModul& AchievementManager::GetAchievementModul()
{
    return modul;
}

void AchievementManager::Init()
{
    modul.LoadAchievements();
}

void AchievementManager::Update()
{
    if (unlocksThisFrame.size())
    {
        modul.AchUnlocked(unlocksThisFrame.back());
        unlocksThisFrame.pop_back();
    }
}

void AchievementModul::LoadAchievements()
{
    std::ifstream inFile;
    inFile.open("achievements.txt");
    if (!inFile)
    {                       //if there is no such file
        std::ofstream outfile("achievements.txt");
        outfile << "0#0\n1#0\n2#0\n3#0\n4#0\n5#0\n6#0\n7#0\n8#0\n9#0" << std::endl;
        outfile.close();
        inFile.open("achievements.txt");
    }
    std::string line1;
    while (std::getline(inFile, line1))
    {
        achievements.emplace(static_cast<Achievement>(line1[0]), static_cast<bool>(line1[2]-'0'));
    }
    inFile.close();
}

void AchievementModul::DumpToFile()const
{
    std::ofstream outfile("achievements.txt");
    for (auto& e : achievements)
    {
        outfile << std::to_string(static_cast<size_t>(e.first)-'0') << '#' << std::to_string(e.second)<<"\n";
    }
    outfile.close();
}

void AchievementModul::AchUnlocked(Achievement a)
{
    if (!achievements[a])
    {
        achievements[a] = true;
        DumpToFile();
    }
}

const std::map<AchievementModul::Achievement, bool>& AchievementModul::GetAchievements()const
{
    return achievements;
}

bool   AchievementManager::isPlayerTurn = false;
size_t AchievementManager::turnsWithoutDamage = 0u;
bool   AchievementManager::playerTookDamageThisTurn = false;
bool   AchievementManager::playerWasHitThisTurn = false;
bool   AchievementManager::sandStormWasUsed = false;
size_t AchievementManager::turnsUsedSandStormAndWasHit = 0u;	
bool   AchievementManager::dustThrowWasUsed = false;
bool   AchievementManager::enemyDiedThisTurn = false;
bool   AchievementManager::playerUsedSwordStrike = false;
bool   AchievementManager::enemyDecidedToBlock = false;
bool   AchievementManager::playerUsedBlock = false;
bool   AchievementManager::playerCrited = false;
AchievementModul AchievementManager::modul;
std::vector<AchievementModul::Achievement> AchievementManager::unlocksThisFrame;
