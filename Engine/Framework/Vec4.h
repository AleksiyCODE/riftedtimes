//coded by Chili
#pragma once

#include <algorithm>
#include "ChiliMath.h"
#include "Vec3.h"

template <typename T>
class _Vec4 : public _Vec3<T>
{
public:
	_Vec4() = default;
	_Vec4( T x,T y,T z,T w )
		:
		_Vec3<T>( x,y,z ),
		w( w )
	{}
	_Vec4( const _Vec3<T>& v3,float w = 1.0f  )
		:
		_Vec3<T>( v3 ),
		w( w )
	{}
	template <typename T2>
	explicit operator _Vec4<T2>() const
	{
		return{ (T2)_Vec2<T>::x,(T2)_Vec2<T>::y,(T2)_Vec3<T>::z,(T2)w };
	}
	_Vec4	operator-() const
	{
		return _Vec4( -_Vec2<T>::x,-_Vec2<T>::y,-_Vec3<T>::z,-w );
	}
	_Vec4&	operator=( const _Vec4 &rhs )
	{
		_Vec2<T>::x = rhs._Vec2<T>::x;
		_Vec2<T>::y = rhs._Vec2<T>::y;
		_Vec3<T>::z = rhs._Vec3<T>::z;
		w = rhs.w;
		return *this;
	}
	_Vec4&	operator+=( const _Vec4 &rhs )
	{
		_Vec2<T>::x += rhs._Vec2<T>::x;
		_Vec2<T>::y += rhs._Vec2<T>::y;
		_Vec3<T>::z += rhs._Vec3<T>::z;
		w += rhs.w;
		return *this;
	}
	_Vec4&	operator-=( const _Vec4 &rhs )
	{
		_Vec2<T>::x -= rhs._Vec2<T>::x;
		_Vec2<T>::y -= rhs._Vec2<T>::y;
		_Vec3<T>::z -= rhs._Vec3<T>::z;
		w -= rhs.w;
		return *this;
	}
	_Vec4	operator+( const _Vec4 &rhs ) const
	{
		return _Vec4( *this ) += rhs;
	}
	_Vec4	operator-( const _Vec4 &rhs ) const
	{
		return _Vec4( *this ) -= rhs;
	}
	_Vec4&	operator*=( const T &rhs )
	{
		_Vec2<T>::x *= rhs;
		_Vec2<T>::y *= rhs;
		_Vec3<T>::z *= rhs;
		w *= rhs;
		return *this;
	}
	_Vec4	operator*( const T &rhs ) const
	{
		return _Vec4( *this ) *= rhs;
	}
	_Vec4&	operator/=( const T &rhs )
	{
		_Vec2<T>::x /= rhs;
		_Vec2<T>::y /= rhs;
		_Vec3<T>::z /= rhs;
		w /= rhs;
		return *this;
	}
	_Vec4	operator/( const T &rhs ) const
	{
		return _Vec4( *this ) /= rhs;
	}
	bool	operator==( const _Vec4 &rhs ) const
	{
		return _Vec2<T>::x == rhs._Vec2<T>::x && _Vec2<T>::y == rhs._Vec2<T>::y && rhs._Vec3<T>::z == _Vec3<T>::z && rhs.w == w;
	}
	bool	operator!=( const _Vec4 &rhs ) const
	{
		return !(*this == rhs);
	}
	// clamp to between 0.0 ~ 1.0
	_Vec4&	Saturate()
	{
		_Vec2<T>::x = std::min( 1.0f,std::max( 0.0f,_Vec2<T>::x ) );
		_Vec2<T>::y = std::min( 1.0f,std::max( 0.0f,_Vec2<T>::y ) );
		_Vec3<T>::z = std::min( 1.0f,std::max( 0.0f,_Vec3<T>::z ) );
		w = std::min( 1.0f,std::max( 0.0f,w ) );
		return *this;
	}
	// clamp to between 0.0 ~ 1.0
	_Vec4	GetSaturated() const
	{
		_Vec4 temp( *this );
		temp.Saturate();
		return temp;
	}
	// x3 = x1 * x2 etc.
	_Vec4&  Hadamard( const _Vec4& rhs )
	{
		_Vec2<T>::x *= rhs._Vec2<T>::x;
		_Vec2<T>::y *= rhs._Vec2<T>::y;
		_Vec3<T>::z *= rhs._Vec3<T>::z;
		w *= rhs.w;
		return *this;
	}
	// x3 = x1 * x2 etc.
	_Vec4	GetHadamard( const _Vec4& rhs ) const
	{
		_Vec4 temp( *this );
		temp.Hadamard( rhs );
		return temp;
	}
public:
	T w;
};

typedef _Vec4<float> Vec4;
typedef _Vec4<double> Ved4;
typedef _Vec4<int> Vei4;