#include "Skill.h"
SkillProcessResult Skill::ProcessPress()const
{
	if (!avalible)
	{
		return BlockedIgnore;
	}
	if (!quick)
	{
		return Finish;
	}
	else return Continue;
}

void Skill::SetAvalibility(bool upd)
{
	avalible = upd;
}

bool Skill::GetAvalibility() const
{
	return avalible;
}

size_t Skill::GetID() const
{
	return id;
}

void Skill::Rewind()
{
	if (!avalible)
	{
		if (!wasRewound)
		{
			wasRewound = true;
			avalible = true;
		}
	}
}

void Skill::ResetTurn()
{
	wasRewound = false;
}
