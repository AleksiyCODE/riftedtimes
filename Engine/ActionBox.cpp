#include "ActionBox.h"
#include "./SkillStuff/Skill.h"
#include "RandomUnit.h"
#include "SoundPlayerInterface.h"
#include "AchievementManager.h"

ActionBox::ActionBox(std::vector<std::unique_ptr<Skill>>& skillPool) :
	boxPool(skillPool)
{
	keyToSkill.emplace_back('W');
	keyToSkill.emplace_back('A');
	keyToSkill.emplace_back('S');
	keyToSkill.emplace_back('D');
	InitializeAboxSkillInfoMicroicons();
};

void ActionBox::PullNewSkills()
{
	SetPressedKey(Key::All);
	pickedSkills.clear();
	std::vector<std::unique_ptr<Skill>*> temp;
	for (auto& s: boxPool)			
	{
		temp.push_back(&s);
	}
	for (int i = 0; i < nBoxSkills; i++)
	{
		size_t k = (size_t)(RU.GetUInt(0u, (size_t)temp.size() - 1u));
		pickedSkills.push_back(temp[k]);
		temp.erase(temp.begin() + k);
	}
	for (size_t i = 0u; i < pickedSkills.size(); i++)
	{
		if (pickedSkills[i]->get()->IsRewinding()) aboxSkillInfoMicroicons[i][SkillInfoMicroicons::Rewinding] = true;
		else  aboxSkillInfoMicroicons[i][SkillInfoMicroicons::Rewinding] = false;		

		if (pickedSkills[i]->get()->IsUpgraded()) aboxSkillInfoMicroicons[i][SkillInfoMicroicons::Upgraded] = true;
		else  aboxSkillInfoMicroicons[i][SkillInfoMicroicons::Upgraded] = false;

		if (pickedSkills[i]->get()->IsQuick())
		{
			aboxSkillInfoMicroicons[i][SkillInfoMicroicons::Quick] = true;
			aboxSkillInfoMicroicons[i][SkillInfoMicroicons::TurnEnder] = false;
		}
		else
		{
			aboxSkillInfoMicroicons[i][SkillInfoMicroicons::Quick] = false;
			aboxSkillInfoMicroicons[i][SkillInfoMicroicons::TurnEnder] = true;
		}
	}
	skillsActivatedThisTurn = 0u;
	sh.DiscardPending();		//prevents execution of skills, activated the previous turn
}

void ActionBox::UnlockSkills()
{
	for (auto& el : pickedSkills)
	{
		el->get()->SetAvalibility(true);
		el->get()->ResetTurn();
	}
}

void ActionBox::BlockSkills()
{
	for (auto& el : pickedSkills)
	{
		el->get()->SetAvalibility(false);
	}
}

ABoxDrawingInfo ActionBox::GetPickedSkills() const
{
	ABoxDrawingInfo abdi;
	for (size_t i = 0;i<pickedSkills.size();i++)
	{
		if (pickedSkills[i]->get()->GetAvalibility())abdi.s[i] = static_cast<Skill0>(pickedSkills[i]->get()->GetID());		//gets normal 
		else abdi.s[i] = static_cast<Skill0>(pickedSkills[i]->get()->GetID()+static_cast<size_t>(Skill0::CutBlocked));		//gets blocked
	}
	return std::move(abdi);
}

const std::vector<std::map<SkillInfoMicroicons, bool>>& ActionBox::GetPickedSkillsInfo() const
{
	return aboxSkillInfoMicroicons;
}

bool ActionBox::ProcessInput(const char input)
{
	bool ending = false;
	for (int i = 0; i < keyToSkill.size(); i++)
	{
		if (input == keyToSkill[i])
		{
			bool blocked = false;
			switch (pickedSkills[i]->get()->ProcessPress())													
			{
			case Finish:
				BlockSkills();
				sh.PendSkill(pickedSkills[i]->get());
				PlayPressSound();
				ending =  true;				
				break;
			case Continue:										
				for (auto el : pickedSkills)
				{
					if (el->get()->IsRewinding())
					{
						el->get()->Rewind();
					}
				}
				pickedSkills[i]->get()->SetAvalibility(false);
				sh.PendSkill(pickedSkills[i]->get());
				PlayPressSound();
				break;
			case BlockedIgnore:
				SPI::Play(Sounds::ABoxBlockedPress);
				blocked = true;
				break;
			default:
				break;
			}
			if (!blocked)
			{
				skillsActivatedThisTurn++;
				switch (i)
				{
				case 0:
					SetPressedKey(Key::W);
					break;
				case 1:
					SetPressedKey(Key::A);
					break;
				case 2:
					SetPressedKey(Key::S);
					break;
				case 3:
					SetPressedKey(Key::D);
					break;
				}
			}
		}
	}
	if (skillsActivatedThisTurn >= 6)AM::Alert(AM::Event::PlayerUsed6Skills);
	return ending;
}

ActionBox::TurnState ActionBox::UpdateTurnState(float dt)
{

	bool animation = !sh.Update(dt);
	bool notFinished = 0;						
	for (size_t i = 0; i < pickedSkills.size(); i++)
	{
		notFinished = pickedSkills[i]->get()->IsAvalible() || notFinished;
	}
	if (notFinished&&animation)
	{		
		return TurnState::NotDoneAnimation;
	}
	else if (notFinished && !animation)
	{
		return  TurnState::NotDone;
	}
	else if (!notFinished && animation)
	{
		return TurnState::DoneAnimation;
	}
	else if (!notFinished && !animation)
	{
		return	TurnState::Done;
	}
	assert(false && "invalid something in ActionBox::UpdateTurnState ");
	return	TurnState::Done;
}

bool ActionBox::UpgradeRandomSkill()
{
	bool upgraded = false;
	std::vector<std::unique_ptr<Skill>*> temp;
	for (auto& s : boxPool)
	{
		temp.push_back(&s);
	}
	for (int i = 0; i < boxPool.size(); i++)
	{
		size_t k = (size_t)(RU.GetUInt(0u, temp.size() - 1u));
		if (!temp[k]->get()->IsUpgraded())
		{
			temp[k]->get()->Upgrade();
			upgraded = true;
		}
		temp.erase(temp.begin() + k);
		if (upgraded)break;
	}
	if (!upgraded)AM::Alert(AM::Event::PlayerUpgradedAllSkills);
	return !upgraded;
}

void ActionBox::Reset()
{
	InitializeAboxSkillInfoMicroicons();
}

void ActionBox::PlayPressSound()
{
	SPI::Play(Sounds::ABoxSkillPress,1.0f+0.2f*(float)skillsActivatedThisTurn);
}

void ActionBox::InitializeAboxSkillInfoMicroicons()
{
	for(size_t i = 0u; i < nBoxSkills; i++)aboxSkillInfoMicroicons.emplace_back();
	for (auto& absim : aboxSkillInfoMicroicons)
	{
		absim.emplace(SkillInfoMicroicons::Quick, false);
		absim.emplace(SkillInfoMicroicons::TurnEnder, false);
		absim.emplace(SkillInfoMicroicons::Rewinding, false);
		absim.emplace(SkillInfoMicroicons::Upgraded, false);
	}
}

