#include "CombatTimers.h"

bool CombatTimers::UpdateTimer(float dt)
{
	if (curTimer == ActiveTimer::Countdown)
	{

		if (countTimer.Update(dt))
		{

			curTimer = ActiveTimer::NoActive;
			return 1;
		}
	}
	else if (curTimer == ActiveTimer::Turn)
	{
		if (turnTimer.Update(dt))
		{
			curTimer = ActiveTimer::NoActive;
			return 1;
		}
	}
	return 0;
}

void CombatTimers::SkipTimer()
{
	if (curTimer == ActiveTimer::Countdown)
	{
		countTimer.Skip();
	}
	else if (curTimer == ActiveTimer::Turn)
	{
		turnTimer.Skip();
	}
}

void CombatTimers::Stop()
{
	turnTimer.Reset(0.0f);
}

void CombatTimers::DrawTimer()const
{
	{
		if (curTimer == ActiveTimer::Countdown)
		{
			countTimer.DrawDigit();
		}
		else if (curTimer == ActiveTimer::Turn)
		{
			turnTimer.Draw();
		}
	}
}

void CombatTimers::SetActiveTimer(ActiveTimer t, float turnTime)
{
	curTimer = t;
	if (curTimer == ActiveTimer::Countdown)
	{
		SPI::Play(Sounds::CountdownTick, 1.0f, 0.25f);
		countTimer.Reset();
	}
	else if (curTimer == ActiveTimer::Turn)
	{
		turnTimer.Reset(turnTime);
	}
}
