#pragma once
#include<vector>
#include<tuple>
#include"CombatEntity.h"
#include "SkillStuff\Skill.h"
class EnemyCombat :public CombatEntity
{
public:
	enum class State
	{
		GhoulAttack,
		GhoulRage,
		GhoulDead,
		GhoulPreparingAttack,
		GhoulPreparingRage,
		GhoulCovering,
		GhoulPreparingCover,
		GhoulIdle,
		GhoulInvisible,
		GhoulWin,
		NoEntity
	};
	EnemyCombat();

	virtual ~EnemyCombat() = default;

	size_t SufferPhysicalDamage(size_t incDmg) override;

	float GetCurAcc()const;

	void DecreaceAcc(float acc);

	void DumpTemporaryStatus()override;

	virtual void Act()  = 0;			

	virtual enum class AbilityType Choose(bool justReset = false) = 0;

	void OnPlayerDeath();

	void SetState(State st);

	State GetState()const;

protected:
	int chosenAbility;				//its position in pool
	std::vector<std::tuple<std::unique_ptr<Skill>, float, float>> abilities;	//first float represents default perk chance, second current. it is used for emulating behaviour
	float	baseDodge = 0;
	float	baseAcc = 0.95f;
	size_t previousBlock = 0u;
	State	curState = State::GhoulInvisible;
};