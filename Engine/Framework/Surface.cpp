//coded by Chili
#include "Surface.h"
#include "ChiliException.h"
#include <sstream>
#include "MainWindow.h"
Surface::Surface(unsigned int width, unsigned int height)
{
	HRESULT hr = scratch.Initialize2D(
		format,
		width, height, 1u, 1u
	);
	if (FAILED(hr))
	{
		throw Surface::Exception(__LINE__, _CRT_WIDE(__FILE__), L"no file", L"Failed to initialize ScratchImage",hr);
	}
}

bool Surface::AlphaLoaded() const noexcept
{
	return !scratch.IsAlphaAllOpaque();
}

Surface::Surface(DirectX::ScratchImage scratch) noexcept
	:
	scratch(std::move(scratch))
{}

Surface::Surface( const std::wstring & fileName )
{
	DirectX::ScratchImage tScratch;
	HRESULT hr = DirectX::LoadFromWICFile(fileName.c_str(), DirectX::WIC_FLAGS_NONE, nullptr, tScratch);
	if (FAILED(hr))
	{
		throw Surface::Exception(__LINE__, _CRT_WIDE(__FILE__), fileName, L"Failed to load image", hr);
	}
	if (tScratch.GetImage(0, 0, 0)->format != format)
	{
		DirectX::ScratchImage converted;
		hr = DirectX::Convert(
			*tScratch.GetImage(0, 0, 0),
			format,
			DirectX::TEX_FILTER_DEFAULT,
			DirectX::TEX_THRESHOLD_DEFAULT,
			converted
		);
		if (FAILED(hr))
		{
			throw Surface::Exception(__LINE__, _CRT_WIDE(__FILE__), fileName, L"Failed to convert image", hr);
		}
		scratch = std::move(converted);
	}
	else
	{
		scratch = std::move(tScratch);
	}
}

// surface exception stuff
Surface::Exception::Exception(int line, const wchar_t* file, std::wstring note) noexcept
	:
	ChiliException(line, file),
	note(std::move(note))
{}

Surface::Exception::Exception(int line, const wchar_t* file, std::wstring filename, std::wstring note, std::optional<HRESULT> hr) noexcept
	:
	ChiliException(line, file)
{
	using namespace std::string_literals;
	whatBuffer = L"[File] "s + filename + L"\n"s + L"[Note] "s + note;
	if (hr)
	{
		whatBuffer = L"[Error String] " + MainWindow::Exception::TranslateErrorCode(*hr) + whatBuffer;
	}
}

const wchar_t* Surface::Exception::MyWhat() const noexcept
{
	std::wstringstream oss;
	oss << ChiliException::MyWhat() << std::endl
		<< L"[Note] " << GetNote();
	whatBuffer = oss.str();
	return whatBuffer.c_str();
}

const wchar_t* Surface::Exception::GetType() const noexcept
{
	return L"Chili Graphics Exception";
}

const std::wstring& Surface::Exception::GetNote() const noexcept
{
	return note;
}


