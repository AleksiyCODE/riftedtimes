#include "PlayerManager.h"
#include "SoundPlayerInterface.h"
#include "SkillStuff/Skills0.h"
#include "AchievementManager.h"
#include "HudOverlay.h"

PlayerManager::PlayerManager():
	actionBox(skillPool)
{
	Reset();
}

void PlayerManager::BeginBattle()
{
	ResetTimePool();
	actionBox.UnlockSkills();			//in case some were locked before the fight ended
	actionBox.PullNewSkills();
	actionBox.BlockSkills();
	Heal(size_t((float)(maxHP - HP) * 0.1f));
}

void PlayerManager::StartTurn()
{
	actionBox.UnlockSkills();
	actionBox.PullNewSkills();
	AM::Alert(AM::Event::PlayerTurnBegan);
}

void PlayerManager::EndTurn()
{
	actionBox.BlockSkills();
	AM::Alert(AM::Event::PlayerTurnEnded);
}

ActionBox::TurnState PlayerManager::UpdateTurnState(float dt)
{
	return actionBox.UpdateTurnState(dt);
}

bool PlayerManager::ProcessInput(const char input)
{
	return actionBox.ProcessInput(input);
}

bool PlayerManager::UpgradeRandomSkill()
{
	return actionBox.UpgradeRandomSkill();
}

void PlayerManager::Reset()
{
	HP =	250u;
	maxHP = 250u;
	block = 0u;
	baseCrit = 0.0f;
	baseDodge = 0.0f;
	baseTurnTime = 10.0f;
	activeStatusEffects[StatusEffects::CritChance].first = baseCrit;
	activeStatusEffects[StatusEffects::Dodge].first = baseDodge;
	skillPool.clear();
	skillPool.push_back(std::make_unique<Cut>(this));
	skillPool.push_back(std::make_unique<Cut>(this));
	skillPool.push_back(std::make_unique<Cut>(this));
	skillPool.push_back(std::make_unique<Deflect>(this));
	skillPool.push_back(std::make_unique<SandStorm>(this));
	skillPool.push_back(std::make_unique<SharpenTheBlade>(this));
	skillPool.push_back(std::make_unique<SandsOfTime>(this));
	skillPool.push_back(std::make_unique<DustThrow>(this));
	actionBox.Reset();
	CureBleed();
	speed = 1.0f;
	isDead = false;
}

size_t PlayerManager::SufferPhysicalDamage(size_t incDmg)
{
	if (incDmg < HP + block)		//if does not kill
	{
		if (block >= incDmg)
		{
			SPI::Play(Sounds::Player_Block, 1.0f, (std::max)(0.1f, (std::min)(0.8f, incDmg*0.1f)));
			return 0u;
		}
		else
		{
			incDmg -= block;
			HP -= incDmg;
			if (block) SPI::Play(Sounds::Player_Block, 1.0f, (std::max)(0.1f, (std::min)(0.8f, block*0.015f)));
			SPI::Play(Sounds::Player_Hit, 1.0f, (std::max)(0.1f, (std::min)(0.9f, incDmg*0.09f)));		
			AM::Alert(AM::Event::PlayerLostHP);
			HudOverlay::BoostOverlay(std::max(0.5f, 0.03857f * (float)incDmg));
			return incDmg;
		}
	}
	else
	{
		auto dmg = HP;
		HP = 0;
		OnDeath();
		AM::Alert(AM::Event::PlayerLostHP);
		HudOverlay::BoostOverlay(2.0f);
		return dmg;
	}
}

float PlayerManager::GetCritMod() const
{
	return activeStatusEffects.at(StatusEffects::CritChance).first;
}

const ActionBox& PlayerManager::GetActionBox() const
{
	return actionBox;
}

float PlayerManager::GetDodge() const
{
	return activeStatusEffects.at(StatusEffects::Dodge).first;
}

float PlayerManager::GetTurnTime() const
{
	return curTurnTime;
}

void PlayerManager::AddTimePool(float in_time)
{
	curTurnTime += in_time;
}

void PlayerManager::ResetTimePool()
{
	curTurnTime = baseTurnTime;
	speed = 1.0f;
}

void PlayerManager::AddCrit(float in_crit)
{
	activeStatusEffects[StatusEffects::CritChance].first += in_crit;
}

void PlayerManager::AddDodge(float in_dodge)
{
	activeStatusEffects[StatusEffects::Dodge].first = std::min(0.95f, activeStatusEffects[StatusEffects::Dodge].first + in_dodge);
}

void PlayerManager::AddPassiveCrit(float in_crit)
{
	baseCrit += in_crit;
}

void PlayerManager::AddPassiveDodge(float in_dodge)
{
	baseDodge += in_dodge;
}

void PlayerManager::DumpTemporaryStatus()
{
	block /= 4u;
	activeStatusEffects.at(StatusEffects::Dodge).first = baseDodge;
	activeStatusEffects.at(StatusEffects::CritChance).first = baseCrit;
}