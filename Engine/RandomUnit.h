#pragma once
#include <random>
#define RU RandomUnit::GetRandomUnit()
class RandomUnit
{
private:
	RandomUnit():rng(rd()){};
public:
	static RandomUnit &GetRandomUnit() { static RandomUnit unit; return unit; }			//thanks Obama										
	size_t GetUInt(size_t low, size_t high);
	float GetFloat(float low, float high);
	float GetFloat0to1();
	bool GetBool();
private:
	std::random_device rd;
	std::mt19937 rng;
};