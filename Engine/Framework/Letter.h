#pragma once
#include "Drawable.h"
#include "Bindable.h"
#include "IndexBuffer.h"
#include "ConstantBuffers.h"
#include "Colors.h"
#include "FrameCommander.h"
class Letter : public Drawable		
{
public:
	Letter(std::wstring fontSprite, DXGraphics& gfx, wchar_t letter);
	Letter() = default;						
	Letter(Letter&&);
	DirectX::XMMATRIX GetTransformXM() const noexcept override;
	void BindLetterData(std::vector<LetterData>* posData);
	void SmartSubmit(FrameCommander& fc);		
	virtual ~Letter() = default;				
private:
	std::vector<LetterData>* letData;			//one letter - one position
	mutable size_t currentObjectBeingDrawn = 0;
};

