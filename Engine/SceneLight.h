#pragma once
#include "Framework\PointLight.h"
#include "Framework\Vec3.h"
#include "Framework\PlaneTexNorm.h"
#include <map>
#include "Framework\FrameCommander.h"
#include <xmmintrin.h>

#define SL SceneLight
class SceneLight					//handles logic of scene lights
{
public:
	enum class LightDistortionType
	{
		DarkeningFlicker,
		RednessBoost,
		ColdEmbrace,
		YellowSpark,
		GreenWave,
		ExtraLongAndDark,
		NoDistortion
	};
	SceneLight(DXGraphics& gfx);

	void SubmitLightHolders(FrameCommander& fc);

	void UpdateAndBind(float dt, DXGraphics& gfx, DirectX::XMMATRIX cameraTransform);			//must be called before FrameCommander begins processing the frame
	static void PendDistortion(LightDistortionType pendDist)
	{
		pendingDistortion = pendDist;
	}
private:
	const __m128 posLight0[NUM_OF_LIGHTS]{
		{ 3.0f, 1.52f, -0.82f,0.0f },
		{ 3.0f, 1.52f, 2.82f, 0.0f },
		{ 6.5f, 1.52f, 2.82f, 0.0f },
		{ 6.5f, 1.52f, -0.82f,0.0f } };//positions of light holders

	__m128 posLightBuffer[NUM_OF_LIGHTS]			{ posLight0[0],posLight0[1],posLight0[2],posLight0[3] };
	__m128 basePosLightBuffer[NUM_OF_LIGHTS]		{ posLight0[0],posLight0[1],posLight0[2],posLight0[3] };
	__m128 initialPosLightBuffer[NUM_OF_LIGHTS]		{ posLight0[0],posLight0[1],posLight0[2],posLight0[3] };
	__m128 flickerEndPosLightBuffer[NUM_OF_LIGHTS]	{ posLight0[0],posLight0[1],posLight0[2],posLight0[3] };

	const __m128 yellow	{ 0.85, 0.4352f, 0.105f, 0.0f };		//these colors are used to interpolate light color 
	const __m128 red	{ 1.0f, 0.4352f, 0.105f, 0.0f };

	const __m128 ambient{ 0.0f,	0.0f, 0.0f,	0.0f };
	float intencity = 40.3f;
	const Vec4 intensityParam{ intencity, 1.6f, 3.0f , 10.0f };         //first float - intencity, second - constanf attenuation, third - linear, fourth - quadratic

	float baseIntBuffer[NUM_OF_LIGHTS]{ intencity, intencity, intencity, intencity };
	float initialIntBuffer[NUM_OF_LIGHTS]{ intencity, intencity, intencity, intencity };
	float endIntBuffer[NUM_OF_LIGHTS]{ intencity, intencity, intencity, intencity };

	__m128 baseDifBuffer[NUM_OF_LIGHTS]{ red, red, red, red };
	__m128 initDifBuffer[NUM_OF_LIGHTS]{ red, red, red, red };
	__m128 endDifBuffer[NUM_OF_LIGHTS]{ red, red, red, red };

	PointLightCBuf dataLightBuffer{
		ambient,					ambient,					ambient,					ambient,
		red,						red,						red,						red,
		intensityParam,				intensityParam,				intensityParam,				intensityParam
	};

	static constexpr float minFlickerTime = 0.2f;
	static constexpr float maxFlickerTime = 0.6f;
	static constexpr float positionFlickerAmount = 0.03f;		
	static constexpr float intencityFlickerAmount = 5.1f;			

	PointLight pl;
	float currentFlickerTime[NUM_OF_LIGHTS]	{0.0f,0.0f,0.0f,0.0f};
	float totalFlickerTime[NUM_OF_LIGHTS]	{ 0.0f,0.0f,0.0f,0.0f };

	std::unique_ptr<PlaneTexNorm> lightHolder;
	std::vector<PositionData> lightHolderPositionData;

	//distortion stuff
	static constexpr float distortionTime = 1.5f;

	static constexpr float darkeningMod = 3.2f;

	static constexpr float redBoostMod = 3.0f;
	static constexpr float redColortMod = 1.4f;

	static constexpr float greenBoostMode = 1.5f;

	static constexpr float yellowBoostMode = 1.5f;

	static constexpr float coldMoveMod = 0.0f;
	static constexpr float coldColortModR = 0.9f;
	static constexpr float coldColortModG = 0.95f;
	static constexpr float coldColortModB = 1.7f;
	static constexpr float coldBrightMod = 1.1f;

	static constexpr float extradarkProlongation = 2.0f;
	static constexpr float extradarkPower = 0.2f;

	float currentDistortedTime = 0.0f;
	LightDistortionType currentDistortion = LightDistortionType::NoDistortion;
	static LightDistortionType pendingDistortion;
};