//coded by Chili
#include "IndexBuffer.h"
#include "GraphicsThrowMacros.h"
#include "BindableCodex.h"
#include "ChiliUtil.h"

namespace Bind
{
	IndexBuffer::IndexBuffer(DXGraphics& gfx,const std::vector<unsigned short>& indices )
		:
		IndexBuffer( gfx,L"?",indices )
	{}
	IndexBuffer::IndexBuffer( DXGraphics& gfx,std::wstring tag,const std::vector<unsigned short>& indices )
		:
		tag( tag ),
		count( (UINT)indices.size() )
	{
		INFOMAN( gfx );

		D3D11_BUFFER_DESC ibd = {};
		ibd.BindFlags = D3D11_BIND_INDEX_BUFFER;
		ibd.Usage = D3D11_USAGE_DEFAULT;
		ibd.CPUAccessFlags = 0u;
		ibd.MiscFlags = 0u;
		ibd.ByteWidth = UINT( count * sizeof( unsigned short ) );
		ibd.StructureByteStride = sizeof( unsigned short );
		D3D11_SUBRESOURCE_DATA isd = {};
		isd.pSysMem = indices.data();
		GFX_THROW_INFO( GetDevice( gfx )->CreateBuffer( &ibd,&isd,&pIndexBuffer ) );
	}

	void IndexBuffer::Bind( DXGraphics& gfx ) noexcept
	{
		GetContext( gfx )->IASetIndexBuffer( pIndexBuffer.Get(),DXGI_FORMAT_R16_UINT,0u );
	}

	UINT IndexBuffer::GetCount() const noexcept
	{
		return count;
	}
	std::shared_ptr<IndexBuffer> IndexBuffer::Resolve( DXGraphics& gfx,const std::wstring& tag,
			const std::vector<unsigned short>& indices )
	{
		assert( tag != L"?" );
		return Codex::Resolve<IndexBuffer>( gfx,tag,indices );
	}
	std::wstring IndexBuffer::GenerateUID_( const std::wstring& tag )
	{
		return ToWide(typeid(IndexBuffer).name()) + L"#" + tag;
	}
	std::wstring IndexBuffer::GetUID() const noexcept
	{
		return GenerateUID_( tag );
	}
}
