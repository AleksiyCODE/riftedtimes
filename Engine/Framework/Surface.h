//coded by Chili
#pragma once
#include "ChiliWin.h"
#include "Colors.h"
#include "Rect.h"
#include "ChiliException.h"
#include <string>
#include <dxtex/DirectXTex.h>
#include <optional>
class Surface
{
public:
	class Exception : public ChiliException
	{
	public:
		Exception(int line, const wchar_t* file, std::wstring note) noexcept;
		Exception(int line, const wchar_t* file, std::wstring filename, std::wstring note, std::optional<HRESULT> hr = {}) noexcept;
		const wchar_t* MyWhat() const noexcept override;
		const wchar_t* GetType() const noexcept override;
		const std::wstring& GetNote() const noexcept;
	private:
		std::wstring note;
	};
public:
	Surface(unsigned int width, unsigned int height);
	Surface(const std::wstring& filename);
	Surface(Surface&& donor) = default;
	Surface(const Surface&) = delete;
	Surface& operator=(const Surface&) = delete;
	Surface& operator=(Surface&& donor) noexcept = default;
	//Surface() = default;	//is needed for creating dummy surfaces !in OLD graphics only?! (might be bad design)
	~Surface() = default;
	Color* GetBufferPtr() noexcept;
	const Color* GetBufferPtr() const noexcept;
	const Color* GetBufferPtrConst() const noexcept;
	void PutPixel(int x, int y, Color c);
	void PutPixelAlpha( int x,int y,Color c );
	Color GetPixel(int x, int y) const;
	int GetWidth() const noexcept;
	int GetHeight() const noexcept;
	RectI GetRect() const noexcept;
	bool AlphaLoaded() const noexcept;
	Surface(DirectX::ScratchImage scratch) noexcept;
private:
	static unsigned int GetPitch( unsigned int width,unsigned int byteAlignment )
	{
		assert( byteAlignment % 4 == 0 );
		const unsigned int pixelAlignment = byteAlignment / sizeof( Color );
		return width + ( pixelAlignment - width % pixelAlignment ) % pixelAlignment;
	}
private:
	static constexpr DXGI_FORMAT format = DXGI_FORMAT::DXGI_FORMAT_B8G8R8A8_UNORM;
	DirectX::ScratchImage scratch;
};