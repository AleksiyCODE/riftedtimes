#include "Game.h"
#include "imgui/imgui.h"
#include "imgui/imgui_impl_win32.h"
#include "imgui/imgui_impl_dx11.h"

Game::Game():
	wnd(L"Rifted Times"),
	ll(wnd.DXGfx(),fc,cm),
	td(wnd.DXGfx(),fc),
	hud(menu,fc, wnd.DXGfx(),cm.GetPlayerManager().GetActionBox(),cm),
	menu(cm),
	eh(wnd.DXGfx()),
	fc(wnd.DXGfx())
{
	am.Init();
	wnd.DXGfx().SetProjection(DirectX::XMMatrixPerspectiveLH(1.0f, 9.0f / 16.0f, 0.5f, 400.0f));
}

int Game::Go()
{
	while(true)
	{
		if (isExiting)break;
		while (const auto e = wnd.kbd.ReadKey())
		{
			if (!e->IsPress())			//no need to handle non-press events
			{
				continue;
			}
			cm.ProcessInput(e.value());
			switch (e->GetCode())
			{
			case VK_ESCAPE:
				if (wnd.CursorEnabled())
				{
					wnd.DisableCursor();
					wnd.mouse.EnableRaw();
				}
				else
				{
					wnd.EnableCursor();
					wnd.mouse.DisableRaw();
				}
				break;
			case VK_F1:
				showDemoWindow = true;
				break;
			}
			if (menu.Update(e.value())) isExiting = true;		//must go after CombatManager, exits the application
		}
		// process all messages pending, but to not block for new messages
		if (const auto ecode = MainWindow::ProcessMessages())
		{
			// if return optional has value, means we're quitting so return exit code
			return *ecode;
		}
		const auto dt = timer.Mark() * speed_factor;
		wnd.DXGfx().BeginFrame(0.0f, 0.0f, 0.0f);
		wnd.DXGfx().SetCamera(ll.GetCameraMatrix());
		ll.UpdateAndBindLight(dt, ll.GetCameraMatrix());
		wnd.DXGfx().SetCamPos(ll.GetCameraPosition());	
		bm.UpdateBlur(dt);
		hud.Submit();
		ll.SubmitDrawables();
		cm.Draw(wnd.DXGfx());
		eh.SubmitAndUpdate(fc, dt);
		td.SubmitText(dt);
		fc.Execute(wnd.DXGfx(), dt);
		wnd.DXGfx().EndFrame();
		td.EndFrame();
		fc.Reset();
		cm.UpdateCombatState(dt);
		am.Update();
		hud.Animate(dt);
	}
	return(-1);					//for compiler not to worry
}