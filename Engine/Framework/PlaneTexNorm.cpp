#include "PlaneTexNorm.h"
#include "BindableCommon.h"
#include "../imgui/imgui.h"
#include "Vertex.h"

PlaneTexNorm::PlaneTexNorm(std::wstring modelName, DXGraphics& gfx, float scale, float roll, float pitch, float yaw)
{
	//getting main drawable data
	using namespace Bind;
	namespace dx = DirectX;
	using Dvtx::VertexLayout;
	using namespace std::string_literals;
	Assimp::Importer imp;
	std::string sName = ToNarrow(modelName);
	std::string& pathString = sName;
	const auto pPlane = imp.ReadFile(pathString.c_str(),
		aiProcess_Triangulate |
		aiProcess_JoinIdenticalVertices |
		aiProcess_ConvertToLeftHanded |
		aiProcess_GenNormals |
		aiProcess_CalcTangentSpace
	);
	if (pPlane == nullptr)
	{
		auto s = imp.GetErrorString();	// ModelException not implemented
		assert(false && "bad model");	//throw ModelException(__LINE__, __FILE__, imp.GetErrorString());
	}
	const auto rootPath = std::filesystem::path(modelName).parent_path().wstring() + L"\\";
	Technique standard;
	Technique CamFacing;
	{
		bool hasSpecularMap = false;
		bool hasAlphaDiffuse = false;
		bool hasNormalMap = false;
		bool hasDiffuseMap = false;
		float shininess = 20.0f;
		float aspectRatio = 1.0f;
		dx::XMFLOAT3 specularColor = { 0.08f,0.08f,0.08f };
		dx::XMFLOAT3 diffuseColor = { 0.45f,0.45f,0.85f };
		auto mesh = pPlane->mMeshes[0];		//shortcut
		Step only(0);
		if (mesh->mMaterialIndex >= 0)
		{
			auto& material = *pPlane->mMaterials[mesh->mMaterialIndex];
			aiString texFileName;
			if (material.GetTexture(aiTextureType_DIFFUSE, 0, &texFileName) == aiReturn_SUCCESS)
			{
				auto tex = Texture::Resolve(gfx, rootPath + ToWide(texFileName.C_Str()));
				hasAlphaDiffuse = tex->HasAlpha();
				aspectRatio = (float)tex->GetWidth() / (float)tex->GetHeight();
				only.AddBindable(std::move(tex));
				hasDiffuseMap = true;
				material.Get(AI_MATKEY_COLOR_DIFFUSE, reinterpret_cast<aiColor3D&>(diffuseColor));
			}
			//not checking for specular map (there should be none)
			material.Get(AI_MATKEY_COLOR_SPECULAR, reinterpret_cast<aiColor3D&>(specularColor));
			material.Get(AI_MATKEY_SHININESS, shininess);

			if (material.GetTexture(aiTextureType_NORMALS, 0, &texFileName) == aiReturn_SUCCESS)
			{
				auto tex = Texture::Resolve(gfx, rootPath + ToWide(texFileName.C_Str()), 2);	//pixel shader expects it in slot 2
				only.AddBindable(std::move(tex));
				hasNormalMap = true;
			}
			only.AddBindable(Bind::Sampler::Resolve(gfx));				//for filtering
		}
		assert(hasDiffuseMap && "model is missing diffuse map");//supposed to have it (just to be sure)		
		Dvtx::VertexBuffer vbuf(std::move(
			VertexLayout{}
			.Append(VertexLayout::Position3D)
			.Append(VertexLayout::Normal)
			.Append(VertexLayout::Tangent)
			.Append(VertexLayout::Bitangent)
			.Append(VertexLayout::Texture2D)
		));
		for (unsigned int i = 0; i < mesh->mNumVertices; i++)
		{
			vbuf.EmplaceBack(
				dx::XMFLOAT3(mesh->mVertices[i].x * scale, mesh->mVertices[i].y * scale, mesh->mVertices[i].z * scale * aspectRatio),
				*reinterpret_cast<dx::XMFLOAT3*>(&mesh->mNormals[i]),
				*reinterpret_cast<dx::XMFLOAT3*>(&mesh->mTangents[i]),
				*reinterpret_cast<dx::XMFLOAT3*>(&mesh->mBitangents[i]),
				*reinterpret_cast<dx::XMFLOAT2*>(&mesh->mTextureCoords[0][i])
			);
		}
		std::vector<unsigned short> indices;
		indices.reserve(mesh->mNumFaces * 3);
		for (unsigned int i = 0; i < mesh->mNumFaces; i++)
		{
			const auto& face = mesh->mFaces[i];
			assert(face.mNumIndices == 3);
			indices.push_back(face.mIndices[0]);
			indices.push_back(face.mIndices[1]);
			indices.push_back(face.mIndices[2]);
		}
		only.AddBindable((PixelShader::Resolve(gfx, hasAlphaDiffuse ? L"Shaders\\PhongPSNormMask.cso" : L"Shaders\\PhongPSNormalMap.cso")));
		auto pvs = VertexShader::Resolve(gfx, L"Shaders\\PhongVSNormalMap.cso");
		auto pvsbc = pvs->GetBytecode();
		only.AddBindable(InputLayout::Resolve(gfx, vbuf.GetLayout(), pvsbc));
		only.AddBindable(std::move(pvs));
		Dcb::RawLayout layout;
		layout.Add<Dcb::Float3>(L"texSpecCol");
		layout.Add<Dcb::Float>(L"specularPower");
		layout.Add<Dcb::Float3>(L"texDifColInt");
		layout.Add<Dcb::Bool>(L"normalMapEnabled");

		auto cbuf = Dcb::Buffer(std::move(layout));
		cbuf[L"texSpecCol"] = specularColor;
		cbuf[L"specularPower"] = shininess;
		cbuf[L"texDifColInt"] = diffuseColor;
		cbuf[L"normalMapEnabled"] = hasNormalMap;

		only.AddBindable(std::make_shared<CachingPixelConstantBufferEX>(gfx, cbuf, 1u));
		only.AddBindable(std::make_shared<TransformCbuf>(gfx, TransformCbuf::DataType::Standart));
		standard.AddStep(only);			//is not moved to preserve the object
		only.PopBindable();				//replacing TransformCbuf with the new one for CamFacing technique
		only.AddBindable(std::make_shared<TransformCbuf>(gfx, TransformCbuf::DataType::CamFacing));
		CamFacing.AddStep(std::move(only));

		//these two should be bound outside steps because they stay the same no matter the Technique
		const auto meshTag = std::filesystem::path(modelName).wstring() + L"%" + ToWide(mesh->mName.C_Str());
		pVertices = VertexBuffer::Resolve(gfx, meshTag, vbuf);
		pIndices = IndexBuffer::Resolve(gfx, meshTag, indices);
	}
	AddTechnique(std::move(standard));
	Technique ConsecutiveStandart;		//use this only in case where you are drawing the same drawable multiple times in one frame
										//it greatly decreaces the abount of binding done by the pipeline
	{
		Step only(0);
		only.AddBindable(std::make_shared<TransformCbuf>(gfx, TransformCbuf::DataType::Standart));
		ConsecutiveStandart.AddStep(std::move(only));
	}
	AddTechnique(std::move(ConsecutiveStandart));

	AddTechnique(std::move(CamFacing));
	Technique ConsecutiveCamFacing;		//the order of AddTechnique calls matters because for the lookup to succeed 
										//it has to match the order in witch they are specified in Drawable base class
	{
		Step only(0);
		only.AddBindable(std::make_shared<TransformCbuf>(gfx, TransformCbuf::DataType::CamFacing));
		ConsecutiveCamFacing.AddStep(std::move(only));
	}
	AddTechnique(std::move(ConsecutiveCamFacing));
}

PlaneTexNorm::PlaneTexNorm(PlaneTexNorm && mvFrom)
{
	posData = mvFrom.posData;
	currentObjectBeingDrawn = mvFrom.currentObjectBeingDrawn;
	pIndices = mvFrom.pIndices;
	pVertices = mvFrom.pVertices;
	techniques = std::move(mvFrom.techniques);
	this->RebindParent();
}



DirectX::XMMATRIX PlaneTexNorm::GetTransformXM() const noexcept
{
	assert(posData->size() && "no positions bound for this object - can't draw");
	currentObjectBeingDrawn ++;
	if (currentObjectBeingDrawn == posData->size())currentObjectBeingDrawn = 0;	
	DirectX::XMMATRIX transMat =
		DirectX::XMMatrixRotationRollPitchYaw(
			(*posData)[currentObjectBeingDrawn].rot.x,
			(*posData)[currentObjectBeingDrawn].rot.y,
			(*posData)[currentObjectBeingDrawn].rot.z) *			
		DirectX::XMMatrixTranslation(
			(*posData)[currentObjectBeingDrawn].pos.x, 
			(*posData)[currentObjectBeingDrawn].pos.y,
			(*posData)[currentObjectBeingDrawn].pos.z );
	return transMat;
}

void PlaneTexNorm::BindPositionData(std::vector<PositionData>* posData)
{
	this->posData = posData;
}

void PlaneTexNorm::SmartSubmit(FrameCommander& fc, Techniques mainTec)
{
	this->Submit(fc,mainTec);
	switch (mainTec)
	{
	case Techniques::Standart:
		for (size_t i = 1; i < posData->size(); i++)this->Submit(fc, Techniques::ConsecutiveStandart);
		break;
	case Techniques::ConsecutiveStandart:
		assert(false && "ConsecutiveStandart can't be main technique");
		break;
	case Techniques::CamFacing:
		for (size_t i = 1; i < posData->size(); i++)this->Submit(fc, Techniques::ConsecutiveCamFacing);
		break;
	case Techniques::ConsecutiveCamFacing:
		assert(false && "ConsecutiveCamFacing can't be main technique");
		break;
	default:
		assert(false && "programmer missed the case in SmartSubmit in PlaneTexNorm");
		break;
	}
}