#include "Ghoul.h"
#include "RandomUnit.h"
#include "./SkillStuff/Skill.h"
#include "SoundPlayerInterface.h"
#include <assert.h>
#include "AchievementManager.h"
#include "SceneLight.h"
#include "CameraManager.h"
#include "BlurManager.h"
#include "EffectHandler.h"

class Ghoul_Bite : public Skill
{
public:
	Ghoul_Bite(CombatEntity* owner) : Skill(owner)
	{
		id = 1001;
		valLow = 5.0f;
		valHigh = 7.0f;
		succChance = 0.0f;
		type = AbilityType::Attack;
	};
	void Activate()
	{
		switch (RU.GetUInt(0u,2u)	)
		{
		case 0:		
			SPI::Play(Sounds::Ghoul_attack_01, 1.0f);
			break;
		case 1:
			SPI::Play(Sounds::Ghoul_attack_02, 1.0f);
			break;
		case 2:
			SPI::Play(Sounds::Ghoul_attack_03, 1.0f);
			break;
		default:
			break;
		}
		owner->AddEvent(SkillEvent(EventType::Player_PhysicalAttackedWithBleed, valLow, valHigh));
		dynamic_cast<EnemyCombat*>(owner)->SetState(EnemyCombat::State::GhoulAttack);			//only enemy can have these skills
		SceneLight::PendDistortion(SceneLight::LightDistortionType::DarkeningFlicker);
	}
};
class Ghoul_Enforce : public Skill
{
public:
	Ghoul_Enforce(CombatEntity* owner) : Skill(owner)
	{
		id = 1003;
		valLow = 15.0f;
		valHigh = 17.0f;
		succChance = 0.0f;
		type = AbilityType::Block;
	};
	void Activate()
	{
		SPI::Play(Sounds::Ghoul_Enforce, 1.0f);
		owner->AddEvent(SkillEvent(EventType::Enemy_AddBlock, valLow, valHigh));
		dynamic_cast<EnemyCombat*>(owner)->SetState(EnemyCombat::State::GhoulCovering);
		SL::PendDistortion(SL::LightDistortionType::ColdEmbrace);
	}
};

class Ghoul_Enrage : public Skill
{
public:
	Ghoul_Enrage(CombatEntity* owner) : Skill(owner)
	{
		id = 1002;
		valLow = valHigh = 1.25f;
		type = AbilityType::Buff;
	};
	void Activate()
	{
		SPI::Play(Sounds::Ghoul_Enrage, 1.0f);
		owner->AddEvent(SkillEvent(EventType::Enemy_AddSpeed, valLow, valHigh));
		dynamic_cast<EnemyCombat*>(owner)->SetState(EnemyCombat::State::GhoulRage);
		SL::PendDistortion(SL::LightDistortionType::RednessBoost);
		CM::PendScenario(CM::Scenario::GhoulRage);
		EH::AddEffect3D(EH::Effect::GhoulHowl);
		BlurManager::AddBlur(1.4f);
	}
};
class Ghoul_Tear : public Skill
{
public:
	Ghoul_Tear(CombatEntity* owner) : Skill(owner)
	{
		id = 1000;
		valLow = 11.0f;
		valHigh = 32.0f;
		succChance = 0.0f;
		type = AbilityType::Attack;
	};
	void Activate()
	{
		switch (RU.GetUInt(0u, 2u))
		{
		case 0:
			SPI::Play(Sounds::Ghoul_attack_01, 1.0f);
			break;
		case 1:
			SPI::Play(Sounds::Ghoul_attack_02, 1.0f);
			break;
		case 2:
			SPI::Play(Sounds::Ghoul_attack_03, 1.0f);
			break;
		default:
			break;
		}
		owner->AddEvent(SkillEvent(EventType::Player_PhysicalDamage, valLow, valHigh));
		dynamic_cast<EnemyCombat*>(owner)->SetState(EnemyCombat::State::GhoulAttack);
		SceneLight::PendDistortion(SceneLight::LightDistortionType::DarkeningFlicker);
	}
};

Ghoul::Ghoul()
{
	HP = 75u;
	maxHP = 75u;
	speed = 1.0f;
	block = 0u;
	abilities.emplace_back(std::make_unique<Ghoul_Bite>		(this), 0.35f, 0.35f );
	abilities.emplace_back(std::make_unique<Ghoul_Tear>		(this), 0.30f, 0.30f);
	abilities.emplace_back(std::make_unique<Ghoul_Enrage>	(this), 0.15f, 0.15f);
	abilities.emplace_back(std::make_unique<Ghoul_Enforce>	(this), 0.20f, 0.20f);
}

void Ghoul::Act()
{
	std::get<0>(abilities[chosenAbility])->Activate();

	if (chosenAbility == 2)		//if Enrage
	{
		std::get<2>(abilities[1]) += 0.9f;	//manipulating AI
		std::get<2>(abilities[2]) = 0.0f;
		std::get<2>(abilities[3]) = 0.0f;
	}
	else
	{
		std::get<2>(abilities[0]) = std::get<1>(abilities[0]);
		std::get<2>(abilities[1]) = std::get<1>(abilities[1]);
		std::get<2>(abilities[2]) = std::get<1>(abilities[2]);
		std::get<2>(abilities[3]) = std::get<1>(abilities[3]);
	}
}

AbilityType Ghoul::Choose(bool justReset)
{
	if (!justReset)
	{
		float chanceSumm = 0.0f;
		for (auto& entry : abilities)
		{
			chanceSumm += std::get<2>(entry);
		}
		float roll = RU.GetFloat(0.0f, chanceSumm);
		for (int i = 0; ; i++)
		{
			roll -= std::get<2>(abilities[i]);
			if (roll <= 0)
			{
				chosenAbility = i;
				auto type = std::get<0>(abilities[i])->GetType();
				switch (type)
				{
				case AbilityType::NoType:
					SetState(EnemyCombat::State::GhoulIdle);
					break;
				case AbilityType::Attack:
					SetState(EnemyCombat::State::GhoulPreparingAttack);
					break;
				case AbilityType::Buff:
					SetState(EnemyCombat::State::GhoulPreparingRage);
					break;
				case AbilityType::Block:
					AM::Alert(AM::Event::GhoulChoseToBlock);
					SetState(EnemyCombat::State::GhoulPreparingCover);
					break;
				default:
					assert(false && "missed ability type in Ghoul::Choose");
					break;
				}
				return type;
			}
		}
	}
	else
	{
		SetState(EnemyCombat::State::GhoulIdle);
		return AbilityType::NoType;
	}
}
