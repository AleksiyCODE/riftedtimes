#pragma once
#include <map>
#include<vector>

#define AM AchievementManager
#define NUM_ACHIEVEMENTS 10

class AchievementModul
{
public:
	enum class Achievement
	{
		Untouchable = '0',			//dont lose hp 5 turns
		LockedAndLoaded,			//upgrade all skills
		SnakeEyes,					//fail to dodge after using SandStorm and DustThrow
		EatMyDust,					//kill a ghoul with a DustThrow
		Quickdraw,					//use 6 skills in a single turn
		Copycat,					//use Block when Ghoul is about to Block
		Bleedout,					//die to bloodloss
		HeavyBlow,					//kill with a crit
		FirstAid,					//heal 20 bleed with a single bandage
		Slayer,						//kill 8 ghouls in a run
		Last
	};
	const std::map<Achievement, bool>& GetAchievements()const;
private:
	friend class AchievementManager;
	void LoadAchievements();
	void DumpToFile()const;
	void AchUnlocked(Achievement a);
	std::map<Achievement, bool> achievements;
};


class AchievementManager 
{
public:
	enum class Event
	{
		PlayerUpgradedAllSkills,
		PlayerUsedSandStorm,
		PLayerUsedDustThrow,
		PlayerUsedSwordStrike,
		PlayerLostHP,
		PlayerTurnBegan,
		PlayerTurnEnded,
		EnemyDied,
		PlayerUsed6Skills,
		PlayerCrited,
		PlayerUsedBlock,
		GhoulChoseToBlock,
		PlayerDiedToBloodloss,
		PlayerHealed20Bleed,
		PlayerHasBlock,
		TurnBegin,
		TurnEnd,
		PlayerGotHit,
		PlayerKilled8Enemies
	};

	void Init();		//is called from Game
	void Update();		//is called from Game
	static void Alert(Event e, float emount = 0.0f);
	static void ResetState();			//is called when a combat ends
	static const  AchievementModul& GetAchievementModul();			//is needed for drawing achievements
private:
	static bool isPlayerTurn;
	static size_t turnsWithoutDamage;
	static bool playerTookDamageThisTurn;
	static bool playerWasHitThisTurn;
	static bool sandStormWasUsed;
	static size_t turnsUsedSandStormAndWasHit;
	static bool dustThrowWasUsed;
	static bool enemyDiedThisTurn;
	static bool playerUsedSwordStrike;
	static bool enemyDecidedToBlock;
	static bool playerUsedBlock;
	static bool playerCrited;
	static std::vector<AchievementModul::Achievement> unlocksThisFrame;
	static class AchievementModul modul;
};


