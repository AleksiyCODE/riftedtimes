#pragma once
#include "Framework\HudElement.h"

class InitiativeBar : public HudElement
{
public:
	InitiativeBar(DXGraphics& gfx, const struct CombatData& cd);
	void UpdateFillness(DXGraphics& gfx);
	void SmartSubmit(FrameCommander& fc) const override;
	void Animate(float dt) override;
private:
	std::vector<PositionData> refPosData;
	struct HPBarCBuff
	{
		float relation;				//player speed devided by enemy speed
		float pad1;
		float pad2;
		float pad3;
	};
	const struct CombatData& cd;
	HPBarCBuff cbData;
	HudElement initBarHolder;
	HudElement burn;	
	mutable float prevRelation = 1.0f;
	std::vector<PositionData> holderPosData;
	std::shared_ptr <Bind::PixelConstantBuffer<HPBarCBuff>> cbuf;

	std::vector<PositionData> burnPosData;
	static constexpr float burnDefaultScale = 0.07f;
	static constexpr float burnFlickerScale = 0.2f;	
};