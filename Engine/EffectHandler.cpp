#include "EffectHandler.h"

struct EffectCBuff
{
	float val;
	float pad1;
	float pad2;
	float pad3;
};

Effect2DData Effect2DDataInterpolate(Effect2DData e1, Effect2DData e2, float progress)
{
	return Effect2DData(e1*(1.0f-progress)+e2*progress);
}

Effect3DData Effect3DDataInterpolate(Effect3DData e1, Effect3DData e2, float progress)
{
	return Effect3DData(e1 * (1.0f - progress) + e2 * progress);
}


HudEffect<Effect2DSequence,HudElement>::HudEffect(DXGraphics& gfx, Effect2DSequence data, std::wstring spriteName):
	HudElement(spriteName, gfx),
	effectSequence(data),
	gfx(gfx)
{
	using namespace Bind;
	auto& s = GetTechniquesReference()[0].GetSteps()[0];
	s.DeleteBindableByUID(L"class Bind::PixelShader#Shaders\\HudPS.cso");
	s.AddBindable(PixelShader::Resolve(gfx, L"Shaders\\HUDEffectPS.cso"));
	auto pcb = std::make_shared<PixelConstantBuffer<EffectCBuff>>(gfx, 3);
	cbuf = pcb;
	s.AddBindable(pcb);	
	BindPositionData(&refPosData);
	refPosData.emplace_back();
}


HudEffect<Effect3DSequence, PlaneTexNorm>::HudEffect(DXGraphics& gfx, Effect3DSequence data, std::wstring spriteName) :
	PlaneTexNorm(spriteName, gfx),effectSequence(data), gfx(gfx)
{
	auto& s = GetTechniquesReference()[2].GetSteps()[0];

	s.DeleteBindableByUID(L"class Bind::VertexShader#Shaders\\PhongVSNormalMap.cso");
	s.AddBindable(Bind::VertexShader::Resolve(gfx, L"Shaders\\PhongVSScale.cso"));

	s.DeleteBindableByUID(L"class Bind::PixelShader#Shaders\\PhongPSNormMask.cso");
	s.AddBindable(Bind::PixelShader::Resolve(gfx, L"Shaders\\PhongPSOpac.cso"));

	scaleCbuf = std::make_shared<Bind::VertexConstantBuffer<EffectCBuff>>(gfx, 3);
	cbuf = std::make_shared<Bind::PixelConstantBuffer<EffectCBuff>>(gfx, 4);			//opacity buffer
	s.AddBindable(cbuf);
	s.AddBindable(scaleCbuf);
	s.AddBindable(Bind::Blender::Resolve(gfx, true));		//alpha usage set to true by default

	s.SetTargetPass(1u);				//so that they are drawn after models without alpha

	BindPositionData(&refPosData);
	refPosData.emplace_back();
}

template<typename T, typename Base>
HudEffect<T,Base>::HudEffect(HudEffect&& mvFrom):
HudElement(std::move(mvFrom)),
cbData0			 (std::move(mvFrom.cbData0)),
refPosData		 (std::move(mvFrom.refPosData)),
cbuf			 (std::move(mvFrom.cbuf	)),
effectSequence	 (std::move(mvFrom.effectSequence)),
gfx				 (std::move(mvFrom.gfx)){}


HudEffect<Effect3DSequence, PlaneTexNorm>::HudEffect(HudEffect&& mvFrom) :
	PlaneTexNorm(std::move(mvFrom)),
	cbData0(std::move(mvFrom.cbData0)),
	cbData1(std::move(mvFrom.cbData1)),
	refPosData(std::move(mvFrom.refPosData)),
	cbuf(std::move(mvFrom.cbuf)),
	effectSequence(std::move(mvFrom.effectSequence)),
	gfx(std::move(mvFrom.gfx)) {}



bool HudEffect<Effect2DSequence, HudElement>::UpdateData(float dt)
{
	auto nd = effectSequence.Update(dt);
	if (nd.has_value())
	{
		refPosData[0].pos.x = nd->posX;
		refPosData[0].pos.y = nd->posY;
		refPosData[0].pos.z = nd->scale;
		refPosData[0].rot.x = nd->rotation;
		cbData0.val = nd->opacity;
		cbuf->Update(gfx, cbData0);
		return 0;
	}
	else return 1;
}

bool HudEffect<Effect3DSequence, PlaneTexNorm>::UpdateData(float dt)
{
	auto nd = effectSequence.Update(dt);
	if (nd.has_value())
	{
		refPosData[0].pos.x = nd->posX;
		refPosData[0].pos.y = nd->posY;
		refPosData[0].pos.z = nd->posZ;
		refPosData[0].rot.x = nd->roll;
		refPosData[0].rot.y = nd->pitch;
		refPosData[0].rot.z = nd->yaw;
		cbData0.val = nd->scale;
		cbData1.val = nd->opacity;
		scaleCbuf->Update(gfx, cbData0);
		cbuf->Update(gfx, cbData1);
		return 0;
	}
	else return 1;
}

EffectHandler::EffectHandler(DXGraphics& gfx)
{
	EffectHandler::gfx = &gfx;
}

void EffectHandler::AddEffect2D(Effect2DSequence data, std::wstring spriteName)
{
	e2d.push_back(std::make_unique<HudEffect<Effect2DSequence, HudElement>>(*gfx, data, spriteName));
}

void EffectHandler::AddEffect2D(PositionData posData,Effect e)
{
	switch (e)
	{
	case Effect::Shine:						//can be called by different entities
	{
		Effect2DSequence e2ds;
		e2ds.AddStage({ posData.pos.x,posData.pos.y,0.0f,0.0f }, 0.1f);
		e2ds.AddStage({ posData.pos.x,posData.pos.y,posData.pos.z,0.9f }, 0.3f);
		e2ds.AddStage({ posData.pos.x,posData.pos.y,posData.pos.z,0.0f }, 0.0f);
		EffectHandler::AddEffect2D(e2ds, L"Media\\Sprites\\effect_shine.png");
		break;
	}
	case Effect::ShineOrange:				//can be called by different entities	
	{
		Effect2DSequence e2ds;
		e2ds.AddStage({ posData.pos.x,posData.pos.y,posData.pos.z*0.5f,	 0.4f }, 0.1f);
		e2ds.AddStage({ posData.pos.x,posData.pos.y,posData.pos.z,		 0.9f }, 0.2f);
		e2ds.AddStage({ posData.pos.x,posData.pos.y,posData.pos.z*2.0f,	 0.4f }, 0.3f);
		e2ds.AddStage({ posData.pos.x,posData.pos.y,posData.pos.z * 4.0f,0.0f }, 0.0f);
		EffectHandler::AddEffect2D(e2ds, L"Media\\Sprites\\effect_particle1.png");
		break;
	}
	case Effect::Sparks:					//is usually called alongside  Effect::Shine:	
	{
		ParticleBurstData pbd{ 30u, posData, 1.0f, 10.0f, 1.5f, 0.95f };
		e2df.push_back(std::make_unique<Effect2DPhysics>( pbd, L"Media\\Sprites\\effect_particle1.png", *gfx));
		break;
	}
	default:
		assert(false && "wrong overloaded version of EffectHandler::AddEffect2D was called for this effect");
		break;
	}
}

void EffectHandler::AddEffect2D(Effect e)
{
	switch (e)
	{
	case Effect::GhoulIntentShine_Attack:				//are called by IntentIconManager
	case Effect::GhoulIntentShine_Buff:
	case Effect::GhoulIntentShine_Defence:
	{
		PositionData posData = { 0.0f, 0.0f, 0.35f };
		Effect2DSequence e2ds;
		e2ds.AddStage({ posData.pos.x,posData.pos.y,posData.pos.z,			 0.0f }, 0.3f);
		e2ds.AddStage({ posData.pos.x,posData.pos.y,posData.pos.z,			 1.0f }, 0.3f);
		e2ds.AddStage({ posData.pos.x,posData.pos.y,posData.pos.z * 1.5f,	 0.0f }, 0.0f);
		switch (e)
		{
		case Effect::GhoulIntentShine_Attack:
			EffectHandler::AddEffect2D(e2ds, L"Media\\Sprites\\Enemy_iconAttack.png");
			break;
		case Effect::GhoulIntentShine_Buff:
			EffectHandler::AddEffect2D(e2ds, L"Media\\Sprites\\Enemy_iconBuff.png");
			break;
		case Effect::GhoulIntentShine_Defence:
			EffectHandler::AddEffect2D(e2ds, L"Media\\Sprites\\Enemy_iconBlock.png");
			break;
		}
		break;
	}
	}

}

void EffectHandler::AddEffect3D(Effect3DSequence data, std::wstring spriteName, bool CamFacing)
{
	if (CamFacing)
	{
		e3dCamFace.push_back(std::make_unique<HudEffect<Effect3DSequence, PlaneTexNorm>>(*gfx, data, spriteName));
	}
	else
	{
		e3d.push_back(std::make_unique<HudEffect<Effect3DSequence, PlaneTexNorm>>(*gfx, data, spriteName));
	}
}

void EffectHandler::AddEffect3D(PositionData posData, Effect e)
{
	switch (e)
	{	
	case EffectHandler::Effect::PlayerSandThrow:				//is called by EffectHandler::AddEffect3D(Effect e)
	{
		ParticleBurstData pbd{ 2u, posData, 1.0f, 5.0f, 1.5f, 0.95f };
		ParticleSpreadData psd{
		{ 0.0f,PI * 0.5,0.0f},
		{ PI,  PI,     PI},
		{ 3.9f, -1.0f, -1.0f},
		{ 3.9f, 1.0f, 1.0f} };
		e3df.push_back(std::make_unique<Effect3DPhysics>(pbd, psd, L"Media\\Models\\Player_SandThrow.obj", *gfx));
		break;
	}
	case EffectHandler::Effect::PlayerSandStorm:				//is called by EffectHandler::AddEffect3D(Effect e)
	{
		ParticleSpreadData psd{
		{ 0.0f, 3.0f * PI / 2.0f, PI * 1.9f},
		{ 1.0f, 3.0f * PI / 2.0f, PI * 2.1f },
		{ -0.1f, 0.5f, -1.0f},
		{ 0.1f, 1.0f, 1.0f} };
		ParticleBurstData pbd{ 1u, posData, 1.0f, 1.5f, 4.5f, 0.978f };
		e3df.push_back(std::make_unique<Effect3DPhysics>(pbd, psd, L"Media\\Models\\Player_SandStorm.obj", *gfx));
		break;
	}
	case EffectHandler::Effect::AmbientPermanentBackgroundSmoke:			//is called by EffectHandler::AddEffect3D(Effect e) only once per launch
	{
		ParticleSpreadData psd{
		{1.4f*PI, 3.0f * PI / 2.0f, PI},
		{1.6f*PI, 3.0f * PI / 2.0f, PI },
		{ -1.9f, -0.01f, -0.0f},
		{ -1.0f,  0.01f,  0.0f} };
		ParticleBurstData pbd{ 1u, posData, 0.1f, 0.3f, 9999999999.0f, 0.99f };

		//smoke in the back of the room 
		pbd.origin.pos.z -= 1.8f;
		pbd.origin.pos.y -= 0.8f;
		e3df.push_back(std::make_unique<Effect3DPhysics>(pbd, psd, L"Media\\Models\\Player_SandStorm.obj", *gfx));
		pbd.origin.pos.y += 1.6f;
		e3df.push_back(std::make_unique<Effect3DPhysics>(pbd, psd, L"Media\\Models\\Player_SandStorm.obj", *gfx));
		pbd.origin.pos.z += 3.6f;
		e3df.push_back(std::make_unique<Effect3DPhysics>(pbd, psd, L"Media\\Models\\Player_SandStorm.obj", *gfx));
		pbd.origin.pos.y -= 1.6f;
		e3df.push_back(std::make_unique<Effect3DPhysics>(pbd, psd, L"Media\\Models\\Player_SandStorm.obj", *gfx));
		pbd.origin.pos.y += 0.5f;
		e3df.push_back(std::make_unique<Effect3DPhysics>(pbd, psd, L"Media\\Models\\Player_SandStorm.obj", *gfx));
		pbd.origin.pos.y += 1.0f;
		pbd.origin.pos.z += 0.5f;
		e3df.push_back(std::make_unique<Effect3DPhysics>(pbd, psd, L"Media\\Models\\Player_SandStorm.obj", *gfx));
		pbd.origin.pos.z -= 4.0f;
		e3df.push_back(std::make_unique<Effect3DPhysics>(pbd, psd, L"Media\\Models\\Player_SandStorm.obj", *gfx));
		pbd.origin.pos.y += 0.5f;
		e3df.push_back(std::make_unique<Effect3DPhysics>(pbd, psd, L"Media\\Models\\Player_SandStorm.obj", *gfx));


		//smoke on the floor (middle)
		psd.minRollPitchYaw.x -= (PI*0.4f);
		psd.maxRollPitchYaw.x -= (PI*0.4f);
		pbd.origin.pos.y = 0.2f;
		pbd.origin.pos.x = 1.5f;
		pbd.origin.pos.z = 1.0f;
		e3df.push_back(std::make_unique<Effect3DPhysics>(pbd, psd, L"Media\\Models\\WhiteSmoke.obj", *gfx));
		pbd.origin.pos.x += 1.0f;
		e3df.push_back(std::make_unique<Effect3DPhysics>(pbd, psd, L"Media\\Models\\WhiteSmoke.obj", *gfx));
		pbd.origin.pos.x += 1.0f;
		e3df.push_back(std::make_unique<Effect3DPhysics>(pbd, psd, L"Media\\Models\\WhiteSmoke.obj", *gfx));
		pbd.origin.pos.x += 1.0f;
		e3df.push_back(std::make_unique<Effect3DPhysics>(pbd, psd, L"Media\\Models\\WhiteSmoke.obj", *gfx));
		pbd.origin.pos.x += 1.0f;
		e3df.push_back(std::make_unique<Effect3DPhysics>(pbd, psd, L"Media\\Models\\WhiteSmoke.obj", *gfx));
		//floor smoke(left)
		psd.minRollPitchYaw.z -= (PI * 0.3f);
		psd.maxRollPitchYaw.z -= (PI * 0.3f);
		pbd.origin.pos.y = 0.35f;
		pbd.origin.pos.x = 1.5f;
		pbd.origin.pos.z = 2.1f;
		e3df.push_back(std::make_unique<Effect3DPhysics>(pbd, psd, L"Media\\Models\\WhiteSmoke.obj", *gfx));
		pbd.origin.pos.x += 1.0f;
		e3df.push_back(std::make_unique<Effect3DPhysics>(pbd, psd, L"Media\\Models\\WhiteSmoke.obj", *gfx));
		pbd.origin.pos.x += 1.0f;
		e3df.push_back(std::make_unique<Effect3DPhysics>(pbd, psd, L"Media\\Models\\WhiteSmoke.obj", *gfx));
		pbd.origin.pos.x += 1.0f;
		e3df.push_back(std::make_unique<Effect3DPhysics>(pbd, psd, L"Media\\Models\\WhiteSmoke.obj", *gfx));
		pbd.origin.pos.x += 1.0f;
		e3df.push_back(std::make_unique<Effect3DPhysics>(pbd, psd, L"Media\\Models\\WhiteSmoke.obj", *gfx));
		//floor smoke(right)
		psd.minRollPitchYaw.z += (PI * 0.6f);
		psd.maxRollPitchYaw.z += (PI * 0.6f);
		pbd.origin.pos.x = 1.5f;
		pbd.origin.pos.z = -0.1f;
		e3df.push_back(std::make_unique<Effect3DPhysics>(pbd, psd, L"Media\\Models\\WhiteSmoke.obj", *gfx));
		pbd.origin.pos.x += 1.0f;
		e3df.push_back(std::make_unique<Effect3DPhysics>(pbd, psd, L"Media\\Models\\WhiteSmoke.obj", *gfx));
		pbd.origin.pos.x += 1.0f;
		e3df.push_back(std::make_unique<Effect3DPhysics>(pbd, psd, L"Media\\Models\\WhiteSmoke.obj", *gfx));
		pbd.origin.pos.x += 1.0f;
		e3df.push_back(std::make_unique<Effect3DPhysics>(pbd, psd, L"Media\\Models\\WhiteSmoke.obj", *gfx));
		pbd.origin.pos.x += 1.0f;
		e3df.push_back(std::make_unique<Effect3DPhysics>(pbd, psd, L"Media\\Models\\WhiteSmoke.obj", *gfx));

		break;
	}
	default:
		break;
	}
}

void EffectHandler::AddEffect3D(Effect e)
{
	switch (e)
	{
	case Effect::GhoulAttack:						//is called from switch on enemyState in LevelLayout
	{
		PositionData posData = { 3.9f, 1.0f, 1.0f,   0.0f,  3.0f * PI / 2.0f,PI/2.0f  };

		Effect3DSequence e3ds2;
		e3ds2.AddStage({ posData.pos.x + 0.01f,posData.pos.y,posData.pos.z,		1.0f	,1.0f,posData.rot.x,posData.rot.y,posData.rot.z }, 1.0f);
		e3ds2.AddStage({ posData.pos.x + 0.01f,posData.pos.y,posData.pos.z,		1.0f	,1.0f,posData.rot.x,posData.rot.y,posData.rot.z }, 0.2f);
		e3ds2.AddStage({ posData.pos.x + 0.01f,posData.pos.y,posData.pos.z,		1.0f	,0.0f,posData.rot.x,posData.rot.y,posData.rot.z }, 0.0f);
		EffectHandler::AddEffect3D(e3ds2, L"Media\\Models\\Enemy_hit.obj",true);

		Effect3DSequence e3ds1;
		e3ds1.AddStage({ posData.pos.x,posData.pos.y,posData.pos.z ,	1.0f	,0.8f,posData.rot.x,posData.rot.y,posData.rot.z }, 0.65f);
		e3ds1.AddStage({ posData.pos.x-1.0f,posData.pos.y,posData.pos.z,1.0f	,0.0f,posData.rot.x,posData.rot.y,posData.rot.z }, 0.0f);	
		EffectHandler::AddEffect3D(e3ds1, L"Media\\Models\\Enemy_hit.obj",true);
		break;
	}
	case Effect::GhoulHowl:						//is called from ghoul skill Enrage
	{
		PositionData posData = { 4.1f, 1.2f, 1.11f,   0.0f,  3.0f * PI / 2.0f,PI / 2.0f };

		Effect3DSequence e3ds2;													  
		e3ds2.AddStage({ posData.pos.x + 0.01f,posData.pos.y,posData.pos.z,		1.5f	,0.6f,posData.rot.x,posData.rot.y,posData.rot.z }, 1.0f);
		e3ds2.AddStage({ posData.pos.x + 0.01f,posData.pos.y,posData.pos.z,		1.5f	,1.0f,posData.rot.x,posData.rot.y,posData.rot.z }, 0.2f);
		e3ds2.AddStage({ posData.pos.x + 0.01f,posData.pos.y,posData.pos.z,		1.5f	,0.0f,posData.rot.x,posData.rot.y,posData.rot.z }, 0.0f);
		EffectHandler::AddEffect3D(e3ds2, L"Media\\Models\\Enemy_howl.obj", true);

		Effect3DSequence e3ds1;
		e3ds1.AddStage({ posData.pos.x,posData.pos.y,posData.pos.z ,	1.4f	,0.8f,posData.rot.x,posData.rot.y,posData.rot.z }, 0.65f);
		e3ds1.AddStage({ posData.pos.x - 1.0f,posData.pos.y,posData.pos.z,1.4f	,0.0f,posData.rot.x,posData.rot.y,posData.rot.z }, 0.0f);
		EffectHandler::AddEffect3D(e3ds1, L"Media\\Models\\Enemy_howl.obj", true);
		break;
	}
	case Effect::PlayerSwordAttack:			//is called by class Cut : public Skill (method void Activate())
	case Effect::PlayerSwordAttackUpgraded:			
	{
		PositionData posData = { 3.9f, 1.0f, 1.0f,   0.0f,  3.0f * PI / 2.0f,nextAngle+PI/2.0f };

		Effect3DSequence e2ds2;
		e2ds2.AddStage({ posData.pos.x - 0.05f,posData.pos.y,posData.pos.z, 1.5f,1.0f,posData.rot.x,posData.rot.y,posData.rot.z }, 1.0f);
		e2ds2.AddStage({ posData.pos.x - 0.05f,posData.pos.y,posData.pos.z, 1.5f,1.0f,posData.rot.x,posData.rot.y,posData.rot.z }, 0.2f);
		e2ds2.AddStage({ posData.pos.x - 0.05f,posData.pos.y,posData.pos.z, 1.5f,0.0f,posData.rot.x,posData.rot.y,posData.rot.z }, 0.0f);

		Effect3DSequence e2ds1;
		e2ds1.AddStage({ posData.pos.x-0.1f,		posData.pos.y,posData.pos.z, 1.5f,1.0f,posData.rot.x,posData.rot.y,posData.rot.z }, 0.45f);
		e2ds1.AddStage({ posData.pos.x-0.2f,posData.pos.y,posData.pos.z,		2.5f,0.0f,posData.rot.x,posData.rot.y,posData.rot.z }, 0.0f);

		if (e == Effect::PlayerSwordAttack)
		{
			EffectHandler::AddEffect3D(e2ds2, L"Media\\Models\\Player_SwordStrike.obj", true);
			EffectHandler::AddEffect3D(e2ds1, L"Media\\Models\\Player_SwordStrike.obj", true);
		}
		else
		{
			EffectHandler::AddEffect3D(e2ds2, L"Media\\Models\\Player_SwordStrikeUpgraded.obj", true);
			EffectHandler::AddEffect3D(e2ds1, L"Media\\Models\\Player_SwordStrikeUpgraded.obj", true);
		}
		break;
	}
	case Effect::PlayerSandThrowDamage:			//is called by Class sandThrow : public Skill (method void Activate())
	{
		PositionData posData = { RU.GetFloat(3.85f,3.95f), RU.GetFloat(0.8f,1.2f), RU.GetFloat(0.7f,1.2f),0.0f, 3.0f * PI / 2.0f,	 RU.GetFloat(-0.4f, 0.4f) + PI / 2.0f };

		Effect3DSequence e3ds2;
		e3ds2.AddStage({ posData.pos.x + 0.01f,posData.pos.y,posData.pos.z,		1.0f	,1.0f,posData.rot.x,posData.rot.y,posData.rot.z }, 1.0f);
		e3ds2.AddStage({ posData.pos.x + 0.01f,posData.pos.y,posData.pos.z,		1.0f	,1.0f,posData.rot.x,posData.rot.y,posData.rot.z }, 0.2f);
		e3ds2.AddStage({ posData.pos.x + 0.01f,posData.pos.y,posData.pos.z,		1.0f	,0.0f,posData.rot.x,posData.rot.y,posData.rot.z }, 0.0f);
		EffectHandler::AddEffect3D(e3ds2, L"Media\\Models\\Player_SandThrow_damage.obj", true);

		Effect3DSequence e3ds1;
		e3ds1.AddStage({ posData.pos.x,posData.pos.y,posData.pos.z ,	1.0f	,0.8f,posData.rot.x,posData.rot.y,posData.rot.z }, 0.65f);
		e3ds1.AddStage({ posData.pos.x - 1.0f,posData.pos.y,posData.pos.z,1.0f	,0.0f,posData.rot.x,posData.rot.y,posData.rot.z }, 0.0f);
		EffectHandler::AddEffect3D(e3ds1, L"Media\\Models\\Player_SandThrow_damage.obj", true);
	}		//v-v-v-v-v-v-v-v-v-v-v-v-v-v-v-v-v-v-v-v-v-v-v-v-v-v
	case Effect::PlayerSandThrow:			//is called by Class sandThrow : public Skill (method void Activate())
	{	
		PositionData posData = { 3.9f, 1.0f, 1.0f,   0.0f,  3.0f * PI / 2.0f, RU.GetFloat(-0.4f, 0.4f) + PI / 2.0f };
		Effect3DSequence e3ds2;
		e3ds2.AddStage({ posData.pos.x + 0.01f,posData.pos.y,posData.pos.z,		1.0f	,1.0f,posData.rot.x,posData.rot.y,posData.rot.z }, 1.0f);
		e3ds2.AddStage({ posData.pos.x + 0.01f,posData.pos.y,posData.pos.z,		1.0f	,1.0f,posData.rot.x,posData.rot.y,posData.rot.z }, 0.2f);
		e3ds2.AddStage({ posData.pos.x + 0.01f,posData.pos.y,posData.pos.z,		1.0f	,0.0f,posData.rot.x,posData.rot.y,posData.rot.z }, 0.0f);
		EffectHandler::AddEffect3D(e3ds2, L"Media\\Models\\Player_SandThrowIndicator.obj", true);

		Effect3DSequence e3ds1;
		e3ds1.AddStage({ posData.pos.x,posData.pos.y,posData.pos.z ,	1.0f	,0.8f,posData.rot.x,posData.rot.y,posData.rot.z }, 0.65f);
		e3ds1.AddStage({ posData.pos.x - 1.0f,posData.pos.y,posData.pos.z,1.0f	,0.0f,posData.rot.x,posData.rot.y,posData.rot.z }, 0.0f);
		EffectHandler::AddEffect3D(e3ds1, L"Media\\Models\\Player_SandThrowIndicator.obj", true);

		EH::AddScenario(EffectScenario::EffectStep::PlayerSandThrow, 7u, 0.035f);
			break;
	}
	case Effect::PlayerSandStorm:			//is called by Class sandStorm : public Skill (method void Activate())
	{ 
		EH::AddScenario(EffectScenario::EffectStep::PlayerSandStorm,20u,0.04f);
		break;
	}
	case Effect::AmbientPermanentBackgroundSmoke:			//is called byLevelLayoutConstructor
	{
		EH::AddScenario(EffectScenario::EffectStep::AmbientPermanentBackgroundSmoke, 99999999u, 99999999.0f);
		break;
	}
	default:
		assert(false && "no such 3D effect exists");
		break;
	}
}

void EffectHandler::AddScenario(EffectScenario::EffectStep e, size_t timesRepeat, float delay)
{
	scenarios.emplace_back(e, timesRepeat, delay);
}

void EffectHandler::SetNextAngle(float ang)
{
	nextAngle = ang;
}

void EffectHandler::SubmitAndUpdate(FrameCommander& fc, float dt)
{
	scenarios.erase(std::remove_if(scenarios.begin(), scenarios.end(), [dt](EffectScenario& o) {return o.Execute(dt); }), scenarios.end());
	e2d.erase(std::remove_if(e2d.begin(), e2d.end(), [dt](std::unique_ptr<HudEffect<Effect2DSequence, HudElement>>& o) {return o->UpdateData(dt); }),e2d.end());
	for (const auto& e : e2d)e->SmartSubmit(fc);
	e3d.erase(std::remove_if(e3d.begin(), e3d.end(), [dt](std::unique_ptr<HudEffect<Effect3DSequence, PlaneTexNorm>>& o) {return o->UpdateData(dt); }), e3d.end());
	for (const auto& e : e3d)e->SmartSubmit(fc,Techniques::Standart);
	e3dCamFace.erase(std::remove_if(e3dCamFace.begin(), e3dCamFace.end(), [dt](std::unique_ptr<HudEffect<Effect3DSequence, PlaneTexNorm>>& o) {return o->UpdateData(dt); }), e3dCamFace.end());
	for (const auto& e : e3dCamFace)e->SmartSubmit(fc, Techniques::CamFacing);
	for (auto& e : e2df)
	{
		e->SubmitAndUpdateParticles(fc, dt);
	}
	e2df.erase(std::remove_if(e2df.begin(), e2df.end(), [](std::unique_ptr <Effect2DPhysics>& o) {return o->isDead; }), e2df.end());
	for (auto& e : e3df)
	{
		e->SubmitAndUpdateParticles(fc, dt);
	}
	e3df.erase(std::remove_if(e3df.begin(), e3df.end(), [](std::unique_ptr <Effect3DPhysics>& o) {return o->isDead; }), e3df.end());
}

std::optional<Effect2DData> Effect2DSequence::Update(float dt)				//make friend of handler
{
	currentStageTime += dt;
	for (;;)
	{
		if (currentStageTime < stageTimes[0u])
		{
			return std::optional<Effect2DData>(Effect2DDataInterpolate(stages[0u], stages[1u], currentStageTime / stageTimes[0u]));
		}
		else
		{
			currentStageTime -= stageTimes[0u];
			stages.erase(stages.begin());
			stageTimes.erase(stageTimes.begin());
			if (stageTimes.size() == 1u) return std::optional<Effect2DData>(); //no more data to interpolate, the effect has done its job
		}
	}
}

Effect2DPhysics::Effect2DPhysics(ParticleBurstData partData,  std::wstring sprite, DXGraphics& gfx):
	lifeTime(partData.lifeTime),
	particleSprite(sprite, gfx)
{
	PositionData po = partData.origin;
	auto& ru = RU;			
	po.pos.z *= partScale;				//scaling down
	for (size_t i = 0; i < partData.count; i++)
	{
		partPos.push_back(po);
		auto speed = ru.GetFloat(partData.speedMin, partData.speedMax);
		auto speedDist = ru.GetFloat0to1();
		partVel.emplace_back(ru.GetBool()?speed * speedDist : -speed * speedDist, ru.GetBool() ? speed * (1.0f - speedDist): -speed * (1.0f - speedDist), partData.friction);
	}
	particleSprite.BindPositionData(&partPos);
}

void Effect2DPhysics::SubmitAndUpdateParticles(FrameCommander& fc, float dt)
{
	curTime += dt;
	if (curTime <= lifeTime)
	{
		for (size_t i = 0; i < partPos.size(); i++)
		{
			partPos[i].pos.x += (partVel[i].pos.x * dt);
			partPos[i].pos.y += (partVel[i].pos.y * dt);

			partVel[i].pos.x *= (partVel[i].pos.z);
			partVel[i].pos.y *= (partVel[i].pos.z);
		}
	}
	else
	{
		isDead = true;
		partPos.clear();
		partVel.clear();
	}
	if (partPos.size())
	{
		particleSprite.SmartSubmit(fc);
	}
}

std::optional<Effect3DData> Effect3DSequence::Update(float dt)
{
	currentStageTime += dt;
	for (;;)
	{
		if (currentStageTime < stageTimes[0u])
		{
			return std::optional<Effect3DData>(Effect3DDataInterpolate(stages[0u], stages[1u], currentStageTime / stageTimes[0u]));
		}
		else
		{
			currentStageTime -= stageTimes[0u];
			stages.erase(stages.begin());
			stageTimes.erase(stageTimes.begin());
			if (stageTimes.size() == 1u) return std::optional<Effect3DData>(); //no more data to interpolate, the effect has done its job
		}
	}
}

Effect3DPhysics::Effect3DPhysics(ParticleBurstData burstData, ParticleSpreadData partSpread, std::wstring model, DXGraphics& gfx) :
	lifeTime(burstData.lifeTime),
	particleModel(model, gfx),
	friction(burstData.friction),
	gfx(gfx)
{
	particleModel.GetTechniquesReference()[0].GetSteps()[0].SetTargetPass(1u);		
	particleModel.GetTechniquesReference()[1].GetSteps()[0].SetTargetPass(1u);		
	PositionData pos = burstData.origin;
	auto& ru = RU;
	for (size_t i = 0; i < burstData.count; i++)
	{
		partPos.emplace_back(pos.pos.x, pos.pos.y, pos.pos.z,
			ru.GetFloat(partSpread.minRollPitchYaw.x, partSpread.maxRollPitchYaw.x),
			ru.GetFloat(partSpread.minRollPitchYaw.y, partSpread.maxRollPitchYaw.y),
			ru.GetFloat(partSpread.minRollPitchYaw.z, partSpread.maxRollPitchYaw.z));
		auto speed = ru.GetFloat(burstData.speedMin, burstData.speedMax);
		Vec3 velocity { ru.GetFloat(partSpread.minXYZDir.x,partSpread.maxXYZDir.x),
			ru.GetFloat(partSpread.minXYZDir.y,partSpread.maxXYZDir.y),
			ru.GetFloat(partSpread.minXYZDir.z,partSpread.maxXYZDir.z)	
		};
		velocity *= speed;
		auto speedDist = ru.GetFloat0to1();
		partVel.emplace_back(velocity.x,velocity.y,velocity.z);
	}
	particleModel.BindPositionData(&partPos);

	auto& s = particleModel.GetTechniquesReference()[0].GetSteps()[0];		//making model use pixel shader with configurable opacity

	s.DeleteBindableByUID(L"class Bind::PixelShader#Shaders\\PhongPSNormMask.cso");
	s.AddBindable(Bind::PixelShader::Resolve(gfx, L"Shaders\\PhongPSOpac.cso"));

	cbuf = std::make_shared<Bind::PixelConstantBuffer<OpacCBuff>>(gfx, 4);			//opacity buffer
	s.AddBindable(cbuf);
	cbData.val = 1.0f;
}

void Effect3DPhysics::SubmitAndUpdateParticles(FrameCommander& fc, float dt)
{
	curTime += dt;
	if (!isDying)
	{
		if (curTime <= lifeTime)
		{
			for (size_t i = 0; i < partPos.size(); i++)
			{
				partPos[i].pos.x += (partVel[i].pos.x * dt);
				partPos[i].pos.y += (partVel[i].pos.y * dt);
				partPos[i].pos.z += (partVel[i].pos.z * dt);

				partVel[i].pos.x *= friction;
				partVel[i].pos.y *= friction;
				partVel[i].pos.z *= friction;
			}
		}
		else
		{
			isDying = true;
			curTime = 0.0f;			
		}
	}
	else
	{
		if (curTime <= deathTime)
		{
			cbData.val = (1.0f - curTime / deathTime);
		}
		else
		{
			isDead = true;
			partPos.clear();
			partVel.clear();
		}
	}
	if (partPos.size())
	{
		cbuf->Update(gfx, cbData);
		particleModel.SmartSubmit(fc, Techniques::Standart);
	}
}

EffectScenario::EffectScenario(EffectStep e, size_t timesRepeat, float delay) :
	totalStages(timesRepeat),
	stageTime(delay),
	effect(e)
{}

bool EffectScenario::Execute(float dt)
{
	curTime += dt;
	while (curTime > stageTime||curStage==0u)
	{
		if(curStage!=0)curTime -= stageTime;
		curStage++;
		if (curStage <= totalStages)
		{
			switch (effect)
			{
			case EffectStep::PlayerSandThrow:
				EH::AddEffect3D({ 1.5f, 1.0f, 1.0f }, EH::Effect::PlayerSandThrow);
				break;
			case EffectStep::PlayerSandStorm:
				EH::AddEffect3D({ RU.GetFloat(2.0f,7.0f), -0.2f, RU.GetFloat(0.0f, 2.0f) }, EH::Effect::PlayerSandStorm);
				break;
			case EffectStep::AmbientPermanentBackgroundSmoke:
				EH::AddEffect3D({ 8.2f, 1.0f, 1.0f }, EH::Effect::AmbientPermanentBackgroundSmoke);
				break;
			}
		}
		else return true;
	}
	return false;
}

std::vector<std::unique_ptr<HudEffect<Effect2DSequence, HudElement>>> EffectHandler::e2d;
std::vector<std::unique_ptr<Effect2DPhysics>> EffectHandler::e2df;
std::vector<std::unique_ptr<HudEffect<Effect3DSequence, PlaneTexNorm>>> EffectHandler::e3d;
std::vector<std::unique_ptr<HudEffect<Effect3DSequence, PlaneTexNorm>>> EffectHandler::e3dCamFace;
std::vector<std::unique_ptr<Effect3DPhysics>> EffectHandler::e3df;
std::vector<EffectScenario> EffectHandler::scenarios;
DXGraphics* EffectHandler::gfx;	
float EffectHandler::nextAngle;