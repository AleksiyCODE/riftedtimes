#pragma once
#include"Framework\HudElement.h"
#include "AchievementManager.h"

class AchievementPanel : public HudElement
{
public:
	friend class AchievementsScreen;
	AchievementPanel(DXGraphics& gfx, size_t achIndex, bool isUnlocked);
	AchievementPanel(AchievementPanel&&);
	void UpdateFadedness(DXGraphics& gfx, bool isFaded);
	void SmartSubmit(FrameCommander& fc) const override;
private:
	struct fadeCbuf
	{
		size_t isToggled =	0u;
		size_t pad1 = 666u;
		size_t pad2 = 666u;
		size_t pad3 = 666u;
	};
	std::shared_ptr <Bind::PixelConstantBuffer<fadeCbuf>> cbuf;
	fadeCbuf cBufContent;
	std::vector<PositionData>borderPos;			//this position will be used as a parrent for other two
	std::vector<PositionData>picturePos;
	std::vector<PositionData>textPos;
	HudElement border;
	HudElement picture;
	HudElement text;
	static constexpr size_t achInRow = 5u;
	static constexpr size_t achInCol = 2u;
	static constexpr float cardOffsetHorisontal = 0.325f;
	static constexpr size_t cardsInARow = 5u;
};

class AchievementsScreen : public HudElement		
{
public:
	AchievementsScreen(DXGraphics& gfx);
	void UpdateLockedSkills(DXGraphics& gfx);
	void SmartSubmit(FrameCommander& fc) const override;
private:
	std::vector<AchievementPanel> panels;
	const AchievementModul& am;
};





