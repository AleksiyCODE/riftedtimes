#pragma once
#include "Framework\FrameCommander.h"
#include "./Framework/HudElement.h"
#include <unordered_map>
#include "ActionBox.h"
#include "Menu.h"			
class HUD			//responsible for drawing hud
{	
	enum class Element		
	{
		TopPanel,
		ActionBox,
		MainMenuHolder,
		TutorialHolder,
		TutorialSlide1,
		TutorialSlide2,
		TutorialSlide3,
		PlayerStatusBar,
		EnemyStatusBar,
		IntentIcon,
		RewardMenu,
		InitiativeBar,
		AchievementsScreen,
		SkillInfo,
		OtherInfo,
		Overlay
	};
public:
	HUD(const Menu& menu,FrameCommander& fc, DXGraphics& gfx, const ActionBox& abox, const CombatManager& cm);
	void Submit()const;
	void Animate(float dt);
private:	
	const Menu& menu;		
	mutable Menu::MenuState ms = Menu::MenuState::None;
	mutable Menu::MenuState prevState = Menu::MenuState::None;		//helps execute some effects correctly
	FrameCommander& fc;
	DXGraphics& gfx;
	const CombatData& cd;
	std::unordered_map<Element, std::unique_ptr<HudElement>> elements;
};