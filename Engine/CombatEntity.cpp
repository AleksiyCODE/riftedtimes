#include "CombatEntity.h"
#include "AchievementManager.h"
#include <iostream>
#include "Framework\ChiliUtil.h"
#include "HudOverlay.h"

using namespace std::string_literals;

void CombatEntity::SufferPureDamage(size_t incDmg)
{
	if (incDmg < HP)		//if does not kill
	{
		if (ToWide(typeid(*this).name()) == L"class PlayerManager"s)
		{
			HudOverlay::BoostOverlay(std::max(0.4f, 0.02857f * (float)incDmg));
		}
		HP -= incDmg; 
	}
	else
	{
		if (ToWide(typeid(*this).name()) == L"class PlayerManager"s)
		{
			HudOverlay::BoostOverlay(1.5f);
			AM::Alert(AM::Event::PlayerDiedToBloodloss);
		}
		HP = 0;
		OnDeath();
	}
	if (ToWide(typeid(*this).name()) == L"class PlayerManager"s)AM::Alert(AM::Event::PlayerLostHP);
}

size_t CombatEntity::ProkEndTurnStatusEffects(size_t statusIndex)		//one by one
{
	size_t numberOfStatuses = 0u;
	size_t curStatus = 0u;
	bool isBleading = activeStatusEffects.find(StatusEffects::Bleed) != activeStatusEffects.end();
	if (isBleading)
	{
		numberOfStatuses++;
		if (curStatus == statusIndex)
		{
			SPI::Play(Sounds::Cut);
			SufferPureDamage((size_t)activeStatusEffects[StatusEffects::Bleed].first);
			activeStatusEffects[StatusEffects::Bleed].second = true;
		}
		else
		{
			curStatus++;
			activeStatusEffects[StatusEffects::Bleed].second = false;
		}
	}
	return numberOfStatuses;
}

void CombatEntity::OnDeath()
{
	isDead = true;
}

bool CombatEntity::IsDead()
{
	return isDead;
}
