#pragma once
#include "./Framework/MainWindow.h"
#include "imgui/ImguiManager.h"
#include "./Framework/FrameTimer.h"
#include "menu.h"
#include "HUD.h"
#include "Framework/FrameCommander.h"
#include "LevelLayout.h"
#include"./Framework/TextDrawer.h"
#include "EffectHandler.h"
#include "AchievementManager.h"
#include "BlurManager.h"

class Game
{
public:
	Game();
	Game(const Game&) = delete;
	Game& operator=(const Game&) = delete;
	int Go();
private:
	ImguiManager imgui;
	MainWindow wnd;
	SoundPlayer snd;
	FrameTimer timer;
	float speed_factor = 1.0f;
	float dt=0.0f;
	FrameCommander fc;
	CombatManager cm;
	bool showDemoWindow = true;	
	bool isExiting = false;
	class LevelLayout ll;		//make sure goes after wnd and Frame Commander and combat manager
	TextDrawer td;
	Menu menu;					//menu before HUD
	HUD hud;
	EffectHandler eh;
	AchievementManager am;
	BlurManager bm;
};