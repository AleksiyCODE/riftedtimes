#pragma once
#include "SoundPlayerInterface.h"
#include "Framework\TextDrawer.h"
class CombatTimers
{
public:
	enum class ActiveTimer
	{
		NoActive,
		Countdown,
		Turn
	};

	bool UpdateTimer(float dt);			//returns isEnded
	
	void SkipTimer();

	void Stop();

	void DrawTimer() const;

	void SetActiveTimer(ActiveTimer t, float turnTime = 0);			

private:
	class TurnTimer
	{
	public:
		TurnTimer() {};

		void Reset(float turnRime)
		{		
			SPI::StopAllInstances(Sounds::tic_tac);
			SPI::StopAllInstances(Sounds::TimerCritical);
			lowTime = 0;
			criticalTime = 0;
			skipped = false;
			remainingTime = turnRime;
		}
		void Skip()
		{
			SPI::StopAllInstances(Sounds::tic_tac);
			remainingTime = 0.0f;
			skipped = true;
		}
		bool Update(float dt)				//returns isEnded
		{
			remainingTime -= dt;
			if (!lowTime&&remainingTime < 5.0f)
			{
				SPI::Play(Sounds::tic_tac, 1.0f, 0.5f);
				lowTime = 1;
			} 
			else if (!criticalTime&&remainingTime < 2.0f)
			{
				SPI::Play(Sounds::TimerCritical, 1.0f, 0.3f, 2.0f / remainingTime);
				criticalTime = 1;
			}
			else if (remainingTime < 0.0f && !skipped)
			{
				SPI::Play(Sounds::TimerEnd, 1.0f, 0.4f);
				SPI::StopAllInstances(Sounds::tic_tac);
				SPI::StopAllInstances(Sounds::TimerCritical);
				return 1;
			}
			else if(remainingTime < 0.0f && skipped)
			{
				SPI::StopAllInstances(Sounds::tic_tac);
				SPI::StopAllInstances(Sounds::TimerCritical);
				return 1;
			}
			return 0;
		}
		void Draw()const
		{
			TD::DrawString(std::to_wstring(remainingTime).substr(0u,4u), Vec2(0.23f,-0.858f), Colors::Red, 0.08f);
		}
	private:
		float remainingTime;
		bool lowTime = 0;
		bool criticalTime = 0;
		bool skipped = false;
	};
	class CountdownTimer
	{
	public:
		CountdownTimer() {};
		void Reset()
		{
			curDigit = Digits::three;
			remainingDigitTime = defaultDigitTime;
		}
		bool Update(float dt)				//returns isEnded
		{
			remainingDigitTime -= dt;
			if (curDigit != Digits::go)
			{
				if (remainingDigitTime < 0.0f)
				{
					SPI::Play(Sounds::CountdownTick, 1.0f, 0.15f);
					curDigit = static_cast<Digits>(curDigit - 1);
					if (curDigit == Digits::go)
					{
						remainingDigitTime = defaultGoTime;
					}
					else
					{
						remainingDigitTime = defaultDigitTime;
					}
				}
			}
			else
			{
				if (remainingDigitTime < 0.0f)
				{
					return 1;
				}
			}
			return 0;
		}
		void Skip()
		{
			if (curDigit != Digits::go)
			{
				remainingDigitTime = defaultGoTime;
				curDigit = Digits::go;
			}
		}
		void DrawDigit()const
		{
			Vec2 digitPos{ 0.0f, -0.65f };
			switch (curDigit)
			{
			case CombatTimers::CountdownTimer::three:		
				TD::DrawString(L"3", digitPos, Colors::Red,0.15f);
				break;
			case CombatTimers::CountdownTimer::two:
				TD::DrawString(L"2", digitPos, Colors::Red, 0.15f);
				break;
			case CombatTimers::CountdownTimer::one:
				TD::DrawString(L"1", digitPos, Colors::Red, 0.15f);
				break;
			case CombatTimers::CountdownTimer::go:
				TD::DrawString(L"GO", digitPos, Colors::Red, 0.15f);
				break;
			default:
				break;
			}
		}
	private:
		enum Digits
		{
			go,
			one,
			two,
			three
		};
		static constexpr float defaultDigitTime = 0.4f;
		static constexpr float defaultGoTime = 0.15f;
		float remainingDigitTime = defaultDigitTime;
		Digits curDigit = Digits::three;
	};
private:	
	ActiveTimer curTimer = ActiveTimer::NoActive;
	CountdownTimer countTimer;
	TurnTimer turnTimer;
};