//coded by Chili
#pragma once
#include "Bindable.h"
#include "GraphicsThrowMacros.h"
#include "Vertex.h"

namespace Bind
{
	class VertexBuffer : public Bindable
	{
	public:
		VertexBuffer(DXGraphics& gfx,const std::wstring& tag,const Dvtx::VertexBuffer& vbuf );
		VertexBuffer(DXGraphics& gfx,const Dvtx::VertexBuffer& vbuf );
		void Bind(DXGraphics& gfx ) noexcept override;
		static std::shared_ptr<VertexBuffer> Resolve(DXGraphics& gfx,const std::wstring& tag,
			const Dvtx::VertexBuffer& vbuf );
		template<typename...Ignore>
		static std::wstring GenerateUID( const std::wstring& tag,Ignore&&...ignore )
		{
			return GenerateUID_( tag );
		}
		std::wstring GetUID() const noexcept override;
	private:
		static std::wstring GenerateUID_( const std::wstring& tag );
	protected:
		std::wstring tag;
		UINT stride;
		Microsoft::WRL::ComPtr<ID3D11Buffer> pVertexBuffer;
	};
}
