#include "SoundPlayer.h"
#include "SoundPlayerInterface.h"
SoundPlayer::SoundPlayer()
{
	pSounds.insert({ Sounds::CombatCountdown,	std::make_unique<Sound>(L"Media\\Sounds\\CountDown.wav") });
	pSounds.insert({ Sounds::ABoxSkillPress,	std::make_unique<Sound>(L"Media\\Sounds\\ABoxPress.wav") });
	pSounds.insert({ Sounds::ABoxBlockedPress,	std::make_unique<Sound>(L"Media\\Sounds\\ABoxPressBlocked.wav") });
	pSounds.insert({ Sounds::Cut,				std::make_unique<Sound>(L"Media\\Sounds\\Cut.wav") });
	pSounds.insert({ Sounds::CountdownTick,		std::make_unique<Sound>(L"Media\\Sounds\\CountdownTick.wav") });
	pSounds.insert({ Sounds::MissBigDamage,		std::make_unique<Sound>(L"Media\\Sounds\\MissBigDamage.wav") });
	pSounds.insert({ Sounds::MissLittleDamage,	std::make_unique<Sound>(L"Media\\Sounds\\MissLittleDamage.wav") });
	pSounds.insert({ Sounds::Ghoul_Block,		std::make_unique<Sound>(L"Media\\Sounds\\Ghoul_Block.wav") });
	pSounds.insert({ Sounds::Applause,			std::make_unique<Sound>(L"Media\\Sounds\\Aplause.wav") });
	pSounds.insert({ Sounds::Ambient_Dungeon,	std::make_unique<Sound>(L"Media\\Sounds\\Ambient_Dungeon.wav",0.0f,15.0f)});
	pSounds.insert({ Sounds::Ambient_Torch,		std::make_unique<Sound>(L"Media\\Sounds\\Ambient_Torch.wav",0.0f,4.0f) });
	pSounds.insert({ Sounds::Dust_Throw,		std::make_unique<Sound>(L"Media\\Sounds\\Dust_Throw.wav") });
	pSounds.insert({ Sounds::Player_Block,		std::make_unique<Sound>(L"Media\\Sounds\\Player_Block.wav") });
	pSounds.insert({ Sounds::Player_Hit,		std::make_unique<Sound>(L"Media\\Sounds\\Player_Hit.wav") });
	pSounds.insert({ Sounds::SharpenTheBlade,	std::make_unique<Sound>(L"Media\\Sounds\\SharpenTheBlade.wav") });
	pSounds.insert({ Sounds::SandStorm,			std::make_unique<Sound>(L"Media\\Sounds\\SandStorm.wav") });
	pSounds.insert({ Sounds::SandsOfTime,		std::make_unique<Sound>(L"Media\\Sounds\\SandsOfTime.wav") });
	pSounds.insert({ Sounds::tic_tac,			std::make_unique<Sound>(L"Media\\Sounds\\tic_tac.wav",0.0f,3.0f) });
	pSounds.insert({ Sounds::TimerEnd,			std::make_unique<Sound>(L"Media\\Sounds\\TimerEnd.wav") });
	pSounds.insert({ Sounds::TimerCritical,		std::make_unique<Sound>(L"Media\\Sounds\\TimerCritical.wav") });
	pSounds.insert({ Sounds::Block,				std::make_unique<Sound>(L"Media\\Sounds\\Block.wav") });
	pSounds.insert({ Sounds::Ghoul_attack_01,	std::make_unique<Sound>(L"Media\\Sounds\\Ghoul_attack_01.wav") });
	pSounds.insert({ Sounds::Ghoul_attack_02,	std::make_unique<Sound>(L"Media\\Sounds\\Ghoul_attack_02.wav") });
	pSounds.insert({ Sounds::Ghoul_attack_03,	std::make_unique<Sound>(L"Media\\Sounds\\Ghoul_attack_03.wav") });
	pSounds.insert({ Sounds::Ghoul_Death,		std::make_unique<Sound>(L"Media\\Sounds\\Ghoul_Death.wav") });
	pSounds.insert({ Sounds::Ghoul_oh_01,		std::make_unique<Sound>(L"Media\\Sounds\\Ghoul_oh_01.wav") });
	pSounds.insert({ Sounds::Ghoul_oh_02,		std::make_unique<Sound>(L"Media\\Sounds\\Ghoul_oh_02.wav") });
	pSounds.insert({ Sounds::Ghoul_Enrage,		std::make_unique<Sound>(L"Media\\Sounds\\Ghoul_Enrage.wav") });
	pSounds.insert({ Sounds::Ghoul_Enforce,		std::make_unique<Sound>(L"Media\\Sounds\\Ghoul_Enforce.wav") });
	pSounds.insert({ Sounds::SkillUpgrade,		std::make_unique<Sound>(L"Media\\Sounds\\EpicSwordStrike.wav") });
	pSounds.insert({ Sounds::MissBigDamage,		std::make_unique<Sound>(L"Media\\Sounds\\MissBigDamage.wav") });
	pSounds.insert({ Sounds::Bandage,			std::make_unique<Sound>(L"Media\\Sounds\\bandage.wav") });
	pSounds.insert({ Sounds::StartBattle,		std::make_unique<Sound>(L"Media\\Sounds\\start_battle.wav") });
	pSounds.insert({ Sounds::StartPlayerTurn,	std::make_unique<Sound>(L"Media\\Sounds\\start_playerTurn.wav") });
	pSounds.insert({ Sounds::StartEnemyTurn,	std::make_unique<Sound>(L"Media\\Sounds\\start_enemyTurn.wav") });
	pSounds.insert({ Sounds::CritWave,			std::make_unique<Sound>(L"Media\\Sounds\\CritWave.wav") });
	pSounds.insert({ Sounds::Defend,			std::make_unique<Sound>(L"Media\\Sounds\\Defend.wav") });
	SPI::LinkToPlayer(this);
};

void SoundPlayer::Play(Sounds snd, float freqMod,  float volMod, float playbackSpeed) const
{	
	pSounds.at(snd)->Play(freqMod, volMod, playbackSpeed);
}

void SoundPlayer::Stop(Sounds snd) const
{
	pSounds.at(snd)->StopOne();
}

void SoundPlayer::StopAllInstances(Sounds snd) const
{
	pSounds.at(snd)->StopAll();
}
