#pragma once
#include "./Framework/TextDrawer.h"
class TurnAnnouncer
{
public:
	enum class AnnTurn
	{
		Player,
		Enemy,
		PlayerDead,
		EnemyDead,
		TheBattleBegins,
		SpawningEnemy,
		Void
	};
	bool Update(float dt)				//returns isEnded
	{
		assert(curAnn != AnnTurn::Void);	
		curAnnTime += dt;
		if (curAnnTime > announceTime)
		{
			Reset();
			return 1;
		}		
		return 0;
	}
	void SetAnnouncment(AnnTurn in_Ann)
	{
		curAnn = in_Ann;
		if (in_Ann == AnnTurn::EnemyDead)curAnnTime -= 2.5f;		//making it 2.5 seconds longer
		if (in_Ann == AnnTurn::TheBattleBegins)curAnnTime -= 1.5f;		
		if (in_Ann == AnnTurn::SpawningEnemy)curAnnTime -= 2.0f;
	}
	void Draw(size_t killcount)
	{
		TD::DrawString(std::to_wstring(killcount), Vec2(-0.65, -0.97), Colors::Yellow,0.03f);
		switch (curAnn)
		{			
		case AnnTurn::Player:
			TD::DrawString(L"Player Turn", { -0.6f, -0.4f }, Colors::Green, 0.1f);
				break;
		case AnnTurn::Enemy:
			TD::DrawString(L"Enemy Turn", { -0.6f, 0.4f }, Colors::Yellow, 0.1f);
			break;
		case AnnTurn::PlayerDead:
			TD::DrawString(L"YOU\nDIED", Vec2(-0.2f, 0.3f), Colors::Red, 0.3f);
			break;
		case AnnTurn::EnemyDead:
			TD::DrawString(L"     Killed it!\nChoose your reward!", Vec2(-0.52f, 0.15f), Colors::Green, 0.09f);
			break;
		case AnnTurn::TheBattleBegins:
			TD::DrawString(L"Let The Battle\n   BEGIN!", Vec2(-0.4f, 0.0f), Colors::Yellow, 0.12f);
			break;
		case AnnTurn::SpawningEnemy:
			TD::DrawString(L"GHOUL enteres\n the room", Vec2(-0.4f, -0.1f), Colors::Orange, 0.12f);
			break;
		default:
			break;
		}
	}
private:
	void Reset()
	{
		curAnnTime = 0.0f;
		curAnn = AnnTurn::Void;
	}
	static constexpr float announceTime = 1.2f;
	float curAnnTime = 0.0f;
	AnnTurn curAnn;
};


