#include "Menu.h"
#include "Framework/Keyboard.h"

bool Menu::Update(Keyboard::Event in_event)
{
	if (in_event.IsPress())
	{
		SetPressedKey(Key::None);
		if (cm.IsReturningToMenu())
		{
			mState = Menu::MenuState::Main;
			cm.ReturnedToMenu();
		}
		if(cm.GetRewardMenuActiveness())mState = Menu::MenuState::ChoosingReward;
		auto code = in_event.GetCode();
		switch (mState)
		{
		case Menu::MenuState::Main:
			if (code == 'W')		 
			{
				mState = MenuState::Inactive;
				cm.StartBattle(0);
				SetPressedKey(Key::W);
			}
			else if (code == 'D')		 
			{
				SetPressedKey(Key::D);
				return 1;					//quit was pressed
			}	
			else if (code == 'A')		 
			{
				SetPressedKey(Key::A);
				mState = MenuState::Achivements;
			}
			else if (code == 'S')		 
			{
				SetPressedKey(Key::S);
				mState = MenuState::Tutorial_Selection;
			}
			break;
		case Menu::MenuState::Achivements:
			if (code == 'D')		 
			{
				mState = MenuState::Main;
			}
			break;
		case Menu::MenuState::Tutorial_Selection:
			if (code == 'W')		 
			{
				SetPressedKey(Key::W);
				mState = MenuState::Tutorial1;
			}
			else if (code == 'A')		 
			{
				SetPressedKey(Key::A);
				mState = MenuState::SkillInfo;
			}
			else if (code == 'S')		 
			{
				SetPressedKey(Key::S);
				mState = MenuState::OtherInfo;
			}
			else if (code == 'D')		 
			{
				SetPressedKey(Key::D);
				mState = MenuState::Main;
			}
			break;
		case Menu::MenuState::SkillInfo:
			if (code == 'D')		 
			{
				mState = MenuState::Tutorial_Selection;
			}
			break;
		case Menu::MenuState::OtherInfo:
			if (code == 'D')		 
			{
				mState = MenuState::Tutorial_Selection;
			}
			break;
		case Menu::MenuState::Tutorial1:
			if (code == 'A')		 
			{
				mState = MenuState::Tutorial_Selection;
			}
			if (code == 'D')		 
			{
				mState = MenuState::Tutorial2;
			}
			break;
		case Menu::MenuState::Tutorial2:
			if (code == 'A')		 
			{
				mState = MenuState::Tutorial1;
			}
			if (code == 'D')		 
			{
				mState = MenuState::Tutorial3;
			}
			break;
		case Menu::MenuState::Tutorial3:
			if (code == 'A')		 
			{
				mState = MenuState::Tutorial2;
			}
			if (code == 'D')		 
			{
				mState = MenuState::Tutorial_Selection;
			}
			break;
		case Menu::MenuState::Inactive:
			break;
		case Menu::MenuState::ChoosingReward:			
				if (code == 'W')
				{
					SPI::Play(Sounds::Bandage,0.9f,1.1f);
					SetPressedKey(Key::W);
					cm.ChoseBandage();
					mState = MenuState::Inactive;
				}
				if (code == 'A')
				{
					SetPressedKey(Key::A);
					cm.ChoseDodge();
					SPI::Play(Sounds::Dust_Throw);
					mState = MenuState::Inactive;
				}
				if (code == 'S')
				{
					SetPressedKey(Key::S);
					if (cm.ChoseUpgrade())	//if all skills are already upgraded
					{
						TD::DrawStringAscending(L"All skills are already upgraded", { -0.6f,0.0f }, Colors::Cyan);
					}
					else
					{
						SPI::Play(Sounds::SkillUpgrade);
						mState = MenuState::Inactive;
					}
				}
				if (code == 'D')
				{
					SetPressedKey(Key::D);
					cm.ChoseCrit();
					SPI::Play(Sounds::SharpenTheBlade);
					mState = MenuState::Inactive;
				}			
			break;
		default:
			assert("missed a menu state" && false);
			break;
		}
	}
	return 0;
}
