#include "HudElement.h"
#include "BindableCommon.h"
#include "../imgui/imgui.h"
#include "Vertex.h"

HudElement::HudElement(std::wstring spriteName, DXGraphics& gfx)
{
	using namespace Bind;
	namespace dx = DirectX;
	using Dvtx::VertexLayout;
	using namespace std::string_literals;	

	Technique standard;
	{
		Step only(3);
		auto tex = Texture::Resolve(gfx, spriteName);
		float aspectRatio = (float)tex->GetWidth() / (float)tex->GetHeight();
		float screenAspecrRatio = (float)gfx.GetScreenWidth() / (float)gfx.GetScreenHeight();
		float totalAR = aspectRatio / screenAspecrRatio;
		only.AddBindable(std::move(tex));

		Dvtx::VertexBuffer vbuf(std::move(
			VertexLayout{}
			.Append(VertexLayout::Position2D)
			.Append(VertexLayout::Texture2D)
		));

		if (aspectRatio <= 1.0f)				//streching in right direction
		{
			vbuf.EmplaceBack(dx::XMFLOAT2{ totalAR * -1.0f, 1.0f }, dx::XMFLOAT2{ 0.0f, 0.0f });		//assembling a rectangle of 2 triangles
			vbuf.EmplaceBack(dx::XMFLOAT2{ totalAR * 1.0f, -1.0f }, dx::XMFLOAT2{ 1.0f, 1.0f });
			vbuf.EmplaceBack(dx::XMFLOAT2{ totalAR * -1.0f,-1.0f }, dx::XMFLOAT2{ 0.0f, 1.0f });
			vbuf.EmplaceBack(dx::XMFLOAT2{ totalAR * -1.0f,	1.0f }, dx::XMFLOAT2{ 0.0f, 0.0f });
			vbuf.EmplaceBack(dx::XMFLOAT2{ totalAR * 1.0f,	1.0f }, dx::XMFLOAT2{ 1.0f, 0.0f });
			vbuf.EmplaceBack(dx::XMFLOAT2{ totalAR * 1.0f, -1.0f }, dx::XMFLOAT2{ 1.0f, 1.0f });
		}
		else
		{
			totalAR = 1.0f / totalAR;
			vbuf.EmplaceBack(dx::XMFLOAT2{ -1.0f, totalAR *  1.0f }, dx::XMFLOAT2{ 0.0f, 0.0f });		//assembling a rectangle of 2 triangles
			vbuf.EmplaceBack(dx::XMFLOAT2{  1.0f, totalAR * -1.0f }, dx::XMFLOAT2{ 1.0f, 1.0f });
			vbuf.EmplaceBack(dx::XMFLOAT2{ -1.0f, totalAR * -1.0f }, dx::XMFLOAT2{ 0.0f, 1.0f });
			vbuf.EmplaceBack(dx::XMFLOAT2{ -1.0f, totalAR *  1.0f }, dx::XMFLOAT2{ 0.0f, 0.0f });
			vbuf.EmplaceBack(dx::XMFLOAT2{  1.0f, totalAR *  1.0f }, dx::XMFLOAT2{ 1.0f, 0.0f });
			vbuf.EmplaceBack(dx::XMFLOAT2{  1.0f, totalAR * -1.0f }, dx::XMFLOAT2{ 1.0f, 1.0f });
		}
		std::vector<unsigned short> indices{ 0,1,2,3,4,5 };

		only.AddBindable(PixelShader::Resolve(gfx, L"Shaders\\HudPS.cso"));
		auto pvs = VertexShader::Resolve(gfx, L"Shaders\\HudVS.cso");
		auto pvsbc = pvs->GetBytecode();
		only.AddBindable(InputLayout::Resolve(gfx, vbuf.GetLayout(), pvsbc));
		only.AddBindable(std::move(pvs));
		only.AddBindable(Blender::Resolve(gfx,true));
		only.AddBindable(std::make_shared<TransformCbuf2D>(gfx));
		standard.AddStep(std::move(only));		

		//these two should be bound outside steps because they stay the same no matter the Technique
		const auto meshTag = spriteName + L"%";
		pVertices = VertexBuffer::Resolve(gfx, meshTag, vbuf);
		pIndices = IndexBuffer::Resolve(gfx, meshTag, indices);
	}
	AddTechnique(std::move(standard));
	Technique ConsecutiveStandart;		//use this only in case where you are drawing the same drawable multiple times in one frame
										//it greatly decreaces the abount of binding done by the pipeline
	{
		Step only(3);
		only.AddBindable(std::make_shared<TransformCbuf2D>(gfx));
		ConsecutiveStandart.AddStep(std::move(only));
	}
	AddTechnique(std::move(ConsecutiveStandart));	
}

HudElement::HudElement(std::wstring spriteSheet, DXGraphics& gfx, wchar_t spriteNum, wchar_t spritesPerRow, wchar_t spritesPerColumn)
{
	using namespace Bind;
	namespace dx = DirectX;
	using Dvtx::VertexLayout;

	Technique standard;
	{
		Step only(3);		
		auto tex = Texture::Resolve(gfx, spriteSheet);
		float elementAspectRatio = ((float)tex->GetWidth() / spritesPerRow)
							  / //--------------------------------------------
								  ((float)tex->GetHeight() / spritesPerColumn);
		float screenAspecrRatio = (float)gfx.GetScreenWidth() / (float)gfx.GetScreenHeight();
		float totalAR = elementAspectRatio / screenAspecrRatio;
		only.AddBindable(std::move(tex));

		Dvtx::VertexBuffer vbuf(std::move(
			VertexLayout{}
			.Append(VertexLayout::Position2D)
			.Append(VertexLayout::Texture2D)
		));
		const float singleWdth = 1.0f / (float)spritesPerRow;
		const float singleHeight = 1.0f / (float)spritesPerColumn;
		const float leftX = (spriteNum - (spriteNum / spritesPerRow) * spritesPerRow) * singleWdth;//for texture
		const float topY = (spriteNum / spritesPerRow) * singleHeight;
		const float rightX = leftX + singleWdth;
		const float bottomY = topY + singleHeight;
		vbuf.EmplaceBack(dx::XMFLOAT2{ totalAR * -1.0f, 1.0f },  dx::XMFLOAT2{ 1.001f * leftX, 1.001f * topY });			//assembling a rectangle of 2 triangles
		vbuf.EmplaceBack(dx::XMFLOAT2{ totalAR,        -1.0f },  dx::XMFLOAT2{ 0.999f * rightX,0.999f * bottomY });
		vbuf.EmplaceBack(dx::XMFLOAT2{ totalAR * -1.0f,-1.0f },  dx::XMFLOAT2{ 1.001f * leftX, 0.999f * bottomY });
		vbuf.EmplaceBack(dx::XMFLOAT2{ totalAR * -1.0f, 1.0f },  dx::XMFLOAT2{ 1.001f * leftX, 1.001f * topY });
		vbuf.EmplaceBack(dx::XMFLOAT2{ totalAR,			1.0f },  dx::XMFLOAT2{ 0.999f * rightX,1.001f * topY });
		vbuf.EmplaceBack(dx::XMFLOAT2{ totalAR,		   -1.0f },  dx::XMFLOAT2{ 0.999f * rightX,0.999f * bottomY });
		std::vector<unsigned short> indices{ 0,1,2,3,4,5 };

		only.AddBindable(PixelShader::Resolve(gfx, L"Shaders\\HudPS.cso"));
		auto pvs = VertexShader::Resolve(gfx, L"Shaders\\HudVS.cso");
		auto pvsbc = pvs->GetBytecode();
		only.AddBindable(InputLayout::Resolve(gfx, vbuf.GetLayout(), pvsbc));
		only.AddBindable(std::move(pvs));
		only.AddBindable(Blender::Resolve(gfx, true));		//alpha usage set to true by default
		only.AddBindable(std::make_shared<TransformCbuf2D>(gfx));
		standard.AddStep(only);			//is not moved to preserve the object

		//these two should be bound outside steps because they stay the same no matter the Technique
		const auto meshTag = spriteSheet + L"%" + spriteNum;
		pVertices = VertexBuffer::Resolve(gfx, meshTag, vbuf);
		pIndices = IndexBuffer::Resolve(gfx, meshTag, indices);
	}
	AddTechnique(std::move(standard));
	Technique ConsecutiveStandart;		//use this only in case where you are drawing the same drawable multiple times in one frame
										//it greatly decreaces the abount of binding done by the pipeline
	{
		Step only(3);
		only.AddBindable(std::make_shared<TransformCbuf2D>(gfx));
		ConsecutiveStandart.AddStep(std::move(only));
	}
	AddTechnique(std::move(ConsecutiveStandart));
}

HudElement::HudElement(HudElement&& mvFrom):
	posData(std::move(mvFrom.posData)),
	Drawable(std::move(mvFrom))
{
	currentObjectBeingDrawn = mvFrom.currentObjectBeingDrawn;
	this->RebindParent();
}

DirectX::XMMATRIX HudElement::GetTransformXM() const noexcept
{
	assert(posData->size() && "no positions bound for this object - can't draw");
	currentObjectBeingDrawn++;
	if (currentObjectBeingDrawn == posData->size())currentObjectBeingDrawn = 0;
	return GetTransformXMSpecific(currentObjectBeingDrawn);
}

DirectX::XMMATRIX HudElement::GetTransformXMSpecific(size_t ind) const noexcept
{
	DirectX::XMFLOAT4X4 store{
		(*posData)[ind].pos.x, (*posData)[ind].pos.y, (*posData)[ind].pos.z, (*posData)[ind].rot.x,
		0.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 0.0f };
	if (!parentData.empty())
	{
		bool parented = false;
		std::pair<const HudElement*, size_t> parDat;
		if (parentData.count(applyParentingToAllObjects))
		{
			parDat = parentData.at(applyParentingToAllObjects);
			parented = true;
		}
		else if (parentData.count(currentObjectBeingDrawn))
		{
			parDat = parentData.at(currentObjectBeingDrawn);
			parented = true;
		}
		if (parented)
		{

			DirectX::XMMATRIX parentTransform = parDat.first->GetTransformXMSpecific(parDat.second);
			DirectX::XMFLOAT4X4 parentDec;
			DirectX::XMStoreFloat4x4(&parentDec, parentTransform);
			store._11 *= parentDec._13;//scaling position offset
			store._12 *= parentDec._13;//scaling position offset
			store._11 += parentDec._11;//translating
			store._12 += parentDec._12;//translating
			store._13 *= parentDec._13;//scaling scale
		}
	}
	DirectX::XMMATRIX encoded = DirectX::XMLoadFloat4x4(&store);
	return std::move(encoded);
}

PositionData HudElement::GetCumulatedPosData(size_t ind) const noexcept
{
	auto trans = GetTransformXMSpecific(ind);
	DirectX::XMFLOAT4X4 dec;
	DirectX::XMStoreFloat4x4(&dec, trans);
	return PositionData(dec._11, dec._12, dec._13, dec._14);
}

void HudElement::BindPositionData(const std::vector<PositionData>* posData) 
{
	this->posData = posData;
}

void HudElement::AddParent(const HudElement* par, size_t parentInd, size_t objectInd)
{
	parentData.emplace(objectInd, std::make_pair(par, parentInd));
}

void HudElement::UnbindParents()
{
	parentData.clear();
}

void HudElement::SmartSubmit(FrameCommander& fc) const
{
	if (posData->size())
	{
		this->Submit(fc, Techniques::Standart);
		for (size_t i = 1; i < posData->size(); i++)this->Submit(fc, Techniques::ConsecutiveStandart);
	}
}