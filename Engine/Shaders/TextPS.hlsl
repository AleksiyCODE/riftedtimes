Texture2D tex;
SamplerState splr;
cbuffer TextColor
{
    float3 color;
};
float4 main(float2 pos : Position, float2 tc : Texcoord) : SV_Target
{
    float4 dtex = tex.Sample(splr, tc);
    clip(dtex.a < 0.1f ? -1 : 1);
	// final color
    float4 f = { color, dtex.a };
    return f;
}