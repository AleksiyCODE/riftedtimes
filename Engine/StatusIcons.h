#pragma once
#include "Framework\HudElement.h"

class IconAtivation : public HudElement
{
public:
	IconAtivation(DXGraphics& gfx, std::wstring spriteName,  class StatusIcon* owner);		
	IconAtivation(IconAtivation&&) = default;
	IconAtivation() = default;
	void Activate();
	void Deactivate();
	void SmartSubmit(FrameCommander& fc)const override;
private:
	std::vector<PositionData> posData;					//will store relative data to parent and the parenting hud element will be binded
	const class StatusIcon* owner;
	bool isActive = false;
};

class StatusIcon : public HudElement
{
public:
	virtual ~StatusIcon() = default;
	StatusIcon(DXGraphics& gfx, std::wstring spriteName, StatusOwner owner) :
		HudElement(spriteName, gfx),
		owner(owner)
	{BindPositionData(&posData);}
	StatusIcon& operator = (StatusIcon&&) = delete;
	StatusIcon(StatusIcon&&) = delete;
	void SmartSubmit(FrameCommander& fc) const override;
	const std::vector<PositionData>& GetPosition() const;
	virtual void UpdateData(size_t ind, float val, bool isActivating = false);	//ind indicates the number (left-to-right) of this icon in the StatusBar
protected:
	std::vector<PositionData> posData;
	float value = 0.0f;
	mutable float prevValue = 0.0f;
	static constexpr float xOffset = 0.04f;
	static constexpr float size = 0.1f;
	StatusOwner owner;
	std::optional<IconAtivation> activation;
};



class AccuracyIcon : public StatusIcon		//only enemy is supposed to have accuracy
{
public:
	AccuracyIcon(DXGraphics& gfx, StatusOwner owner) :	StatusIcon(gfx, L"Media\\Sprites\\statusBar_acc.png", owner) {};
};

class BleedIcon : public StatusIcon		
{
public:
	BleedIcon(DXGraphics& gfx, StatusOwner owner) : StatusIcon(gfx, L"Media\\Sprites\\statusBar_bleed.png", owner)
		{
		activation.emplace(gfx, L"Media\\Sprites\\statusBar_bleedAct.png", this);
	};
};

class DodgeIcon : public StatusIcon
{
public:
	DodgeIcon(DXGraphics& gfx, StatusOwner owner) : StatusIcon(gfx, L"Media\\Sprites\\statusBar_dodge.png", owner) {};
};

class CritIcon : public StatusIcon
{
public:
	CritIcon(DXGraphics& gfx, StatusOwner owner) : StatusIcon(gfx, L"Media\\Sprites\\statusBar_crit.png", owner) {};
};