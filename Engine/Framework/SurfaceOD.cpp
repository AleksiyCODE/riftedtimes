#include "Surface.h"
#include <cassert>

//this file was separate for optimization purposes
void Surface::PutPixel(int x, int y, Color c)
{
	assert(x >= 0);
	assert(y >= 0);
	assert(x < GetWidth());
	assert(y < GetHeight());
	auto& imgData = *scratch.GetImage(0, 0, 0);
	reinterpret_cast<Color*>(&imgData.pixels[y * imgData.rowPitch])[x] = c;
}
void Surface::PutPixelAlpha(int x, int y, Color c)
{
	assert(x >= 0);
	assert(y >= 0);
	assert(x < (int)scratch.GetMetadata().width);
	assert(y < (int)scratch.GetMetadata().height);
	// load source pixel
	const Color d = GetPixel(x, y);

	// blend channels
	const unsigned char rsltRed = (c.GetR() * c.GetA() + d.GetR() * (255u - c.GetA())) / 256u;
	const unsigned char rsltGreen = (c.GetG() * c.GetA() + d.GetG() * (255u - c.GetA())) / 256u;
	const unsigned char rsltBlue = (c.GetB() * c.GetA() + d.GetB() * (255u - c.GetA())) / 256u;

	// pack channels back into pixel and fire pixel onto surface
	PutPixel(x, y, { rsltRed,rsltGreen,rsltBlue });
}
Color Surface::GetPixel(int x, int y) const
{
	assert(x >= 0);
	assert(y >= 0);
	assert(x < GetWidth());
	assert(y < GetHeight());
	auto& imgData = *scratch.GetImage(0, 0, 0);
	return reinterpret_cast<Color*>(&imgData.pixels[y * imgData.rowPitch])[x];
}

int Surface::GetWidth() const noexcept
{
	return (unsigned int)scratch.GetMetadata().width;
}

int Surface::GetHeight() const noexcept
{
	return (unsigned int)scratch.GetMetadata().height;
}

RectI Surface::GetRect() const noexcept		//delete after refactoring
{
	assert(false);	//not reimplemented
	return RectI();	
}

Color* Surface::GetBufferPtr() noexcept
{
	return reinterpret_cast<Color*>(scratch.GetPixels());
}

const Color* Surface::GetBufferPtr() const noexcept
{
	return const_cast<Surface*>(this)->GetBufferPtr();
}

const Color* Surface::GetBufferPtrConst() const noexcept
{
	return const_cast<Surface*>(this)->GetBufferPtr();
}