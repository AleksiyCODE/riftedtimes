#include "InitiativeBar.h"
#include <assert.h>
#include "Framework/BindableCommon.h"
#include "CombatManager.h"
#include "RandomUnit.h"

InitiativeBar::InitiativeBar(DXGraphics& gfx, const CombatData& cd ): 
	HudElement(L"Media//Sprites//Init_player.png",gfx),
	initBarHolder(L"Media//Sprites//init_holder.png",gfx),
	burn(L"Media//Sprites//init_burn.png", gfx),
	cd(cd)
{
	using namespace Bind;
	auto texEmpty = Texture::Resolve(gfx, L"Media//Sprites//Init_enem.png", 3u);
	auto& s = GetTechniquesReference()[0].GetSteps()[0];
	s.AddBindable(std::move(texEmpty));
	s.DeleteBindableByUID(L"class Bind::PixelShader#Shaders\\HudPS.cso");
	s.AddBindable(PixelShader::Resolve(gfx, L"Shaders\\InitBarPS.cso"));	
	cbuf = std::make_shared<PixelConstantBuffer<HPBarCBuff>>(gfx, 3);
	s.AddBindable(cbuf);	
	
	this->AddParent(&initBarHolder);		
	BindPositionData(&refPosData);
	refPosData.emplace_back(0.02f, 0.0f, 0.8f);
	initBarHolder.BindPositionData(&holderPosData);
	holderPosData.emplace_back(-0.979f, 0.005f, 0.55f);
	burn.BindPositionData(&burnPosData);
	burn.AddParent(&initBarHolder);
	burnPosData.emplace_back(0.02f, 0.005f, burnDefaultScale);
}

void InitiativeBar::UpdateFillness(DXGraphics& gfx)
{
	cbData.relation = cd.playerSpeed/cd.enemySpeed;
	cbuf->Update(gfx, cbData);
}

void InitiativeBar::SmartSubmit(FrameCommander& fc) const
{
	const auto textColor = cbData.relation < 1.0f ? Colors::Red : Colors::LightBlue;
	TD::DrawString(std::to_wstring(cbData.relation).substr(0u, 4u), Vec2(holderPosData[0].pos.x + 0.05f, holderPosData[0].pos.y), textColor,0.035f);	//drawing digits

	if (prevRelation > cbData.relation)					//drawing flying digits
		TD::DrawStringAscending(std::to_wstring(cbData.relation - prevRelation).substr(0u,5u), Vec2(holderPosData[0].pos.x + 0.05f, holderPosData[0].pos.y), Colors::Red, 0.035f, 1.0f, -0.3f);
	else if (prevRelation < cbData.relation)
		TD::DrawStringAscending(L"+" + std::to_wstring(cbData.relation - prevRelation).substr(0u,4u), Vec2(holderPosData[0].pos.x + 0.05f, holderPosData[0].pos.y), Colors::LightBlue, 0.035f);
	initBarHolder.SmartSubmit(fc);
	this->Submit(fc, Techniques::Standart);
	prevRelation = cbData.relation;
	burn.SmartSubmit(fc);
}

void InitiativeBar::Animate(float dt)
{						//animating burn indicator
	float wholeFill = cbData.relation + 1.0f;      //whole fillness
	float relFill = cbData.relation / wholeFill;   //part of bar that player takes up
	burnPosData[0].pos.y = (2.0f * relFill - 1.0f)*0.8f;
	if(burnPosData[0].pos.z > burnDefaultScale) burnPosData[0].pos.z *= RU.GetFloat(1.0f-burnFlickerScale,1.0f); else  burnPosData[0].pos.z *= RU.GetFloat(1.0f, 1.0f + burnFlickerScale);
}
