#pragma once
enum class EventType
{
	Player_PhysicalDamage = 100,
	Enemy_PhysicalDamage = 200,
	Player_AddDodge = 101,
	Enemy_DecreaceAcc = 202,
	Player_AddBlock = 103,
	Enemy_AddBlock = 203,
	Player_AddCrit = 104,
	Player_Heal = 105,
	Enemy_Heal = 205,
	Player_PhysicalAttackedWithBleed = 107,
	Enemy_PhysicalAttackedWithBleed = 207,
	Player_CureBleed = 108,
	Enemy_CureBleed = 208,
	Enemy_AddSpeed = 109,									
	Player_AddSpeed = 209,

	Invalid  = 1337
};