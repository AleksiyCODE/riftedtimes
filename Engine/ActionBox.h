#pragma once
#include <memory>
#include <vector>
#include "PressableMenu.h"
#include "SkillStuff/SkillHandler.h"

struct ABoxDrawingInfo
{
	Skill0 s[4];
};

enum class SkillInfoMicroicons
{
	Quick,
	TurnEnder,
	Rewinding,
	Upgraded
};

class ActionBox : public PressableMenu
{
public:	
	enum class TurnState
	{
		Done,
		NotDone,
		DoneAnimation,
		NotDoneAnimation
	};
	ActionBox(std::vector<std::unique_ptr<class Skill>>& skillPool);

	void PullNewSkills();
	
	void UnlockSkills();						//these restore skills to their normal condition

	void BlockSkills();							//also sigiftes turn end

	ABoxDrawingInfo GetPickedSkills() const;	

	const std::vector<std::map<SkillInfoMicroicons, bool>>& GetPickedSkillsInfo() const;
	
	bool ProcessInput(const char input);		//returns 1 in isFinisher

	TurnState UpdateTurnState(float dt);			//checks if the turn is finished (if yes returns 1)	

	bool UpgradeRandomSkill();				//returns 1 if all skills are upgraded

	void Reset();

protected:
	std::vector<std::unique_ptr<class Skill>>& boxPool;		
	std::vector<std::unique_ptr<class Skill>*> pickedSkills;
private:	
	void PlayPressSound();
	void InitializeAboxSkillInfoMicroicons();
	Vec2 boxPos;
	static constexpr size_t nBoxSkills = 4;				
	std::vector<char> keyToSkill;					
	bool isFinished;
	std::vector<std::map<SkillInfoMicroicons, bool>> aboxSkillInfoMicroicons;
	size_t skillsActivatedThisTurn = 0u;
	SkillHandler sh;
};