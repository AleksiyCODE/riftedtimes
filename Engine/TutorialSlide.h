#pragma once
#include"Framework\HudElement.h"
class TutorialSlide : public HudElement
{
public:
	TutorialSlide(std::wstring spriteName, DXGraphics& gfx):
		HudElement(spriteName,gfx)
	{
		posData.emplace_back(0.0f,0.0f, 1.0f);
		BindPositionData(&posData);
	}
private:
	std::vector<PositionData> posData;
};