#pragma once
#include "Skills0.h"
#include <queue>

class SkillHandler			//makes so that skills are executed in chain with certain delays sor the animation to play
{		
public:
	bool Update(float dt);
	void PendSkill(Skill* s);
	void DiscardPending();
private:
	float curTime = 0.0f;
	bool acted = false;
	std::queue<Skill*>pendingSkills;
};

