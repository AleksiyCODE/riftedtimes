#include "Transform.hlsl"
#include "LightVectorData.hlsl"
#include "Scale.hlsl"

struct VSOut
{
    float3 viewPos : Position;
    float3 viewNormal : Normal;
    float3 tan : Tangent;
    float3 bitan : Bitangent;
    float2 tc : Texcoord;
    float4 pos : SV_Position;
    float3 lv[NUM_OF_LIGHTS] : Lightvectordata;
};

VSOut main(float3 pos : Position, float3 n : Normal, float3 tan : Tangent, float3 bitan : Bitangent, float2 tc : Texcoord)
{
    VSOut vso;
    #ifdef SCALE_BOI
    pos*=scale;
    #endif    
    vso.viewPos = (float3) mul(float4(pos, 1.0f), modelView);
    vso.viewNormal = mul(n, (float3x3) modelView);
    vso.tan = mul(tan, (float3x3) modelView);
    vso.bitan = mul(bitan, (float3x3) modelView);    
    vso.pos = mul(float4(pos, 1.0f), modelViewProj);
    vso.tc = tc;
    for (int i = 0; i < NUM_OF_LIGHTS;i++)
    {        
        vso.lv[i] = viewLightPos[i] - vso.viewPos;
    }
    return vso;
}