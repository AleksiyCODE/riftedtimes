#pragma once
#include "Framework\HudElement.h"
#include "Framework\FrameCommander.h"

class FillingBar : public HudElement
{
protected:
	FillingBar(DXGraphics& gfx, std::wstring fullBarFilename, std::wstring emptyBarFilename);
private:
	friend class EnemyBar;
	friend class PlayerBar;
	void AddDrawingPosition(PositionData pd);
	void ClearPositions();
	void UpdateFillness(float fill, DXGraphics& gfx);		//	fillness is hp/maxHP
	std::vector<PositionData> refPosData;
	struct HPBarCBuff
	{
		float fillness;
		float pad1;
		float pad2;
		float pad3;
	};
	HPBarCBuff cbData;
	std::shared_ptr <Bind::PixelConstantBuffer<HPBarCBuff>> cbuf;
};

class EntityBar
{
protected:
	std::vector<PositionData> coverPosition;
	std::vector<PositionData> holderPosition;
public:
	virtual ~EntityBar() = default;
	virtual void Submit(FrameCommander& fc) const = 0;
	virtual void UpdateFillness(float hpFill, float armFill, DXGraphics& gfx, float additionalData = 0.0f) = 0;
};


class EnemyBar  : public EntityBar
{
	class EnemyArmorBar : public FillingBar
	{
		friend class EnemyBar;
		EnemyArmorBar(DXGraphics& gfx);
	};

	class EnemyHPBar : public FillingBar
	{
		friend class EnemyBar;
		EnemyHPBar(DXGraphics& gfx);
	};

	class EnemyBarHolder : public HudElement
	{
		friend class EnemyBar;
		EnemyBarHolder(DXGraphics& gfx);
	};

	class EnemyBarCover : public HudElement
	{
		friend class EnemyBar;
		EnemyBarCover(DXGraphics& gfx);
	};

	class EnemyEye : public HudElement
	{
	public:
		EnemyEye(DXGraphics& gfx);
		void UpdateScale(float scale);
	private:
		std::vector<PositionData> posData;
	};
public:
	EnemyBar(DXGraphics& gfx);
	void Submit(FrameCommander& fc) const override;
	void UpdateFillness(float hpFill,float armFill, DXGraphics& gfx, float enemySpeed = 0.0f) override;		// hp/maxHP ie
private:
	EnemyArmorBar arm;
	EnemyHPBar hp;
	EnemyBarHolder holder;
	EnemyBarCover cover;
	EnemyEye eye;
};

class PlayerBar : public EntityBar
{
	class PlayerArmorBar : public FillingBar
	{
	public:
		PlayerArmorBar(DXGraphics& gfx);
	};

	class PlayerHPBar : public FillingBar
	{
	public:
		PlayerHPBar(DXGraphics& gfx);
	};

	class PlayerBarHolder : public HudElement
	{
	public:
		PlayerBarHolder(DXGraphics& gfx);
	};

	class PlayerBarCover : public HudElement
	{
	public:
		PlayerBarCover(DXGraphics& gfx);
	};
public:
	PlayerBar(DXGraphics& gfx);
	void Submit(FrameCommander& fc) const override;
	void UpdateFillness(float hpFill, float armFill, DXGraphics& gfx, float additionalData = 0.0f) override;		// hp/maxHP ie
private:
	PlayerArmorBar arm;
	PlayerHPBar hp;
	PlayerBarHolder holder;
	PlayerBarCover cover;
};