#include "StatusIcons.h"
#include "Framework\TextDrawer.h"

void StatusIcon::UpdateData(size_t ind, float val, bool isActivating)
{
	value = val;
	posData.clear();
	const float y = (owner == StatusOwner::Player ? -0.65f : 0.5f);
	const float x = (2u + ind) * xOffset + size * ind - 1.0f;
	posData.emplace_back(x, y, size);
	if (isActivating && activation.has_value())activation->Activate(); else activation->Deactivate();
}

void StatusIcon::SmartSubmit(FrameCommander& fc) const
{
	TD::DrawString(std::to_wstring(value).substr(0u,4u), Vec2(posData[0].pos.x-0.01f, posData[0].pos.y + 0.12f), Colors::LightGray,0.03f);
	if (activation.has_value())activation->SmartSubmit(fc);
	this->Submit(fc, Techniques::Standart);

	float speed = owner == StatusOwner::Enemy ? -0.3f : 0.3f;
	if (prevValue > value)
		TD::DrawStringAscending(std::to_wstring(value - prevValue).substr(0u, 5u), Vec2(posData[0].pos.x - 0.01f, posData[0].pos.y + 0.12f), Colors::Red, 0.035f, 1.0f, speed);
	else if (prevValue < value)
		TD::DrawStringAscending(L"+" + std::to_wstring(value - prevValue).substr(0u, 4u), Vec2(posData[0].pos.x - 0.01f, posData[0].pos.y + 0.12f), Colors::LightBlue, 0.035f, 1.0f, speed);

	prevValue = value;
}

const std::vector<PositionData>& StatusIcon::GetPosition() const
{
	return posData;
}

IconAtivation::IconAtivation(DXGraphics& gfx, std::wstring spriteName,  StatusIcon* owner) :
	HudElement(spriteName, gfx),
	owner(owner)
{
	posData.emplace_back(0.0f, 0.0f, 2.0f);
	AddParent(owner, 0u);
	BindPositionData(&posData);
}

void IconAtivation::Activate()
{
	isActive = true;
}

void IconAtivation::Deactivate()
{
	isActive = false;
}

void IconAtivation::SmartSubmit(FrameCommander& fc) const
{
	if (isActive)HudElement::SmartSubmit(fc);
}
