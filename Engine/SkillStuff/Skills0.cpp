#include "Skills0.h"
#include "../CameraManager.h"
#include "../SceneLight.h"
#include "../BlurManager.h"

DustThrow::DustThrow(CombatEntity* owner) : Skill(owner)
{
	id = 5;
	valLow = valHigh = 0.25f;
	quick = 1;
	preActTime = step0;
	actTime = step1*3.0f;
}

void DustThrow::OnPend()
{
	CM::PendScenario(CM::Scenario::ThrowSand);
}

void DustThrow::Activate()
{
	SPI::Play(Sounds::Dust_Throw, 1.4f, 0.8f);
	owner->AddEvent(SkillEvent(EventType::Enemy_DecreaceAcc, valLow, valHigh));
	if (upgraded)
	{
		owner->AddEvent(SkillEvent(EventType::Enemy_PhysicalDamage, dmg, dmg));
		EH::AddEffect3D(EH::Effect::PlayerSandThrowDamage);
	}
	AM::Alert(AM::Event::PLayerUsedDustThrow);
	EH::AddEffect3D(EH::Effect::PlayerSandThrow);
}

void DustThrow::Upgrade()
{
	upgraded = true;
	rewinding = true;
	valLow = valHigh = 0.15f;
}

SandsOfTime::SandsOfTime(CombatEntity* owner) : Skill(owner)
{
	id = 4;
	valLow = valHigh = 0.15f;
	quick = 1;
	rewinding = 1;
	preActTime = step0;
	actTime = step0;
}

void SandsOfTime::Activate()
{
	SPI::Play(Sounds::SandsOfTime, 1.0f, 0.4f);
	owner->AddEvent(SkillEvent(EventType::Player_AddSpeed, valLow, valHigh));
	BlurManager::AddBlur(0.2f);
}

void SandsOfTime::Upgrade()
{
	upgraded = true;
	valLow = valHigh = 0.3f;
}

void SandsOfTime::OnPend()
{
	SL::PendDistortion(SL::LightDistortionType::GreenWave);
}

SharpenTheBlade::SharpenTheBlade(CombatEntity* owner) : Skill(owner)
{
	id = 3;
	valLow = valHigh = 0.5f;
	quick = 1;
	actTime = 0.35f;
	preActTime = step0;
	actTime = step0;
}

void SharpenTheBlade::Activate()
{
	SPI::Play(Sounds::SharpenTheBlade, 1.0f, 1.3f);
	owner->AddEvent(SkillEvent(EventType::Player_AddCrit, valLow, valHigh));
	if (upgraded)
	{
		owner->AddEvent(SkillEvent(EventType::Player_AddBlock, block, block));
	}
}

void SharpenTheBlade::Upgrade()
{
	upgraded = true;
	rewinding = true;
	valLow = valHigh = 0.35f;
}

void SharpenTheBlade::OnPend()
{
	SL::PendDistortion(SL::LightDistortionType::YellowSpark);
}

SandStorm::SandStorm(CombatEntity* owner) : Skill(owner)
{
	id = 2;
	valLow = valHigh = 0.6f;
	preActTime = step0;
	actTime = step1;
}

void SandStorm::Activate()
{
	SPI::Play(Sounds::SandStorm, 1.0f, 1.0f);
	owner->AddEvent(SkillEvent(EventType::Player_AddDodge, valLow, valHigh));
	AM::Alert(AM::Event::PlayerUsedSandStorm);
	EH::AddEffect3D(EH::Effect::PlayerSandStorm);
}

void SandStorm::Upgrade()
{
	upgraded = true;
	valLow = valHigh = 0.4f;
	quick = true;
}

void SandStorm::OnPend()
{
	CM::PendScenario(CM::Scenario::UnsettleDust);
}

Deflect::Deflect(CombatEntity* owner) : Skill(owner)
{
	id = 1;
	valLow = 10u;
	valHigh = 16u;
	succChance = 0.0f;
	preActTime = step0;
}

void Deflect::Activate()
{
	owner->AddEvent(SkillEvent(EventType::Player_AddBlock, valLow, valHigh));
	AM::Alert(AM::Event::PlayerUsedBlock);
}

void Deflect::Upgrade()
{
	upgraded = true;
	valLow = 16.0f;
	valHigh = 32.0f;
}

void Deflect::OnPend()
{
	CM::PendScenario(CM::Scenario::Defend);
	SPI::Play(Sounds::Defend, 1.0f, 1.0f);
}

Cut::Cut(CombatEntity* owner) : Skill(owner)
{
	id = 0;
	valLow = 13.0f;
	valHigh = 17.0f;
	preActTime = step0;
	actTime = step1;
}

void Cut::Activate()
{
	AM::Alert(AM::Event::PlayerUsedSwordStrike);
	if (upgraded)
	{
		EH::AddEffect3D(EH::Effect::PlayerSwordAttackUpgraded);
		owner->AddEvent(SkillEvent(EventType::Enemy_PhysicalAttackedWithBleed, valLow, valHigh));
	}
	else
	{
		owner->AddEvent(SkillEvent(EventType::Enemy_PhysicalDamage, valLow, valHigh));
		EH::AddEffect3D(EH::Effect::PlayerSwordAttack);
	}
}

void Cut::Upgrade()
{
	upgraded = true;
	valLow = 15.0f;
	valHigh = 20.0f;
}

void Cut::OnPend()
{
	CM::PendScenario(CM::Scenario::Strike);
}

float Cut::step0 = 0.16f;
float Cut::step1 = 0.06f;
float Deflect::step0 = 0.7f;
float SandStorm::step0 = 0.7f;
float SandStorm::step1 = 0.5f;
float DustThrow::step0 = 0.5f;
float DustThrow::step1 = 0.15f;
float SharpenTheBlade::step0 = 0.15f;
float SandsOfTime::step0 = 0.15f;