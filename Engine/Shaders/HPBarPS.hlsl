cbuffer TransformCBuf : register(b3)
{
    float fillness;
    float3 padding;
};
Texture2D healthFilled;
Texture2D  healthEmpty: register(t3);
SamplerState splr;
float4 main(float2 tc : Texcoord, float4 pos : SV_Position) : SV_Target
{
    float4 dtex;
    if (fillness > tc.x)
    {
        dtex = healthFilled.Sample(splr, tc);
    }
    else
    {
        dtex = healthEmpty.Sample(splr, tc);
    }
    return float4(dtex.rgba);
}



