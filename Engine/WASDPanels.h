#pragma once
#include "Framework\HudElement.h"
#include "CombatManager.h"
#include "Menu.h"

class WASDPanel :public HudElement		//derived classes are to bind panels position data(&panelPositions[i]);
										//and set this object as their parent. panels -> AddParent(this);
										//and initialize myPosData
{																	
public:	
	WASDPanel(DXGraphics& gfx);		
	void SmartSubmit(FrameCommander& fc) const override;
protected:
	std::vector<PositionData> myPosData;
	std::vector<std::vector<PositionData>> panelPositions;
	std::unordered_map<PressableMenu::Key, std::unique_ptr<HudElement>> panels;
	static constexpr float BigScale = 0.625f;
	static constexpr float MediumScale = 0.325f;
	static constexpr float SmallScale = 0.125f;
};

class MainMenuHolder :public WASDPanel
{
public:
	MainMenuHolder(DXGraphics& gfx,const Menu& menu);
	void SmartSubmit(FrameCommander& fc) const override;
	const Menu& menu;
};

class TutorialHolder :public WASDPanel
{
public:
	TutorialHolder(DXGraphics& gfx, const Menu& menu);
	void SmartSubmit(FrameCommander& fc) const override;
	const Menu& menu;
};

class RewardMenu :public WASDPanel
{
public:
	RewardMenu(DXGraphics& gfx, const CombatManager& cm, const Menu& menu);
	void SmartSubmit(FrameCommander& fc)const override;
private:
	const CombatManager& cm;
	const Menu& menu;
};

#pragma once
#include "SkillStuff/Skills0.h"
#include "ActionBox.h"
#include "SkillStuff/SkillMicroicons.h"

class ABox :public WASDPanel					//visual representation of ActionBox
{
public:
	ABox(DXGraphics& gfx, const ActionBox& actBox);
	void SmartSubmit(FrameCommander& fc) const override;
private:
	static std::vector<std::vector<PositionData>> posSkill;				//stores temporary skill positions for binding	//is static because of static cast
	std::unordered_map<Skill0, std::unique_ptr<HudElement>> skills;		
	const ActionBox& actBox;
	std::vector<PositionData>pos4;
	const std::vector<std::map<SkillInfoMicroicons, bool>>& skillIconInfo;	
	HudElement skillMicroiconPositionReferences;
	std::unordered_map<SkillInfoMicroicons, std::unique_ptr<SkillMicroicon>> skillMicroicons;
};