//coded by Chili
#include "Step.h"
#include "Drawable.h"
#include "FrameCommander.h"

Step::Step(size_t targetPass_in) :
	targetPass{ targetPass_in }
{}

void Step::AddBindable(std::shared_ptr<Bind::Bindable> bind_in) noexcept
{
	bindables.push_back(std::move(bind_in));
}

void Step::PopBindable() noexcept
{
	bindables.pop_back();
}

void Step::DeleteBindableByUID(std::wstring Id)
{
	bindables.erase(std::remove_if(bindables.begin(), bindables.end(), [&Id](std::shared_ptr<Bind::Bindable>& st) {return st->GetUID()== Id; }));
}

void Step::Submit( FrameCommander& frame,const Drawable & drawable ) const
{
	frame.Accept( Job{ this,&drawable },targetPass );
}

bool Step::Bind(DXGraphics& gfx) const
{
	for (const auto& b : bindables)
	{
		b->Bind(gfx);
	}
	return bindables.size() > 1 ? 1 : 0; //0 signals that consecutive drawing is used 
										 //and there is no need to bind Index or vertex buffers, or topology
}

void Step::SetTargetPass(size_t newPass)
{
	targetPass = newPass;
}

void Step::InitializeParentReferences( const Drawable& parent ) noexcept
{
	for( auto& b : bindables )
	{
		b->InitializeParentReference( parent );
	}
}
