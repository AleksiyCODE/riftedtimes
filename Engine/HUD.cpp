#include "HUD.h"
#include "TutorialSlide.h"
#include "ActionBox.h"
#include "SkillStuff/Skill.h"
#include "StatusBar.h"
#include "IntentIcons.h"
#include "InitiativeBar.h"
#include "WASDPanels.h"
#include "AchievementsScreen.h"
#include "HudOverlay.h"

HUD::HUD(const Menu& menu, FrameCommander& fc, DXGraphics& gfx, const ActionBox& abox, const CombatManager& cm) :					//will require a rework to support any number of skills other than 4	
	menu(menu), fc(fc),	gfx(gfx), cd(cm.GetCombatDataReference())
{
	elements.emplace(std::make_pair(Element::ActionBox,		 std::make_unique<ABox>(gfx, abox)));
	elements.emplace(std::make_pair(Element::MainMenuHolder, std::make_unique<MainMenuHolder>(gfx,menu)));
	elements.emplace(std::make_pair(Element::TutorialHolder, std::make_unique<TutorialHolder>(gfx,menu)));
	elements.emplace(std::make_pair(Element::TutorialSlide1, std::make_unique<TutorialSlide>(L"Media\\Sprites\\Slide1.png", gfx)));
	elements.emplace(std::make_pair(Element::TutorialSlide2, std::make_unique<TutorialSlide>(L"Media\\Sprites\\Slide2.png", gfx)));
	elements.emplace(std::make_pair(Element::TutorialSlide3, std::make_unique<TutorialSlide>(L"Media\\Sprites\\Slide3.png", gfx)));
	elements.emplace(std::make_pair(Element::SkillInfo,		 std::make_unique<TutorialSlide>(L"Media\\Sprites\\SkillInfo.png", gfx)));
	elements.emplace(std::make_pair(Element::OtherInfo,		 std::make_unique<TutorialSlide>(L"Media\\Sprites\\OtherInfo.png", gfx)));
	elements.emplace(std::make_pair(Element::PlayerStatusBar,std::make_unique<PlayerStatusBar>(cm.GetCombatDataReference(),gfx)));
	elements.emplace(std::make_pair(Element::EnemyStatusBar, std::make_unique<EnemyStatusBar>   (cm.GetCombatDataReference(), gfx)));
	elements.emplace(std::make_pair(Element::IntentIcon,	 std::make_unique<IntentIconManager>(cm.GetCombatDataReference(), gfx)));
	elements.emplace(std::make_pair(Element::InitiativeBar,  std::make_unique<InitiativeBar>(gfx, cm.GetCombatDataReference())));
	elements.emplace(std::make_pair(Element::Overlay,		 std::make_unique<HudOverlay>   (gfx, cm.GetCombatDataReference())));
	elements.emplace(std::make_pair(Element::RewardMenu,	 std::make_unique<RewardMenu>(gfx, cm, menu)));
	elements.emplace(std::make_pair(Element::AchievementsScreen, std::make_unique<AchievementsScreen>(gfx)));
}

void HUD::Submit() const
{
	elements.at(Element::Overlay)->SmartSubmit(fc);
	prevState = ms;
	ms = menu.GetMenuState();
	switch (ms)
	{
	case Menu::MenuState::Main:
		elements.at(Element::MainMenuHolder)->SmartSubmit(fc);
		break;
	case Menu::MenuState::Tutorial_Selection:
		elements.at(Element::TutorialHolder)->SmartSubmit(fc);
		break;
	case Menu::MenuState::Achivements:
		elements.at(Element::AchievementsScreen)->SmartSubmit(fc);
		break;	
	case Menu::MenuState::SkillInfo:
		elements.at(Element::SkillInfo)->SmartSubmit(fc);
		break;
	case Menu::MenuState::OtherInfo:
		elements.at(Element::OtherInfo)->SmartSubmit(fc);
		break;
	case Menu::MenuState::Tutorial1:
		elements.at(Element::TutorialSlide1)->SmartSubmit(fc);
		break;
	case Menu::MenuState::Tutorial2:
		elements.at(Element::TutorialSlide2)->SmartSubmit(fc);
		break;
	case Menu::MenuState::Tutorial3:
		elements.at(Element::TutorialSlide3)->SmartSubmit(fc);
		break;
	case Menu::MenuState::Inactive:					//if menu is inactive - han we are in combat
	case Menu::MenuState::ChoosingReward:
		if (cd.isValid)
		{
			elements.at(Element::ActionBox)->SmartSubmit(fc);
			dynamic_cast<PlayerStatusBar*>(elements.at(Element::PlayerStatusBar).get())->UpdateStatus(gfx);	// casting is the best solution i could find((
			dynamic_cast<EnemyStatusBar*>(elements.at(Element::EnemyStatusBar).get())->UpdateStatus(gfx);
			dynamic_cast<InitiativeBar*>(elements.at(Element::InitiativeBar).get())->UpdateFillness(gfx);
			elements.at(Element::IntentIcon)->SmartSubmit(fc);
			elements.at(Element::PlayerStatusBar)->SmartSubmit(fc);
			elements.at(Element::EnemyStatusBar)->SmartSubmit(fc);
			elements.at(Element::InitiativeBar)->SmartSubmit(fc);
			elements.at(Element::RewardMenu)->SmartSubmit(fc);
		}
		break;
	case Menu::MenuState::None:
		break;
	default:
		assert(false && "MissedMenuState");
		break;
	}

	//these additional submissions are for animations to be executed properly
	if (ms!=prevState)
		if (prevState == Menu::MenuState::Main)
		{
			elements.at(Element::MainMenuHolder)->SmartSubmit(fc);
			if (ms == Menu::MenuState::Achivements)dynamic_cast<AchievementsScreen*>(elements.at(Element::AchievementsScreen).get())->UpdateLockedSkills(gfx);
		}
		else if (prevState == Menu::MenuState::Tutorial_Selection)elements.at(Element::TutorialHolder)->SmartSubmit(fc);
		else if (prevState == Menu::MenuState::ChoosingReward)elements.at(Element::RewardMenu)->SmartSubmit(fc);
}

void HUD::Animate(float dt)
{
	elements.at(Element::Overlay)->Animate(dt);
	ms = menu.GetMenuState();
	if (ms == Menu::MenuState::Inactive || ms == Menu::MenuState::ChoosingReward)
	{
		if (cd.isValid)
		{
			elements.at(Element::InitiativeBar)->Animate(dt);	
		}
	}
}