//coded by Chili
#include "Sampler.h"
#include "GraphicsThrowMacros.h"
#include "BindableCodex.h"
#include "ChiliUtil.h"

namespace Bind
{
	Sampler::Sampler(DXGraphics& gfx )
	{
		INFOMAN( gfx );

		D3D11_SAMPLER_DESC samplerDesc = CD3D11_SAMPLER_DESC{ CD3D11_DEFAULT{} };
		samplerDesc.Filter = D3D11_FILTER_ANISOTROPIC;
		samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_MIRROR;
		samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_MIRROR;
		samplerDesc.MaxAnisotropy = D3D11_REQ_MAXANISOTROPY;

		GFX_THROW_INFO( GetDevice( gfx )->CreateSamplerState( &samplerDesc,&pSampler ) );
	}

	void Sampler::Bind(DXGraphics& gfx ) noexcept
	{
		GetContext( gfx )->PSSetSamplers( 0,1,pSampler.GetAddressOf() );
	}
	std::shared_ptr<Sampler> Sampler::Resolve(DXGraphics& gfx )
	{
		return Codex::Resolve<Sampler>( gfx );
	}
	std::wstring Sampler::GenerateUID()
	{
		return ToWide(typeid(Sampler).name());
	}
	std::wstring Sampler::GetUID() const noexcept
	{
		return GenerateUID();
	}
}