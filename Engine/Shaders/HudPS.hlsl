Texture2D tex;
SamplerState splr;
float4 main(float2 tc : Texcoord, float4 pos : SV_Position) : SV_Target
{
    float4 dtex = tex.Sample(splr, tc);
	// final color
    return float4(dtex.rgba);
}