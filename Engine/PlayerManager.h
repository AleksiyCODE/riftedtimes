#pragma once
#include "CombatEntity.h"
#include "ActionBox.h"

class PlayerManager	:public CombatEntity	
{	
public:
	PlayerManager();

	void BeginBattle();

	void StartTurn();

	void EndTurn();

	ActionBox::TurnState UpdateTurnState(float dt);

	bool ProcessInput(const char input);

	size_t SufferPhysicalDamage(size_t incDmg) override;

	float GetCritMod()const;
	
	const ActionBox& GetActionBox()const;

	 float GetDodge()const;
	
	 float GetTurnTime()const;

	 void AddTimePool(float in_time);			
	 
	 void AddCrit(float in_crit);
	 
	 void AddDodge(float in_dodge);

	 void AddPassiveCrit(float in_crit);

	 void AddPassiveDodge(float in_dodge);

	 void DumpTemporaryStatus() override;

	 bool UpgradeRandomSkill(); //returns true if all skills are upgraded 

	 void Reset();
	
private:
	void ResetTimePool();

	std::vector<std::unique_ptr<Skill>> skillPool;
	float curTurnTime = baseTurnTime;		
	float baseCrit	= 0.0f;				
	float baseDodge	= 0.0f;
	ActionBox actionBox;
	float baseTurnTime = 10.0f;
};