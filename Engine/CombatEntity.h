#pragma once
#include <algorithm>
#include <memory>
#include <vector>
#include <map>
#include "SkillStuff/SkillEvent.h"
#include "SoundPlayerInterface.h"
#include "AchievementManager.h"
enum class StatusEffects
{
	CritChance,
	Dodge,
	Accuracy,
	Bleed
};

class CombatEntity							
{	
public:	
	virtual ~CombatEntity() = default;
	size_t GetHP()const
	{
		return HP;
	}
	size_t GetmaxHP()const
	{
		return maxHP;
	}
	float GetSpeed()const
	{
		return speed;
	}
	void AddSpeed(float dSpeed)
	{
		speed += dSpeed;
	}
	void AddBlock(size_t in_block)
	{
		block += in_block;
	}
	void AddBleed(size_t emount)
	{
		activeStatusEffects[StatusEffects::Bleed].first += (float)emount;
	}
	void Heal(size_t in_heal)
	{
		HP += in_heal;
		HP = std::min(HP, maxHP);
	}
	void CureBleed()
	{
		if (activeStatusEffects.find(StatusEffects::Bleed) != activeStatusEffects.end())		//if there is bleed
		{
			if (activeStatusEffects.at(StatusEffects::Bleed).first >= 20u)AM::Alert(AM::Event::PlayerHealed20Bleed);		//if lots of bleed
			activeStatusEffects.erase(StatusEffects::Bleed);
		}
	}
	size_t ProkEndTurnStatusEffects(size_t statusIndex = 0u);
	size_t GetBlock()const
	{
		return block;
	}
	const auto GetStatusEffectsReference()
	{
		return &activeStatusEffects;
	}
	void OnDeath();
	bool IsDead();
	virtual size_t SufferPhysicalDamage(size_t incDmg) = 0;			//returns 1 if damage was suffered and 0 if it was blocked
	void SufferPureDamage(size_t incDmg);
	virtual void DumpTemporaryStatus() = 0;
	void AddEvent(SkillEvent incEv)
	{
		events.push_back(incEv);
	}
	std::vector<SkillEvent>& GetEvents()		//the caller is responsible for clearing this vector after processing messager
	{											//this is done to avoid copies
		return events;
	}
protected:
	std::map <StatusEffects, std::pair<float,bool>> activeStatusEffects;			//float - emount, bool - activeness
	size_t HP = 0;
	size_t maxHP = 0;
	size_t block = 0;
	float speed = 1.0f;
	std::vector<SkillEvent> events;
	bool isDead = false;
};
constexpr size_t OneBleedStackEmount = 5u;