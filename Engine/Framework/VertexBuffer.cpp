//coded by Chili
#include "VertexBuffer.h"
#include "BindableCodex.h"
#include "ChiliUtil.h"

namespace Bind
{
	VertexBuffer::VertexBuffer(DXGraphics& gfx,const Dvtx::VertexBuffer& vbuf )
		:
		VertexBuffer( gfx,L"?",vbuf )
	{}
	VertexBuffer::VertexBuffer(DXGraphics& gfx,const std::wstring& tag,const Dvtx::VertexBuffer& vbuf )
		:
		stride( (UINT)vbuf.GetLayout().Size() ),
		tag( tag )
	{
		INFOMAN( gfx );

		D3D11_BUFFER_DESC bd = {};
		bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		bd.Usage = D3D11_USAGE_DEFAULT;
		bd.CPUAccessFlags = 0u;
		bd.MiscFlags = 0u;
		bd.ByteWidth = UINT( vbuf.SizeBytes() );
		bd.StructureByteStride = stride;
		D3D11_SUBRESOURCE_DATA sd = {};
		sd.pSysMem = vbuf.GetData();
		GFX_THROW_INFO( GetDevice( gfx )->CreateBuffer( &bd,&sd,&pVertexBuffer ) );
	}

	void VertexBuffer::Bind(DXGraphics& gfx ) noexcept
	{
		const UINT offset = 0u;
		GetContext( gfx )->IASetVertexBuffers( 0u,1u,pVertexBuffer.GetAddressOf(),&stride,&offset );
	}
	std::shared_ptr<VertexBuffer> VertexBuffer::Resolve(DXGraphics& gfx,const std::wstring& tag,
		const Dvtx::VertexBuffer& vbuf )
	{
		assert( tag != L"?" );
		return Codex::Resolve<VertexBuffer>( gfx,tag,vbuf );
	}
	std::wstring VertexBuffer::GenerateUID_( const std::wstring& tag )
	{
		using namespace std::string_literals;
		return ToWide(typeid(VertexBuffer).name()) + L"#"s + tag;
	}
	std::wstring VertexBuffer::GetUID() const noexcept
	{
		return GenerateUID( tag );
	}
}
